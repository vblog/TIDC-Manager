const util = require('../../utils/util')
let p

Page({

	data: {
		activeType: false,
		activeName: '',
		activeCode: false
	},

	temp: {
		activeId: ''
	},

	onLoad: function (options) {
		p = this
	},

	onUnload: () => {
		util.unloadPage('active')
	},

	onShow: function () {

	},

	findActive: (e) => {
		const activeId = e.detail.value.activeid
		if (!activeId || activeId.length == 0) {
			util.modal('系统提示', '请正确填写活动口令哦')
			return
		}
		util.loading('查找中')
		util.request({
			source: 'active',
			method: 'get',
			uri: `active/${activeId}`,
			data: {
				session: util.globalData('session')
			},
			success: (data) => {
				const { activeType, activeName } = data
				p.temp.activeId = activeId
				p.setData({
					activeType,
					activeName
				})
				wx.hideToast()
			},
			fail: (code, msg) => {
				console.log(code, msg)
				if(code == '-2037')
					util.modal('未找到此活动', '请确认活动口令是否正确')
				else if (code == '-2038')
					util.modal('活动尚未开始', '此活动还未开始，请耐心等待哦')
				else if (code == '-2039')
					util.modal('活动已经结束', '此活动已结束，谢谢你的参与')
				else
					util.modal('查找失败', util.errorHandler(code, msg))
				wx.hideToast()
			}
		})
	},

	joinActive: () => {
		if (!p.temp.activeId) {
			util.modal('系统提示', '数据异常，请退出此界面重新打开')
			return
		}
		util.loading('提交中')
		util.request({
			source: 'active',
			method: 'post',
			uri: `active/${p.temp.activeId}`,
			data: {
				session: util.globalData('session')
			},
			success: (data) => {
				console.log(data)
				p.setData({
					activeCode: data
				})
				wx.hideToast()
			},
			fail: (code, msg) => {
				console.log(code, msg)
				util.modal('参加失败', util.errorHandler(code, msg))
				wx.hideToast()
			}
		})
	},

	copyActiveCode: () => {
		wx.setClipboardData({
			data: p.data.activeCode,
			success: () => {
				util.modal('复制成功', '活动码已经复制成功啦，快到线下参加活动吧^-^')
			},
			fail: () => {
				util.error('复制失败', '活动码复制失败，请长按活动码选中复制或截图')
			}
		})
	},

	onPullDownRefresh: function () {

	},

	onShareAppMessage: function () {

	}

})
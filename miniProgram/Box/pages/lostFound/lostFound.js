const util = require('../../utils/util')
let p

Page({

    data: {
		staticDomain: util.config('staticDomain'),
		pageIndex: 0,
		lostList: [],
		loadingPanel: {
			tip: '加载中'
		}
    },

	temp: {
		loadMoreDone: false
	},

    onLoad: function(options) {
		p = this
		if (!util.globalData('logined')) {
			util.tip({
				p,
				text: '登录后才能查看，跳转主页',
				done: () => {
					wx.switchTab({
						url: '/pages/home/home',
					})
				},
				timeout: 1200
			})
			util.hideLoadingPanel(p)
			return
		}
		if (!util.globalData('binded')) {
			util.tip({
				p,
				text: '绑定后才能查看，跳转主页',
				done: () => {
					wx.switchTab({
						url: '/pages/home/home',
					})
				},
				timeout: 1200
			})
			util.hideLoadingPanel(p)
			return
		}
		util.userLogin({
			p,
			scene: 'other',
			complete: () => {
				util.showNotices(p)
				p.loadLostList()
			},
			fail: (title, msg) => {
				wx.navigateBack({
					delta: 1
				})
				util.error(title, msg)
			},
			notBind: () => {
				util.modal('绑定提示', '只有登录和绑定身份后才能使用此功能哦，即将跳转到主页', () => {
					wx.switchTab({
						url: '/pages/home/home'
					})
				})
			}
		})
    },

	onShow: () => {
		if (!util.globalData('logined')) {
			util.tip({
				p,
				text: '登录后才能查看，跳转主页',
				done: () => {
					wx.switchTab({
						url: '/pages/home/home',
					})
				},
				timeout: 1200
			})
			util.hideLoadingPanel(p)
			return
		}
		if (!util.globalData('binded')) {
			util.tip({
				p,
				text: '绑定后才能查看，跳转主页',
				done: () => {
					wx.switchTab({
						url: '/pages/home/home',
					})
				},
				timeout: 1200
			})
			util.hideLoadingPanel(p)
			return
		}
	},

    onUnload: () => {
        util.unloadPage('lostFound')
    },

	loadLostList: (obj) => {
		const { sync = false, loadMore = false, success, fail } = obj || {}
		if (!sync && p.temp.loadMoreDone)
			return
		p.getLostList({
			sync,
			loadMore,
			success: (lostList) => {
				p.setLostList({sync, lostList})
				wx.stopPullDownRefresh()
				if (loadMore)
					util.hideLoadingBox(p)
				else
					util.hideLoadingPanel(p)
				if (success)
					success(lostList)
			},
			fail: (code, msg) => {
				wx.stopPullDownRefresh()
				if(loadMore)
					util.hideLoadingBox(p)
				else
					util.hideLoadingPanel(p)
				if(fail)
					fail(code, msg)
				util.error('获取失物列表失败', util.errorHandler(code, msg))
			}
		})
	},

	getLostList: ({ sync = false, loadMore = false, success, fail}) => {
		if (loadMore)
			util.showLoadingBox(p)
		else if(sync)
			util.showLoadingPanel(p, '加载中')
		util.request({
			source: 'lostFound',
			method: sync ? 'put' : 'get',
			uri: 'user/lost',
			data: {
				start: sync ? 0 : p.data.lostList.length,
				count: 15
			},
			success,
			fail
		})
	},

	setLostList: ({ sync = false, lostList }) => {
		if (lostList.length == 0)
			p.temp.loadMoreDone = true
		if (!sync)
			lostList = p.data.lostList.concat(lostList)
		p.setData({
			lostList
		})
	},

	changePage: e => {
		const index = !isNaN(e) ? e : e.currentTarget.dataset.index
		wx.setNavigationBarTitle({
			title: ['失物大厅', '我的寻物'][index],
		})
		p.setData({
			pageIndex: index
		})
	},

	goLostDetail: (e) => {
		const lostId = e.currentTarget.dataset.id
		util.goPage('lostDetail', {
			lostId
		})
	},

	previewPhoto: (e) => {
		const url = e.currentTarget.dataset.url
		if (!url)
			return
		wx.previewImage({
			urls: [url],
		})
	},

	goLostRecord: () => {
		util.goPage('lostRecord')
	},

	goObtainGoods: () => {
		util.goPage('obtainGoods')
	},

	goLostGoods: () => {
		util.goPage('lostGoods')
	},

	goLostCenter: () => {
		util.goPage('lostCenter')
	},

	onPullDownRefresh: () => {
		if(p.data.pageIndex == 1)
			return
		p.loadLostList({
			sync: true
		})
	},

	onReachBottom: () => {
		p.loadLostList({
			sync: false,
			loadMore: true
		})
	},

    onShareAppMessage: function() {
		return {
			title: '创新失物招领中心，集发布寻物或招领启事以及失物托管于一体的平台',
			path: '/pages/lostFound/lostFound'
		}
    }
	
})
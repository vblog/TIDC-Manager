const util = require('../../utils/util')
let p

Page({

	data: {
		rn: Date.now()
	},

	onLoad: function (params) {
		p = this
	},

	onUnload: () => {
		util.unloadPage('enrollQuery')
	},

	refreshVCode: () => {
		p.setData({
			rn: Date.now()
		})
	},

	queryEnroll: () => {
		
	},
	
	onShareAppMessage: function () {
		return {
			title: '广东创新科技职业学院-录取查询',
			path: '/pages/enrollQuery/enrollQuery'
		}
	}

})
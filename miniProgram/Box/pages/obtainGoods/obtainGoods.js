const util = require('../../utils/util.js')
let p

Page({

	data: {
		photoList: [],
		formSubmiting: false,
		formBtnDisabled: false
	},

	onLoad: function (params) {
		p = this
	},

	onUnload: () => {
		util.unloadPage('obtainGoods')
	},

	addPhoto: () => {
		wx.chooseImage({
			count: Math.abs(p.data.photoList.length - 3),
			sizeType: ['compressed'],
			success: function (res) {
				const { mimes, maxSize } = util.config('upload')
				for (let file of res.tempFilePaths) {
					if (file.size > maxSize.repairPhoto) {
						util.error('图片过大，无法上传，请重新选择小于10M的图片')
						return
					}
				}
				const list = p.data.photoList.concat(res.tempFilePaths)
				p.setData({
					photoList: list
				})
			}
		})
	},

	usePhoto: (e) => {
		const index = e.currentTarget.dataset.index
		wx.showActionSheet({
			itemList: ['删除'],
			success: (res) => {
				p.data.photoList.splice(index, 1)
				p.setData({
					photoList: p.data.photoList
				})
			}
		})
	},

	uploadLostPhoto: (obj) => {
		let { lostPhotoList = [], success, fail, index = 0, uploadIdList = [] } = obj
		if (lostPhotoList.length == 0) {
			success()
			return
		}
		util.uploadFile({
			uri: `user/lost/photo/upload`,
			name: 'image',
			file: lostPhotoList[index],
			data: {
				session: util.globalData('session')
			},
			success: (data) => {
				uploadIdList.push(data)
				if (index == lostPhotoList.length - 1) {
					success(uploadIdList)
					return
				}
				p.uploadLostPhoto({
					lostPhotoList,
					success,
					fail,
					index: ++index,
					uploadIdList
				})
			},
			fail: (code, msg) => {
				fail(code, msg)
				return
			}
		})
	},

	changeAgreeProtocol: (e) => {
		let agree = e.detail.value.length
		p.setData({ formBtnDisabled: !agree })
	},

	goProtocol: () => {

	},

	submitLostGoods: (e) => {
		if (p.data.formSubmiting)
			return
		const { lostTitle, lostContent, lostAddress, lostContact } = e.detail.value
		if (!lostTitle) {
			util.error('填写寻物标题', '请输入突出重点的寻物标题')
			return
		}
		if (!lostContent) {
			util.error('填写失物信息', '请输入完整失物的详情和描述')
			return
		}
		if (!lostAddress) {
			util.error('填写丢失地址', '请输入失物丢失地址')
			return
		}
		if (!lostContact) {
			util.error('填写联系方式', '请输入正确的联系方式，以便失物能够及时归还')
			return
		}
		wx.showModal({
			title: '确认发布',
			content: '您即将发布一份“失物招领启事”，请确认信息是否填写正确，发布后我们将在2小时内完成内容审核，审核通过后将发布在失物大厅，我们先代替失主感谢你！如找到失主归还后请在“发布记录”内结束本次寻物，一条启事信息一般最长保留一个月时间。也可以将失物交给实训楼6-403的失物招领中心处理哦~',
			confirmText: '发布',
			cancelText: '继续填写',
			success: (res) => {
				if (res.confirm) {
					const lostPhotoList = p.data.photoList
					p.setData({ formSubmiting: true })
					p.uploadLostPhoto({
						lostPhotoList,
						success: (uploadIdList) => {
							console.log('上传成功', uploadIdList)
							util.request({
								source: 'obtainGoods',
								method: 'post',
								uri: 'user/lost/goods',
								data: {
									lostTitle,
									lostContent,
									lostAddress,
									lostContact,
									uploadIdList
								},
								success: () => {
									p.setData({ formSubmiting: false })
									util.success('提交成功')
									setTimeout(() => {
										wx.navigateBack({
											success: () => {
												util.goPage('lostRecord')
											}
										})
									}, 600)
								},
								fail: (code, msg) => {
									p.setData({ formSubmiting: false })
									util.error('提交失败', util.errorHandler(code, msg))
								}
							})
						},
						fail: (code, msg) => {
							p.setData({ formSubmiting: false })
							util.error('上传失败', util.errorHandler(code, msg))
						}
					})
				}
			}
		})

	}

})
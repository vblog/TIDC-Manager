const util = require('../../utils/util')
let p

Page({

	data: {
		activeTab: 0,
		courseName: '',
		evaluationContents: [
			'老师营造的课程学习氛围浓厚，使得我很喜欢参与相应教学活动或讨论。',
			'老师提供的教学PPT或其他教学资源生动、清晰，能很好地帮助我理解相应内容。',
			'老师的引导使我对这门课程感兴趣或兴趣更浓。',
			'老师的教学使我对学好这门课程或专业知识、技术提升了信心。',
			'本人理解了课程的教学内容，且能描述相应的内容。',
			'通过课程学习，认为已掌握了老师教的思考、分析和解决问题的方法。',
			'通过课程学习，能运用课程所学内容和方法或工具，独立完成课程预期学习成果。',
			'能在课程学习或课程预期学习成完成的基础上，提出新的问题，并会寻求解决的路径。',
			'评估本人本门课程的学习效果。',
			'评估全班本门课程的整体学习效果。'
		],
		evaluationMap: {
			'great': '优',
			'good': '良',
			'medium': '中',
			'pass': '及格',
			'bad': '差'
		},
		evaluationRemark: '',
		allowSubmit: false
	},

	temp: {
		evaluationId: null,
		evaluationList: []
	},

	onLoad: function (params) {
		p = this
		if(!params.id) {
			util.error('系统提示', '参数不正确', wx.navigateBack)
		}
		p.temp.evaluationId = params.id
		p.getEvaluationTchData({
			evaluationId: params.id,
			success: (evaluationTchData) => {
				console.log(evaluationTchData)
				p.setEvaluationTchData(evaluationTchData)
			},
			fail: (code, msg) => {
				if(code == '-3050') {
					util.error('已评教', '您的所有课程已评教完成，无需再评教，将退回首页', () => {
						wx.switchTab({
							url: '/pages/home/home',
						})
					})
					return
				}
				util.hideLoadingPanel(p)
				util.error('获取失败', util.errorHandler(code, msg), wx.navigateBack)
			}
		})
	},

	onUnload: () => {
		util.unloadPage('evaluationSingleTch')
	},

	getEvaluationTchData: (obj) => {
		const { evaluationId, success, fail } = obj
		util.showLoadingPanel(p, '获取中')
		util.request({
			source: 'evaluationSingleTch',
			method: 'get',
			uri: `user/student/evaluation/${evaluationId}`,
			data: {
				session: util.globalData('session')
			},
			success,
			fail
		})
	},

	setEvaluationTchData: (evaluationTchData) => {
		const { courseName, evaluations, remark } = evaluationTchData
		wx.setNavigationBarTitle({
			title: courseName,
		})
		p.setData({
			courseName,
			evaluationData: evaluations,
			evaluationRemark: remark
		})
		util.hideLoadingPanel(p)
	},

	switchTab: (e) => {
		const current = e.currentTarget.dataset.index || e.detail.current || 0
		p.setData({
			activeTab: current
		})
	},

	evaluationTch: (e) => {
		const {tch, index} = e.currentTarget.dataset
		if (!p.temp.evaluationList[tch])
			p.temp.evaluationList[tch] = []
		wx.showActionSheet({
			itemList: ['优秀', '良好', '中等', '及格', '差劲'],
			success: (res) => {
				if(res.cancel)
					return
				const { evaluationData } = p.data
				evaluationData[tch].evaluation[index] = ['great', 'good', 'medium', 'pass', 'bad'][res.tapIndex]
				let pass = false
				let old = evaluationData[tch].evaluation[0]
				let unknownCount = 0
				for (let e of evaluationData[tch].evaluation) {
					if(e == 'unknown')
						unknownCount++
					if(old != e && !pass)
						pass = true
				}
				if (!pass && unknownCount != evaluationData[tch].evaluation.length) {
					util.modal('温馨提示', '因正方系统限制，每个教师的每一项评价不能全部相同，至少要有一个其它的评价，请修改后重试噢')
					return
				}
				p.setData({
					evaluationData
				})
				let allow = true
				for (let d of evaluationData) {
					if (d.evaluation.indexOf('unknown') != -1) {
						allow = false
						break
					}
				}
				p.setData({
					allowSubmit: allow
				})
			}
		})
	},

	submitEvaluationTch: (e) => {
		const { evaluationRemark } = e.detail.value
		if (!p.data.allowSubmit)
			return
		wx.showModal({
			title: '是否提交？',
			content: '已经完成评价，是否需要现在提交？如需修改请点击继续修改',
			cancelText: '继续修改',
			confirmText: '确认提交',
			success: (res) => {
				if (res.confirm) {
					util.loading('提交中')
					const { evaluationId } = p.temp
					const { evaluationData } = p.data
					for (let tch in evaluationData) {
						p.temp.evaluationList[tch] = evaluationData[tch].evaluation
					}
					util.request({
						source: 'evaluationSingleTch',
						method: 'put',
						uri: `user/student/evaluation/${evaluationId}`,
						data: {
							evaluationList: p.temp.evaluationList,
							evaluationRemark: evaluationRemark.length > 0 ? evaluationRemark : undefined,
							session: util.globalData('session')
						},
						success: (data) => {
							wx.hideToast()
							util.success('提交成功')
							util.globalData('finishEvaluationIdTemp', p.temp.evaluationId)
							setTimeout(() => {
								wx.navigateBack()
							}, 800)
						},
						fail: (code, msg) => {
							wx.hideToast()
							console.error('evaluation course failed', code, msg)
							wx.showModal({
								title: '提交失败',
								content: '提交评价内容失败，请稍后重试哦',
								cancelText: '返回上层',
								confirmText: '重试提交',
								success: (res1) => {
									if (res1.cancel) {
										wx.navigateBack()
									}
									else if (res1.confirm) {
										p.submitEvaluationTch(e)
									}
								}
							})
						}
					})
				}
			}
		})
	}

})
const util = require('../../utils/util')
let p

Page({

	data: {
		correlSyllabusList: [],
		openAddCorrelPanel: false,
		matchMode: false,
		chooseAll: false,
		loadingPanel: {
			tip: '获取中'
		}
	},

	temp: {

	},

	onLoad: function() {
		p = this
		p.getCorrelSyllabusList((correlSyllabusList) => {
			p.setCorrelSyllabusList(correlSyllabusList)
		})
	},

	onUnload: () => {
		util.unloadPage('correlSyllbus')
	},

	correlSyllabus: (e) => {
		const correlStuId = e.detail.value.correlStuId
		wx.showModal({
			title: '确认关联？',
			content: '确认关联将发送关联申请消息给对方，对方接受后，你们双方将都能够看到对方的课程表',
			success: (res) => {
				if(res.confirm)
					p.sendCorrelApply(correlStuId)
			}
		})
	},

	openMatchMode: () => {
		p.setData({
			openAddCorrelPanel: false,
			matchMode: true
		})
	},

	closeMatchMode: () => {
		p.setData({
			matchMode: false
		})
	},

	chooseAll: () => {
		p.setData({
			chooseAll: !p.data.chooseAll
		})
	},

	matchSyllabus: (e) => {
		let list = e.detail.value.chooseCorrelList
		if(list.length == 0) {
			util.error('未勾选', '请勾选需要匹配的关联课程表才能进行匹配')
			return
		}
		if(p.data.chooseAll)
			list.splice(list.indexOf('chooseAll'), 1)
		util.modal('暂未开放', '此功能暂未开放，我们将尽快完工')
		return
		util.loading('匹配中')
		util.request({
			source: 'correlSyllabus',
			uri: ''
		})
	},

	openAddCorrelPanel: () => {
		if (p.data.correlSyllabusList.length >= 50) {
			util.error('已达上限', '关联人数已达50人上限哦，如果有特殊需求请反馈给我们')
			return
		}
		p.setData({
			openAddCorrelPanel: true
		})
	},

	closeAddCorrelPanel: () => {
		p.setData({
			openAddCorrelPanel: false
		})
	},

	cancelCorrel: (e) => {
		const correlId = e.currentTarget.dataset.correlid
		const { correlOtherStuId, correlOtherStuName } = p.getCorrelSyllabusData(correlId)
		wx.showModal({
			title: '解除关联',
			content: `确定要解除与${correlOtherStuName}课程表的关联吗？解除后双方将无法看到对方课表。如果是小情侣请冷静一下双方有什么矛盾好好说，没有什么是沟通无法解决的~`,
			success: (res) => {
				if(res.confirm) {
					util.loading('解除中')
					util.request({
						source: 'correlSyllabus',
						method: 'delete',
						uri: `user/student/syllabus/correl/${correlId}`,
						data: {
							session: util.globalData('session')
						},
						success: () => {
							p.delCorrelSyllabusData(correlId)
							wx.hideToast()
							util.success('解除成功')
						},
						fail: (code, msg) => {
							wx.hideToast()
							util.error('解除失败', util.errorHandler(code, msg))
						}
					})
				}
			}
		})
	},

	goSyllabus: (e) => {
		const correlId = e.currentTarget.dataset.correlid
		const { correlOtherStuId } = p.getCorrelSyllabusData(correlId)
		util.goPage('otherSyllabus', {
			stuId: correlOtherStuId
		})
	},

	getCorrelSyllabusList: (callback) => {
		util.request({
			source: 'correlSyllabus',
			method: 'get',
			uri: 'user/student/syllabus/correl',
			data: {
				session: util.globalData('session')
			},
			success: (data) => {
				callback(data)
			},
			fail: (code, msg) => {
				util.hideLoadingPanel(p)
				util.modal('获取失败', util.errorHandler(code, msg))
			}
		})
	},

	setCorrelSyllabusList: (correlSyllabusList) => {
		let list = {}
		correlSyllabusList.sort((a, b) => {
			return b.correlDate - a.correlDate
		})
		p.setData({
			correlSyllabusList
		})
		util.hideLoadingPanel(p)
	},

	getCorrelSyllabusData(correlId) {
		for (let index in p.data.correlSyllabusList) {
			const correl = p.data.correlSyllabusList[index]
			if (correl.correlId == correlId)
				return correl
		}
	},

	delCorrelSyllabusData(correlId) {
		for (let index in p.data.correlSyllabusList) {
			const correl = p.data.correlSyllabusList[index]
			if (correl.correlId == correlId) {
				p.data.correlSyllabusList.splice(index, 1)
				
				break
			}
		}
		p.setData({
			correlSyllabusList: p.data.correlSyllabusList
		})
	},

	sendCorrelApply: (correlStuId) => {
		util.loading('发送中')
		util.request({
			source: 'correlSyllabus',
			method: 'post',
			uri: 'user/student/syllabus/correl',
			data: {
				correlStuId,
				session: util.globalData('session')
			},
			success: (data) => {
				p.setData({
					openAddCorrelPanel: false
				})
				wx.hideToast()
				util.success('发送成功')
			},
			fail: (code, msg) => {
				wx.hideToast()
				util.error('无法关联', util.errorHandler(code, msg))
			}
		})
	},

	onPullDownRefresh: () => {
		wx.stopPullDownRefresh()
		util.showLoadingPanel(p, '获取中')
		p.getCorrelSyllabusList((correlSyllabusList) => {
			p.setCorrelSyllabusList(correlSyllabusList)
		})
	},

	onShareAppMessage: () => {
		return {
			title: '关联课表功能开通啦~快来关联Ta吧~',
			path: '/pages/home/home'
		}
	}

})
const util = require('../../utils/util')
let p

Page({

	data: {
		selection: true,
		bindType: '',
		bindDone: false,
		continueEdu: false,
		userName: '',
		formSubmiting: false,
		formBtnDisabled: false,
		// vCodeUrl: '',
		// vCodeVal: ''
	},

	temp: {
		
	},

	onLoad: function () {
		p = this
		//p.loadVCode()
	},

	onUnload: () => {
		util.unloadPage('userBinder')
	},

	//进入绑定学生
	goStudentBinder: () => {
		p.setData({
			selection: false,
			bindType: 'student'
		})
	},

	//进入绑定老师
	goTeacherBinder: () => {
		p.setData({
			selection: false,
			bindType: 'teacher'
		})
		//1103
		//126170
	},

	bindStudent: e => {
		let { userId, userPwd, agreeProtocol } = e.detail.value
		//let { userId, userPwd, userVCode, agreeProtocol } = e.detail.value
		if (!/^(1\d{9}|C1\d{9})$/.test(userId)) {
			util.error('学号不正确', '请填写正确的学号哦')
			return
		}
		if (userPwd.length == 0) {
			util.error('密码不能为空', '请填写正确的密码哦')
			return
		}
		// if (!/^[a-zA-Z0-9]{4}$/.test(userVCode)) {
		// 	util.error('验证码不能为空', '请填写正确的验证码哦')
		// 	return
		// }
		if (!agreeProtocol[0]) {
			util.error('未同意协议', '必须勾选同意《创新盒子用户协议》才能绑定用户哦')
			return
		}
		util.modal('温馨提示', '由于本操作需要从学校的龟速教务系统获取数据，所以绑定的时间可能稍长，大概需要10秒左右完成绑定，请耐心等待，如果绑定失败请稍后重试，密码默认是身份证号（教务系统的账号），多次绑定失败可能是教务系统又崩了，您可以访问jwc.gdcxxy.net查看是否能够正常访问和登录，如果能够正常登录却无法绑定请到主页点击“我要反馈”进行反馈，多谢合作！！！', () => {
			p.setData({ formSubmiting: true })
			util.request({
				source: 'userBinder',
				method: 'post',
				uri: 'user/student',
				data: {
					stuId: userId,
					stuPwd: userPwd,
					//zfVCode: userVCode,
					session: util.globalData('session')
				},
				success: (stuData) => {
					console.log(stuData)
					const { stuId, stuName, stuSex } = stuData
					const { userHead, userName } = util.globalData('userData')
					util.globalData('userData', {
						userType: 'student',
						userHead,
						userName,
						stuId,
						stuName,
						stuSex
					})
					p.showBindSuccess()
				},
				fail: (code, msg) => {
					// p.loadVCode()
					// p.setData({
					// 	vCodeVal: ''
					// })
					if (code == '-3000')
						p.setData({ formSubmiting: false })
					else
						p.setData({ formSubmiting: false })
					if (code == '-2')
						util.error('绑定超时', '可能是学校的教务管理系统崩溃中，请稍后重试')
					else
						util.error('绑定失败', util.errorHandler(code, msg))
				}
			})
		})
	},

	bindTeacher: e => {
		let { userId, userPwd, agreeProtocol } = e.detail.value
		if (!/^\d{4}$/.test(userId)) {
			util.error('工号不正确', '请填写正确的工号哦')
			return
		}
		if (userPwd.length == 0) {
			util.error('密码不能为空', '请填写正确的密码哦')
			return
		}
		if (!agreeProtocol[0]) {
			util.error('未同意协议', '必须勾选同意《创新盒子用户协议》才能绑定用户哦')
			return
		}
		util.modal('温馨提示', '由于本操作需要从学校的龟速教务系统获取数据，所以绑定的时间可能稍长，大概需要10秒左右完成绑定，请耐心等待，如果绑定失败请稍后重试，密码默认是身份证后六位（数字化校园的账号），多次绑定失败可能是教务系统又崩了，您可以访问cas.gdcxxy.net与jwc.gdcxxy.net查看是否能够正常访问和登录，如果能够正常登录却无法绑定请到主页点击“我要反馈”进行反馈，多谢合作！！！', () => {
			p.setData({ formSubmiting: true })
			util.request({
				source: 'userBinder',
				method: 'post',
				uri: 'user/teacher',
				data: {
					tchId: userId,
					tchPwd: userPwd,
					session: util.globalData('session')
				},
				success: (tchData) => {
					console.log(tchData)
					const { tchId, tchName, tchSex } = tchData
					const { userHead, userName } = util.globalData('userData')
					util.globalData('userData', {
						userType: 'teacher',
						userHead,
						userName,
						tchId,
						tchName,
						tchSex
					})
					p.showBindSuccess()
				},
				fail: (code, msg) => {
					if (code == '-3000')
						p.setData({ formSubmiting: false })
					else
						p.setData({ formSubmiting: false })
					if (code == '-2')
						util.error('绑定超时', '可能是学校的教务管理系统崩溃中，请稍后重试')
					else
						util.error('绑定失败', util.errorHandler(code, msg))
				}
			})
		})
	},

	changeAgreeProtocol: e => {
		let agree = e.detail.value.length
		p.setData({ formBtnDisabled: !agree })
	},

	//查看用户协议
	goProtocol: () => {

	},

	//扫码绑定码
	scanBindCode: () => {
		wx.scanCode({
			success: (res) => {
				util.modal('温馨提示', '此通道暂未开通，敬请期待')
			},
			fail: (err) => {
				if(err.errMsg.indexOf('cancel') == -1) {
					console.error(err)
					util.error('扫码失败', '嗷，开启扫码功能似乎失败了呢。。。请重新点击或者重启微信再次点击扫码绑定噢(￣▽￣)／')
				}
			}
		})
	},

	//绑定成功显示
	showBindSuccess: () => {
		//清除前绑定者的课程表数据
		wx.removeStorage({
			key: 'syllabusList',
			success: function(res) {},
		})
		wx.removeStorage({
			key: 'courseTime',
			success: function (res) {},
		})
		//设置绑定完成的状态
		util.globalData('binded', true)
		wx.connectSocket({
			url: `${util.config('wsDomain')}message/student?session=${util.globalData('session')}`,
			fail: (err) => {
				console.error(err)
			}
		})
		p.setData({ bindDone: true })
	},

	//结束绑定返回上层
	finishBind: () => {
		wx.navigateBack({
			delta: 1
		})
	},

	//打开绑定提示
	openBindTip: () => {
		util.modal('绑定帮助', '绑定身份能够将您的微信号和您在创新学院的身份信息绑定起来，这样您将能够使用“创新盒子”小程序的所有的功能，我们将需要获取您在 正方教务管理系统 内的信息，因为我们需要知道此微信号是由您（学生、老师或其它）使用的，但不会泄露任何关于您的个人信息，保证您的信息只用于您个人。绑定后当您下次进入本小程序则无需输入任何信息即可自动登录并可使用功能。绑定流程很简单，如果您是学生或老师仅需点击“我是学生”或“我是老师”按钮，接着填写相关账号数据，并且勾选《创新盒子用户协议》，点击“绑定”，通过验证后将绑定成功，您就可以正常使用本小程序的功能了。另外也专门为其它身份提供了“扫码绑定”功能，具体使用方法根据实际情况而定。感谢您的使用ヽ(￣▽￣)ﾉ')
	},

	//加载验证码
	// loadVCode: () => {
	// 	p.setData({
	// 		vCodeUrl: ''
	// 	})
	// 	util.request({
	// 		source: 'userBinder',
	// 		method: 'get',
	// 		uri: 'user/zfVCode',
	// 		success: (vCodeData) => {
	// 			const base64Str = 'data:image/gif;base64,' + wx.arrayBufferToBase64(vCodeData.data.data)
	// 			p.setData({
	// 				vCodeUrl: base64Str
	// 			})
	// 		},
	// 		fail: (code, msg) => {
	// 			util.error('验证码获取失败', util.errorHandler(code, msg))
	// 		}
	// 	})
	// },

})
const util = require('../../utils/util.js')
let p

Page({

    data: {
        staticDomain: util.config('staticDomain'),
		tabIndex: 0,
		waitRecordList: [],
		publishRecordList: [],
		finishRecordList: [],
		waitRecordLoaded: false,
		publishRecordLoaded: false,
		finishRecordLoaded: false,
		syncing: false
    },

    onLoad: function(options) {
		p = this
		p.loadWaitRecord()
    },

    onUnload: function() {
		util.unloadPage('lostRecord')
    },

	changeTab: (e) => {
		const index = !isNaN(e) ? e : (e.detail.current || e.currentTarget.dataset.index || 0)
		p.setData({
			tabIndex: parseInt(index)
		})
		if (!e.detail || (e.detail.source != '' && e.detail.source != 'touch'))
			return
		switch(parseInt(index)) {
			case 0:
				p.loadWaitRecord()
			break
			case 1:
				p.loadPublishRecord()
			break
			case 2:
				p.loadFinishRecord()
			break
		}
	},

	loadWaitRecord: (obj) => {
		const { sync = false, success, fail } = obj || {}
		if (p.data.waitRecordLoaded)
			return
		p.setData({
			syncing: true
		})
		util.showLoadingBox(p)
		util.request({
			source: 'lostRecord',
			method: sync ? 'put' : 'get',
			uri: 'user/lost/waitRecord',
			success: (waitRecordList) => {
				p.setData({
					waitRecordList,
					waitRecordLoaded: true,
					syncing: false
				})
				util.hideLoadingBox(p)
				if (sync)
					wx.stopPullDownRefresh()
				if (success)
					success(waitRecordList)
			},
			fail: (code, msg) => {
				p.setData({
					syncing: false
				})
				util.hideLoadingBox(p)
				if (sync)
					wx.stopPullDownRefresh()
				util.error('加载失败', util.errorHandler(code, msg))
				if (fail)
					fail()
			}
		})
	},

	loadPublishRecord: (obj) => {
		const { sync = false, success, fail } = obj || {}
		if (p.data.publishRecordLoaded)
			return
		p.setData({
			syncing: true
		})
		util.showLoadingBox(p)
		util.request({
			source: 'lostRecord',
			method: sync ? 'put' : 'get',
			uri: 'user/lost/publishRecord',
			success: (publishRecordList) => {
				p.setData({
					publishRecordList,
					publishRecordLoaded: true,
					syncing: false
				})
				util.hideLoadingBox(p)
				if (sync)
					wx.stopPullDownRefresh()
				if (success)
					success(publishRecordList)
			},
			fail: (code, msg) => {
				util.hideLoadingBox(p)
				p.setData({
					syncing: false
				})
				if (sync)
					wx.stopPullDownRefresh()
				util.error('加载失败', util.errorHandler(code, msg))
				if(fail)
					fail()
			}
		})
	},

	loadFinishRecord: (obj) => {
		const { sync = false, success, fail } = obj || {}
		if (p.data.finishRecordLoaded)
			return
		p.setData({
			syncing: true
		})
		util.showLoadingBox(p)
		util.request({
			source: 'lostRecord',
			method: sync ? 'put' : 'get',
			uri: 'user/lost/finishRecord',
			success: (finishRecordList) => {
				p.setData({
					finishRecordList,
					finishRecordLoaded: true,
					syncing: false
				})
				util.hideLoadingBox(p)
				if(sync)
					wx.stopPullDownRefresh()
				if (success)
					success(finishRecordList)
			},
			fail: (code, msg) => {
				util.hideLoadingBox(p)
				p.setData({
					syncing: false
				})
				if (sync)
					wx.stopPullDownRefresh()
				util.error('加载失败', util.errorHandler(code, msg))
				if (fail)
					fail()
			}
		})
	},

	useLost: (e) => {
		const lostId = e.currentTarget.dataset.id
		let itemList = []
		switch(p.data.tabIndex) {
			case 0:
				wx.showActionSheet({
					itemList: ['查看详情', '删除此项'],
					success: (res) => {
						if(res.tapIndex == 0)
							p.goLostDetail(lostId)
						else if (res.tapIndex == 1)
							p.delLostData(lostId)
					}
				})
			break
			case 1:
				wx.showActionSheet({
					itemList: ['查看详情', '删除此项', '结束寻物'],
					success: (res) => {
						if (res.tapIndex == 0)
							p.goLostDetail(lostId)
						else if (res.tapIndex == 1)
							p.delLostData(lostId)
						else if(res.tapIndex == 2)
							p.finishLostFound(lostId)
					}
				})
			break
			case 2:
				wx.showActionSheet({
					itemList: ['查看详情'],
					success: (res) => {
						if (res.tapIndex == 0)
							p.goLostDetail(lostId)
					}
				})
			break
		}
	},

	goLostDetail: (lostId) => {
		util.goPage('lostDetail', {
			lostId
		})
	},

	delLostData: (lostId) => {
		util.loading('删除中')
		util.request({
			source: 'lostRecord',
			method: 'delete',
			uri: `user/lost/${lostId}`,
			success: () => {
				const list = [p.data.waitRecordList, p.data.publishRecordList][p.data.tabIndex]
				for (let index in list) {
					if (list[index].lostId == lostId) {
						list.splice(index, 1)
						p.setData([{
							waitRecordList: list
						}, {
							publishRecordList: list
						}][p.data.tabIndex])
						break
					}
				}
				wx.hideToast()
				util.success('删除成功')
			},
			fail: (code, msg) => {
				util.error('删除失败', util.errorHandler(code, msg))
			}
		})
	},

	finishLostFound: (lostId) => {
		util.loading('结束中')
		util.request({
			source: 'lostRecord',
			method: 'put',
			uri: `user/lost/${lostId}/finish`,
			success: () => {
				for (let index in p.data.publishRecordList) {
					if (p.data.publishRecordList[index].lostId == lostId) {
						p.data.publishRecordList.splice(index, 1)
						p.setData({
							publishRecordList: p.data.publishRecordList,
							finishRecordLoaded: false,
							tabIndex: 2
						})
						break
					}
				}
				wx.hideToast()
				util.success('结束成功')
			},
			fail: (code, msg) => {
				util.error('结束失败', util.errorHandler(code, msg))
			}
		})
	},

	previewPhoto: (e) => {
		const url = e.currentTarget.dataset.url
		if (!url)
			return
		wx.previewImage({
			urls: [url],
		})
	},

    onPullDownRefresh: function() {
		switch (p.data.tabIndex) {
			case 0:
				p.setData({
					waitRecordList: [],
					waitRecordLoaded: false
				})
				p.loadWaitRecord({
					sync: true
				})
				break
			case 1:
				p.setData({
					publishRecordList: [],
					publishRecordLoaded: false
				})
				p.loadPublishRecord({
					sync: true
				})
				break
			case 2:
				p.setData({
					finishRecordList: [],
					finishRecordLoaded: false
				})
				p.loadFinishRecord({
					sync: true
				})
				break
		}
		if(p.data.tabIndex == 0) {
			p.setData({
				publishRecordList: [],
				publishRecordLoaded: false
			})
			p.loadPublishRecord({
				sync: true
			})
		}
		else {
			p.setData({
				finishRecordList: [],
				finishRecordLoaded: false
			})
			p.loadFinishRecord({
				sync: true
			})
		}
    }

})
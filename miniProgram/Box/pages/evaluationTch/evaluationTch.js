const util = require('../../utils/util')
let p

Page({

    data: {
		evaluationData: [],
		mode: '',
		beingEvaluated: false,
		allowSubmit: false,
		submiting: false,
		loadingPanel: {
			tip: '同步中'
		}
    },

	temp: {
		stopEvaluation: false
	},

    onLoad: function() {
		p = this
		util.userLogin({
			p,
			scene: 'other',
			complete: () => {
				util.showNotices(p)
				p.getEvaluationData({
					success: (evaluationData) => {
						p.setEvaluationData(evaluationData)
					},
					fail: (code, msg) => {
						util.hideLoadingPanel(p)
						util.error('同步失败', util.errorHandler(code, msg), wx.navigateBack)
					}
				})
			},
			fail: (title, msg) => {
				wx.navigateBack({
					delta: 1
				})
				util.error(title, msg)
			},
			notBind: () => {
				util.modal('绑定提示', '只有登录和绑定身份后才能使用此功能哦，即将跳转到主页', () => {
					wx.switchTab({
						url: '/pages/home/home'
					})
				})
			}
		})
    },

	onUnload: () => {
		util.unloadPage('evaluationTch')
	},

	onShow: () => {
		const finishEvaluationId = util.globalData('finishEvaluationIdTemp')
		if (finishEvaluationId)
			p.checkAllowSubmit(finishEvaluationId)
	},

	checkAllowSubmit: (finishEvaluationId) => {
		const { evaluationData } = p.data
		let evaluatedCount = 0
		for (let index in evaluationData) {
			if (finishEvaluationId && evaluationData[index].id == finishEvaluationId) {
				evaluationData[index].evaluated = true
				p.setData({
					evaluationData
				})
			}
			if (evaluationData[index].evaluated)
				evaluatedCount++
		}
		if (evaluatedCount == evaluationData.length)
			p.setData({
				allowSubmit: true
			})
	},

	getEvaluationData: (obj) => {
		const {success, fail, sync} = obj
		wx.stopPullDownRefresh()
		util.request({
			source: 'evaluationTch',
			method: sync ? 'put' : 'get',
			uri: 'user/student/evaluation/list',
			data: {
				session: util.globalData('session')
			},
			success,
			fail
		})
	},

	setEvaluationData: (evaluationData) => {
		if (evaluationData.length == 0) {
			p.setData({
				mode: 'success'
			})
			util.hideLoadingPanel(p)
		}
		else {
			p.setData({
				mode: 'evaluation',
				evaluationData
			})
			p.checkAllowSubmit()
			util.hideLoadingPanel(p)
		}
	},

	evaluationTchs: (obj) => {
		const { evaluationData, success } = obj
		let {index = 0} = obj
		if (p.temp.stopEvaluation) {
			p.temp.stopEvaluation = false
			return
		}
		if (!evaluationData[index]) {
			success()
			return
		}
		if (evaluationData[index].evaluated) {
			console.log('已评教')
			p.evaluationTchs({
				evaluationData,
				index: ++index,
				success
			})
			return
		}
		const { id: evaluationId } = evaluationData[index]
		util.request({
			source: 'evaluationTch',
			method: 'put',
			uri: `user/student/evaluation/${evaluationId}`,
			data: {
				session: util.globalData('session')
			},
			success: (data) => {
				p.updateEvaluationStatus(evaluationId, true)
			},
			fail: (code, msg) => {
				if(code == '-3076') {
					p.temp.stopEvaluation = true
					util.error('系统提示', '教务系统暂未对您开放评教，请等待评教通知', wx.navigateBack)
					return
				}
				console.error('evaluation course failed', code, msg)
				p.updateEvaluationStatus(evaluationId, false)
			},
			complete: () => {
				index++
				p.evaluationTchs({
					evaluationData,
					index,
					success
				})
			}
		})
	},

	autoEvaluationTch: () => {
		const {allowSubmit} = p.data
		wx.showModal({
			title: '确认操作',
			content: allowSubmit ? '是否立即提交评教结果？' : '是否使用让创新盒子代替您评教？点击确定后系统将自动以最优评价完成评教，当全部项都显示绿色勾并显示评教成功界面时将完成评教，时间可能稍长请耐心等待，您也可以点击每门课程进入单个评分',
			success: (res) => {
				if(res.confirm) {
					p.setData({
						beingEvaluated: true
					})
					const { evaluationData } = p.data
					p.evaluationTchs({
						evaluationData,
						success: () => {
							console.log(p.data.evaluationData)
							for (let c of p.data.evaluationData) {
								if (!c.evaluated) {
									p.setData({
										beingEvaluated: false
									})
									return
								}
							}
							p.setData({
								allowSubmit: true,
								submiting: true
							})
							util.request({
								source: 'evaluationTch',
								method: 'post',
								uri: 'user/student/evaluation',
								data: {
									evaluationId: evaluationData[evaluationData.length - 1].id,
									session: util.globalData('session')
								},
								success: (data) => {
									console.log(data)
									p.setData({
										submiting: false,
										beingEvaluated: false,
										mode: 'success'
									})
								},
								fail: (err) => {
									console.error(err)
									p.setData({
										submiting: false,
										beingEvaluated: false
									})
								}
							})
						}
					})
				}
			}
		})
	},

	updateEvaluationStatus: (evaluationId, status) => {
		const { evaluationData } = p.data
		for (let index in evaluationData) {
			if (evaluationData[index].id == evaluationId && evaluationData[index].evaluated != status) {
				evaluationData[index].evaluated = Boolean(status)
				p.setData({
					evaluationData
				})
				break
			}
		}
	},

	backToHome: () => {
		wx.switchTab({
			url: '/pages/home/home',
		})
	},

	evaluationSingleTch: (e) => {
		const { beingEvaluated, submiting } = p.data
		if (beingEvaluated || submiting) {
			util.modal('温馨提示', '正在自动评教中，请等待自动评教完成')
			return
		}
		const {id} = e.currentTarget.dataset
		util.goPage('evaluationSingleTch', {
			id
		})
	},

	showHelp: () => {
		util.modal('使用帮助', '速评教是一个能够帮助你快速完成教务评教的功能，使用时需要从学校很慢的教务系统获取数据，有时可能会比较慢，请耐心等待和重试，您可以选择点击自动评教让系统帮你全部评完，默认好评，也可以点击单门课程进入对每位教师评价，已经评教过的课程会打勾，评教完成打勾的课程在自动评教时将会忽略处理，请等待评教完成界面出现时再退出本功能，谢谢合作')
	},

	onPullDownRefresh: () => {
		wx.showModal({
			title: '是否同步？',
			content: '将同步您最新的需评教列表，如果未到评教时间无需同步哦，不同步情况下两个小时后也会自动加载最新的评教列表，是否需要现在同步？',
			success: (res) => {
				if(res.confirm) {
					util.showLoadingPanel(p, '同步中')
					p.getEvaluationData({
						sync: true,
						success: (evaluationData) => {
							p.setEvaluationData(evaluationData)
						},
						fail: (code, msg) => {
							util.hideLoadingPanel(p)
							util.error('同步失败', util.errorHandler(code, msg), wx.navigateBack)
						}
					})
				}
				else
					wx.stopPullDownRefresh()
			}
		})
	},

	onShareAppMessage: function () {
		return {
			title: '懒得上网站评教？为何不试一下创新盒子的自动评教呢？',
			path: '/pages/evaluationTch/evaluationTch'
		}
	}

})
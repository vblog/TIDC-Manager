let p
const util = require('../../utils/util')

Page({

	data: {
		//正在获取课程列表
		gettingCourse: false,
		gettingSportsCourse: false,
		//等待开始抢课
		waitGrabCourse: false,
		waitGrabSportsCourse: false,
		//正在抢课
		courseGrabing: false,
		sportsCourseGrabing: false,
		chooseCourseData: {},
		chooseSportsCourseData: {},
		waitTime: '',
		grabCourseTab: 0,
		grabCourseList: [],
		sportsGrabCourseList: [],
		grabCourseListLoaded: false,
		sportsGrabCourseListLoaded: false
	},

	temp: {
		courseIds: [],
		sportsCourseIds: [],
		refreshCourseTimer: null,
		refreshSportsCourseTimer: null
	},

	onLoad: function (options) {
		p = this
		util.modal('* 这是实验性功能 *', '由于教务系统的不稳定性，所以本抢课功能不保证百分百抢到你需要的课，本功能旨在提高抢课的成功率，省去人工抢课的麻烦，预约后系统每一分钟会更新一次抢课状态。')
		util.showLoadingBox(p)
		p.loadGrabCourse()
	},

	onUnload: () => {
		util.unloadPage('grabCourse')
	},

	//页面卸载时停止自动更新状态
	onUnload: () => {
		clearInterval(p.temp.refreshCourseTimer)
		clearInterval(p.temp.refreshSportsCourseTimer)
	},

	changeCourseType: (e) => {
		const index = e.detail.current || e.currentTarget.dataset.index || 0
		const { grabCourseListLoaded, sportsGrabCourseListLoaded } = p.data
		if (index == 0 && !grabCourseListLoaded)
			p.loadGrabCourse()
		else if (index == 1 && !sportsGrabCourseListLoaded)
			p.loadSportsGrabCourse()
		p.setData({
			grabCourseTab: index
		})
	},

	loadGrabCourse: () => {
		if (p.data.gettingCourse)
			return
		p.setData({
			gettingCourse: true
		})
		p.getGrabCourse({
			success: (data) => {
				const { grabCourseList, choosedCourseList } = data
				let choosedData = {}
				for (let appt of choosedCourseList) {
					const { courseId, apptStatus, apptDate } = appt
					choosedData[courseId] = { apptStatus, apptDate }
				}
				if(choosedData.length > 0) {
					clearInterval(p.temp.refreshCourseTimer)
					p.temp.refreshCourseTimer = setInterval(p.loadGrabCourse, 60000)
				}
				p.setData({
					grabCourseList,
					chooseCourseData: choosedData,
					grabCourseListLoaded: true,
					gettingCourse: false
				})
				wx.stopPullDownRefresh()
			},
			fail: (code, msg) => {
				wx.stopPullDownRefresh()
				util.error('加载抢课课程列表失败', util.errorHandler(code, msg))
			}
		})
	},

	loadSportsGrabCourse: () => {
		if (p.data.gettingSportsCourse)
			return
		p.setData({
			gettingSportsCourse: true
		})
		p.getSportsGrabCourse({
			success: (data) => {
				const { sportsGrabCourseList, choosedSportsCourseList } = data
				let choosedData = {}
				for (let appt of choosedSportsCourseList) {
					const { courseId, apptStatus, apptDate } = appt
					choosedData[courseId] = { apptStatus, apptDate }
				}
				if (choosedData.length > 0) {
					clearInterval(p.temp.refreshSportsCourseTimer)
					p.temp.refreshSportsCourseTimer = setInterval(p.loadSportsGrabCourse, 60000)
				}
				p.setData({
					sportsGrabCourseList,
					chooseSportsCourseData: choosedData,
					sportsGrabCourseListLoaded: true,
					gettingSportsCourse: false
				})
				wx.stopPullDownRefresh()
			},
			fail: (code, msg) => {
				wx.stopPullDownRefresh()
				util.error('加载抢课课程列表失败', util.errorHandler(code, msg))
			}
		})
	},

	getGrabCourse: (obj) => {
		const {success, fail} = obj
		const {majorId} = util.globalData('userData')
		util.request({
			source: 'grabCourse',
			method: 'get',
			uri: 'grab/course',
			data: {
				majorId,
				session: util.globalData('session')
			},
			success,
			fail
		})
	},

	getSportsGrabCourse: (obj) => {
		const { success, fail } = obj
		const { majorId } = util.globalData('userData')
		util.request({
			source: 'grabCourse',
			method: 'get',
			uri: 'user/student/grabCourse',
			data: {
				majorId,
				session: util.globalData('session')
			},
			success,
			fail
		})
	},

	chooseCourse: (e) => {
		p.temp.courseIds = e.detail.value
	},

	chooseSportsCourse: (e) => {
		p.temp.sportsCourseIds = e.detail.value
	},

	submitGrabCourseAppt: () => {
		const courseIds = p.temp.courseIds
		if (courseIds.length == 0) {
			util.error('勾选课程', '请勾选需要抢的选修课，再点击预约哦~')
			return
		}
		util.loading('预约中')
		util.request({
			method: 'post',
			uri: 'grab/course/appt',
			data: {
				courseIds,
				majorId: 'faae476564e0ff38d309283377db6adf',
				session: util.globalData('session')
			},
			success: (data) => {
				const { chooseCourseData, courseGrabing } = p.data
				for (let courseId of data) {
					chooseCourseData[courseId] = {
						apptStatus: courseGrabing ? 1 : 0,
						apptDate: parseInt(Date.now() / 1000)
					}
				}
				p.setData({
					chooseCourseData
				})
				wx.hideToast()
			},
			fail: (code, msg) => {
				wx.hideToast()
				util.error('预约失败', util.errorHandler(code, msg))
			}
		})
	},
	
	submitGrabSportsCourseAppt: () => {
		const courseIds = p.temp.sportsCourseIds
		if (courseIds.length == 0) {
			util.error('勾选课程', '请勾选需要抢的体育课，再点击预约哦~')
			return
		}
		util.loading('预约中')
		util.request({
			method: 'post',
			uri: 'grab/sportsCourse/appt',
			data: {
				courseIds,
				session: util.globalData('session')
			},
			success: (data) => {
				const { chooseSportsCourseData, sportsCourseGrabing } = p.data
				for (let courseId of data) {
					chooseSportsCourseData[courseId] = {
						apptStatus: sportsCourseGrabing ? 1 : 0,
						apptDate: parseInt(Date.now() / 1000)
					}
				}
				p.setData({
					chooseSportsCourseData
				})
				wx.hideToast()
			},
			fail: (code, msg) => {
				wx.hideToast()
				util.error('预约失败', util.errorHandler(code, msg))
			}
		})
	},

	cancelGrabCourseAppt: () => {

	},

	cancelGrabSportsCourseAppt: () => {
		
	},

	refreshGrabCourseList: () => {
		const { grabCourseTab } = p.data
		grabCourseTab == 0 ? p.loadGrabCourse() : p.loadSportsGrabCourse()
	},

	startGrab: () => {

	},

	stopGrab: () => {

	},

	onPullDownRefresh: () => {
		p.refreshGrabCourseList()
	},

	onShareAppMessage: function () {

	}

})
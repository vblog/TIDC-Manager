const util = require('../../utils/util')
let p

Page({

    data: {
		calendarUrl: '',
		loadingPanel: {
			tip: '加载中'
		}
    },

    onLoad: function(options) {
		p = this
		p.getCourseTermInfo({
			success: ({courseYear, courseTerm}) => {
				p.setData({
					calendarUrl: `${util.config('staticDomain')}image/cxxy/calendar/${courseYear}_${courseTerm}.jpg`
				})
			},
			fail: (code, msg) => {
				util.error('获取失败', util.error(code, msg), wx.switchTab({
					url: '/pages/home/home',
				}))
			}
		})
    },

	getCourseTermInfo: (obj) => {
		const {success, fail} = obj
		util.request({
			source: 'calendar',
			method: 'get',
			uri: 'sys/courseTermInfo',
			success,
			fail
		})
	},

	calendarLoaded: () => {
		util.hideLoadingPanel(p)
	},

	onShareAppMessage: () => {
		return {
			title: '创新学院日历',
			path: '/pages/calendar/calendar'
		}
	}
	
})
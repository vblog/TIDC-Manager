const util = require('../../utils/util')
let p

Page({

	data: {
		//静态资源域名
		staticDomain: util.config('staticDomain'),
		//用户数据
		userData: {

			//身份类型
			userType: 'normal',

			//用户头像
			userHead: '',

			//用户微信昵称
			userName: '点我登录',

			//绑定的身份ID
			bindId: '',

			//绑定的身份名称
			bindName: ''

		},
		bannerList: [],
		msgCount: 0,
		funList: util.config('funList'),
		loadingPanel: {
			tip: '加载中'
		}
	},

	temp: {
		openedBinder: false
	},
	
	onLoad: function (params) {
		p = this
		p.updateBanners()
		wx.getStorage({
			key: 'newMsgList',
			success: (res) => {
				if (res.data) {
					util.globalData('newMsgList', res.data)
					p.setData({ msgCount: res.data.length })
				}
			}
		})
		util.bindMsgBack('home', (msg) => {
			p.setData({
				msgCount: util.globalData('newMsgList').length
			})
			wx.vibrateLong()
		})
	},

	onShow: (params) => {
		util.userLogin({
			p,
			scene: 'load',
			success: () => {
				util.showNotices(p)
			},
			complete: (userData) => {
				util.hideLoadingPanel(p)
				p.updateUserData(userData)
			},
			fail: (title, msg) => {
				util.error(title, msg)
			},
			notAuth: () => {
				p.updateUserFunList('normal')
			}
		})
		const newMsgList = util.globalData('newMsgList')
		if (newMsgList)
			p.setData({ msgCount: newMsgList.length })
		p.temp.openedBinder = false
	},

	login: () => {
		util.userLogin({
			p,
			scene: 'touch',
			success: () => {
				util.showNotices(p)
			},
			complete: (userData) => {
				p.updateUserData(userData)
			},
			fail: (title, msg) => {
				util.error(title, msg)
			},
			notAuth: () => {
				p.updateUserFunList('normal')
			}
		})
	},

	updateUserData: (userData) => {
		console.log(userData)
		let { userType, userHead, userName, userSex, stuId, tchId, stuName, tchName } = userData
		try {
			userType = userType == '' ? 'normal' : userType
			util.globalData('binded', userType != 'normal' && userType != '')
			p.setData({
				userData: {
					userType,
					userHead,
					userName,
					userSex,
					bindId: stuId || tchId || '',
					bindName: stuName || tchName || ''
				}
			})
			p.updateUserFunList(userType)
		}
		catch (err) {
			console.error(err)
			util.globalData('logined', false)
			p.data.status.logining = false
			wx.stopPullDownRefresh()
			util.error('数据错误', '登录数据错误，请下拉重试哦')
		}
	},

	updateUserFunList: (userType) => {
		const funList = util.config('funList')
		p.setData({
			funList: funList.filter((fun) => {
				return userType == 'normal' || fun.allow.indexOf(userType) != -1
			})
		})
	},

	//检查新公告
	checkNotice: () => {
		util.checkNotice(p)
	},
	
	//前往幻灯片指向的内容
	goSlide: (e) => {
		let { type, id } = e.currentTarget.dataset
	},

	//前往绑定用户身份页面
	goUserBinder: () => {
		util.userLogin({
			p,
			scene: 'touch',
			success: () => {
				util.showNotices(p)
			},
			complete: (userData) => {
				if (userData.userType != p.data.userData.userType) {
					p.updateUserData(userData)
					return
				}
				else
					p.updateUserData(userData)
				switch (p.data.userData.userType) {
					case 'student':
						wx.showModal({
							title: '学生身份已认证',
							content: '您已认证在创新学院的学生身份，创新盒子将尽力为您提供最好的校园服务，如果您希望解除绑定请点击【解除绑定】',
							confirmText: '解除绑定',
							confirmColor: '#ff6759',
							success: (res) => {
								if (res.confirm)
									p.uniteUser()
							}
						})
						break
					case 'teacher':
						wx.showModal({
							title: '教职工身份已认证',
							content: '您已认证在创新学院的教职工身份，创新盒子将尽力为您提供最好的校园服务，如果您希望解除绑定请点击【解除绑定】',
							confirmText: '解除绑定',
							confirmColor: '#ff6759',
							success: (res) => {
								if (res.confirm)
									p.uniteUser()
							}
						})
						break
					default:
						if (p.temp.openedBinder)
							return
						p.temp.openedBinder = true
						util.goPage('userBinder')
				}
			},
			fail: (title, msg) => {
				util.error(title, msg)
			}
		})
	},

	//前往云抢课
	goGrab: () => {
		if (!p.checkBind()) {
			util.tip({ p, text: '请先绑定登录，才能使用此功能' })
			setTimeout(p.goUserBinder, 800)
			return
		}
		util.goPage('grabCourse')
	},

	//前往一卡通
	goCard: () => {
		if (!p.checkBind()) {
			util.tip({ p, text: '请先绑定登录，才能使用此功能' })
			setTimeout(p.goUserBinder, 800)
			return
		}
		util.goPage('card')
	},

	//前往图书馆
	goLibrary: () => {
		if (!p.checkBind()) {
			util.tip({ p, text: '请先绑定登录，才能使用此功能' })
			setTimeout(p.goUserBinder, 800)
			return
		}
		util.goPage('library')
	},

	//前往树洞
	goSecret: () => {
		if (!p.checkBind()) {
			util.tip({ p, text: '请先绑定登录学生或教师用户，才能使用此功能' })
			setTimeout(p.goUserBinder, 800)
			return
		}
		util.modal('敬请期待', '树洞功能正在开发中，敬请期待！')
	},

	//前往报修
	goRepair: () => {
		if (!p.checkBind()) {
			util.tip({ p, text: '请先绑定登录学生或教师用户，才能使用此功能' })
			setTimeout(p.goUserBinder, 800)
			return
		}
		util.goPage('repair')
	},

	//前往点名单
	goCallName: () => {
		if (!p.checkBind()) {
			util.tip({ p, text: '请先绑定登录教师用户，才能使用此功能' })
			setTimeout(p.goUserBinder, 800)
			return
		}
		util.goPage('callName')
	},

	//前往成绩查询
	goScore: () => {
		if (!p.checkBind()) {
			util.tip({ p, text: '请先绑定登录，才能使用此功能' })
			setTimeout(p.goUserBinder, 800)
			return
		}
		util.goPage('score')
	},

	//前往失物招领
	goLost: () => {
		if (!p.checkBind()) {
			util.tip({ p, text: '请先绑定登录，才能使用此功能' })
			setTimeout(p.goUserBinder, 800)
			return
		}
		util.goPage('lostFound')
	},

	//前往关联课表
	goCorrelSyllabus: () => {
		if (!p.checkBind()) {
			util.tip({ p, text: '请先绑定登录，才能使用此功能' })
			setTimeout(p.goUserBinder, 800)
			return
		}
		util.goPage('correlSyllabus')
	},

	//前往志愿服务
	goVolunteer: () => {
		if (!p.checkBind()) {
			util.tip({ p, text: '请先绑定登录，才能使用此功能' })
			setTimeout(p.goUserBinder, 800)
			return
		}
		util.modal('敬请期待', '志愿服务功能正在开发中，敬请期待！')
	},

	//前往兼职平台
	goPathTime: () => {
		if (!p.checkBind()) {
			util.tip({ p, text: '请先绑定登录，才能使用此功能' })
			setTimeout(p.goUserBinder, 800)
			return
		}
		util.modal('敬请期待', '兼职平台功能正在开发中，敬请期待！')
	},

	//前往考试报名
	goEnroll: () => {
		if (!p.checkBind()) {
			util.tip({ p, text: '请先绑定登录，才能使用此功能' })
			setTimeout(p.goUserBinder, 800)
			return
		}
		util.modal('敬请期待', '报名功能正在开发中，敬请期待！')
	},

	//前往活动报名
	goActive: () => {
		util.goPage('active')
	},

	//前往速评教
	goEvaluation: () => {
		if (!p.checkBind()) {
			util.tip({ p, text: '请先绑定登录，才能使用此功能' })
			setTimeout(p.goUserBinder, 800)
			return
		}
		util.goPage('evaluationTch')
	},

	//前往校日历
	goCalendar: () => {
		if (!p.checkBind()) {
			util.tip({ p, text: '请先绑定登录，才能使用此功能' })
			setTimeout(p.goUserBinder, 800)
			return
		}
		util.goPage('calendar')
	},

	//前往我要反馈页面
	goFeedback: () => {
		util.goPage('feedback')
	},

	//前往关于页面
	goAbout: () => {
		util.goPage('about')
	},

	//前往我的消息
	goMessage: () =>{
		if (!p.checkBind()) {
			util.tip({ p, text: '请先绑定登录，才能使用此功能' })
			setTimeout(p.goUserBinder, 800)
			return
		}
		util.goPage('message')
	},

	//前往天方夜谈
	goStory: () => {
		
	},

	//判断是否未登录或未绑定
	checkBind: () => {
		return util.globalData('logined') && p.data.userData.userType != 'normal'
	},

	//解除绑定
	uniteUser: () => {
		wx.showModal({
			title: '确认解除绑定',
			content: '真的要解除您与创新盒子的绑定吗？解除后您将无法获得更便捷的校园服务，且您与其它人的关联关系将自动解除，请慎重操作',
			confirmColor: '#ff6759',
			confirmText: '再考虑下',
			cancelText: '残忍解绑',
			success: (res) => {
				if(res.cancel) {
					util.loading('解绑中')
					util.request({
						source: 'home',
						method: 'delete',
						uri: `user/${util.getUserType()}`,
						data: {
							session: util.globalData('session')
						},
						success: () => {
							const userData = p.data.userData
							userData.userType = 'normal'
							userData.bindId = ''
							userData.bindName = ''
							util.globalData('notReconnect', true)
							wx.closeSocket({
								fail: (err) => {console.error(err)}
							})
							p.updateUserData(userData)
							wx.removeStorage({
								key: '',
								success: function(res) {},
							})
							util.globalData('userData', userData)
							wx.hideToast()
							util.tip({
								p,
								text: '解绑成功'
							})
						},
						fail: (code, msg) => {
							util.error('解绑失败', util.errorHandler(code, msg))
						}						
					})			
				}
			}
		})
	},

	checkBanners(callback) {
		wx.getStorage({
			key: 'banners',
			success: (res) => {
				callback(res.data)
			},
			fail: (err) => {
				callback(null)
			}
		})
	},

	updateBanners() {
		p.checkBanners((banners) => {
			if(banners)
				p.setData({ bannerList: banners })
			util.request({
				source: 'home',
				method: 'get',
				uri: 'sys/banner',
				success: (data) => {
					if (!banners || JSON.stringify(banners) != JSON.stringify(data)) {
						p.setData({ bannerList: data || [] })
						wx.setStorage({
							key: 'banners',
							data: data,
						})
					}
				},
				fail: (err) => {
					console.error('load banner failed:', err)
				},
				sign: false,
				encrypt: false
			})
		})
	},

	//跳转页面
	goPage: (e) => {
		const page = e.currentTarget.dataset.page
		if(page)
			util.goPage(page)
	},

	//下拉刷新
	onPullDownRefresh: () => {
		util.userLogin({
			p,
			success: () => {
				util.showNotices(p)
			},
			complete: (userData) => {
				p.updateUserData(userData)
			},
			fail: (title, msg) => {
				util.error(title, msg)
			}
		})
	},

	//转发共享
	onShareAppMessage: () => {
		return {
			title: '在创新不用创新盒子就Out啦！各种校园服务不容错过！',
			path: '/pages/home/home'
		}
	}

})
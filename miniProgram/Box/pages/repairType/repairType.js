const util = require('../../utils/util')
let p

Page({

	data: {
		repairTypeList: [],
		loadingPanel: {
			tip: '获取中'
		}
	},

	onLoad: function () {
		p = this
		p.getRepairType({
			success: (repairTypeList) => {
				p.setRepairType(repairTypeList)
			},
			fail: (code, msg) => {
				util.error('获取失败', util.errorHandler(code, msg), wx.navigateBack)
			}
		})
	},

	onUnload: () => {
		util.unloadPage('repairType')
	},

	getRepairType: (obj) => {
		const {success, fail} = obj
		util.request({
			source: 'repairType',
			method: 'get',
			uri: 'sys/repair/type',
			data: {
				session: util.globalData('session')
			},
			success,
			fail
		})
	},

	setRepairType: (repairTypeList) => {
		p.setData({
			repairTypeList
		})
		util.hideLoadingPanel(p)
	},

	chooseRepairType: (e) => {
		const {pidx, cid} = e.currentTarget.dataset
		const list = p.data.repairTypeList
		util.globalData('repairTypesTemp', list)
		util.globalData('repairTypeTemp', {
			id: `${list[pidx].i}-${cid}`,
			name: `${list[pidx].n}-${list[pidx].l[cid]}`
		})
		wx.navigateBack()
	}

})
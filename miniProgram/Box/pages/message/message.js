const util = require('../../utils/util')
let p

Page({

	data: {
		msgList: [],
		msgLoaded: false,
		loadingPanel: {
			tip: '读取中'
		}
	},

	onLoad: function() {
		p = this
		const newMsgList = util.globalData('newMsgList') || []
		wx.getStorage({
			key: 'msgList',
			success: (res) => {
				let msgList = res.data
				let mergeList = []
				if (newMsgList.length > 0)
					mergeList = newMsgList.concat(msgList)
				else
					mergeList = msgList
				mergeList = mergeList.sort((a, b) => {
					return b.time - a.time
				})
				wx.setStorage({
					key: 'msgList',
					data: mergeList,
				})
				util.globalData('msgList', mergeList)
				util.globalData('newMsgList', [])
				p.setData({
					msgList: mergeList,
					msgLoaded: true
				})
				util.hideLoadingPanel(p)
			},
			fail: () => {
				if (newMsgList.length == 0) {
					p.setData({
						msgLoaded: true
					})
					util.hideLoadingPanel(p)
					return
				}
				const mergeList = newMsgList.sort((a, b) => {
					return b.time - a.time
				})
				util.globalData('msgList', mergeList)
				p.setData({
					msgList: mergeList
				})
				wx.setStorage({
					key: 'msgList',
					data: mergeList,
				})
				util.globalData('newMsgList', [])
				util.hideLoadingPanel(p)
			}
		})
		wx.removeStorage({
			key: 'newMsgList'
		})
		util.bindMsgBack('message', (msg) => {
			let msgList = util.globalData('msgList')
			const newMsgList = util.globalData('newMsgList')
			const mergeList = newMsgList.concat(msgList)
			util.globalData('newMsgList', [])
			util.globalData('msgList', mergeList)
			p.setData({
				msgList: mergeList
			})
			wx.setStorage({
				key: 'newMsgList',
				data: [],
			})
			wx.setStorage({
				key: 'msgList',
				data: mergeList
			})
		})
	},// 1601050431 164174

	onUnload: () => {
		util.uniteMsgBack('message')
		util.unloadPage('enrollQuery')
	},

	resolveCorrelSyllabus: (e) => {
		const msgId = e.currentTarget.dataset.id
		util.loading('发送中')
		util.request({
			source: 'message',
			method: 'put',
			uri: `user/student/syllabus/correl/${msgId}/resolve`,
			data: {
				session: util.globalData('session')
			},
			success: () => {
				p.updateMsgStatus(msgId, 1)
			},
			fail: (code, msg) => {
				//判断是否是已处理消息
				if (code == '-2057') {
					p.updateMsgStatus(msgId, msg)
					util.modal('已处理', '你已经处理了这条消息噢')
					return
				}
				if (code == '-2059') {
					p.updateMsgStatus(msgId, 1)
					util.modal('已关联', '你已经成功关联Ta的课程表，无需重复接受')
					return
				}
				util.modal('接受失败', util.errorHandler(code, msg))
			}
		})
	},

	rejectCorrelSyllabus: (e) => {
		const msgId = e.currentTarget.dataset.id
		util.loading('发送中')
		util.request({
			source: 'message',
			method: 'put',
			uri: `user/student/syllabus/correl/${msgId}/reject`,
			data: {
				session: util.globalData('session')
			},
			success: () => {
				p.updateMsgStatus(msgId, 2)
			},
			fail: (code, msg) => {
				//判断是否是已处理消息
				if (code == '-2057') {
					p.updateMsgStatus(msgId, msg)
					util.modal('已处理', '你已经处理了这条消息噢')
					return
				}
				util.modal('拒绝失败', util.errorHandler(code, msg))
			}
		})
	},

	updateMsgStatus: (msgId, status) => {
		let { msgList } = p.data
		msgList.forEach((msg, index) => {
			if(msg.id == msgId) {
				msgList[index].status = status
				util.globalData('msgList', p.data.msgList)
				p.setData({
					msgList: p.data.msgList
				})
				wx.setStorage({
					key: 'msgList',
					data: p.data.msgList
				})
				wx.hideToast()
				return
			}
		})
	},

	goCorrelSyllabus: () => {
		util.goPage('correlSyllabus')
	}

})
let util = require('../../utils/util')
let p

Page({

    data: {
        feedBackContent: '',
        feedBackContact: '',
        submitting: false
    },

    onLoad: function (options) {
        p = this
    },

	onUnload: () => {
		util.unloadPage('feedback')
	},

    feedBackContentInput: (e) => {
        p.data.feedBackContent = e.detail.value
    },

    feedBackContactInput: (e) => {
        p.data.feedBackContact = e.detail.value
    },

    submitFeedBack: () => {
        let { feedBackContent, feedBackContact } = p.data
		feedBackContent = feedBackContent.trim()
		feedBackContact = feedBackContact.trim()
        if (feedBackContent == '' || feedBackContact == '') {
            util.error('未填写完整', '请输入反馈内容和联系方式噢')
            return
        }
		if (!/^([a-zA-Z][-_a-zA-Z0-9]{5,19})|(1[3-9]\d{9})$/.test(feedBackContact) && !/^1[3-9]\d{9}$/.test(feedBackContact)) {
			util.error('联系方式错误', '请输入正确的微信号或手机号哦')
			return
		}
        p.setData({
            submitting: true
        })
        util.request({
			source: 'feedback',
            method: 'POST',
            uri: 'user/feedback',
            data: {
				content: feedBackContent,
				contact: feedBackContact
            },
            success: () => {
				util.success('提交成功', 1000)
				setTimeout(() => {
					wx.switchTab({
						url: '/pages/home/home',
					})
				}, 800)
            },
            fail: (err) => {
                console.error(err)
				p.setData({
					submitting: false
				})
                util.error('提交失败', '提交反馈失败啦，请重试哦')
            }
        })
    },

	backToHome: () => {
		wx.showModal({
			title: '确认离开？',
			content: '我们很重视您的反馈，如果有宝贵的建议和反馈请及时提交给我们，我们将会联系您处理，确认返回主页吗？',
			confirmText: '返回主页',
			success: (res) => {
				if(res.confirm)
					wx.switchTab({
						url: '/pages/home/home',
					})
			}
		})
	},

	onShareAppMessage: function () {
		return {
			title: '创新校园汇邀您献出宝贵的意见',
			path: '/pages/home/home?go=feedback'
		}
	}

})
const util = require('../../utils/util')
let p

Page({

	data: {

	},

	onLoad: function (options) {

	},

	getUserInfo: (e) => {
		console.log(e)
		if (e.detail.errMsg.indexOf('auth deny') != -1) {
			util.error('授权失败', '点击立即授权后要点击允许才能授权成功噢')
		}
		else
			wx.switchTab({
				url: '/pages/home/home',
			})
	}

})
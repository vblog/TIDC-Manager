const util = require('../../utils/util')
let p

Page({

    data: {
		mode: 1,
		courseList: [],
		classList: [],
		studentList: [],
		chooseAll: false,
		classSelectorOpened: true
    },

	temp: {
		classId: ''
	},

    onLoad: function(params) {
		p = this
		p.getTchTodayCourse({
			success: (data) => {
				console.log(data)
			},
			fail: (code, msg) => {
				util.error('获取课程列表失败', util.errorHandler(code, msg))
			}
		})
		p.getTchClassList({
			success: (classList) => {
				p.setTchClassList(classList)
			},
			fail: (code, msg) => {
				util.error('获取列表失败', util.errorHandler(code, msg))
			}
		})
    },

	getTchTodayCourse: ({success, fail}) => {
		util.request({
			source: 'callName',
			method: 'get',
			uri: 'user/teacher/course/today',
			data: {
				session: util.globalData('session')
			},
			success,
			fail: (code, msg) => {
				util.hideLoadingPanel(p)
				fail(code, msg)
			}
		})
	},

	getTchClassList: ({ success, fail }) => {
		util.showLoadingPanel(p, '获取中')
		util.request({
			method: 'get',
			uri: 'user/teacher/classes',
			source: 'callName',
			data: {
				session: util.globalData('session')
			},
			success,
			fail: (code, msg) => {
				util.hideLoadingPanel(p)
				fail(code, msg)
			}
		})
	},

	setTchClassList: (classList) => {
		p.setData({
			classList
		})
		util.hideLoadingPanel(p)
	},

	getClassData: (classId) => {
		for(let d of p.data.classList) {
			if(d.classId == classId)
				return d
		}
	},

	loadClassStudentList: (e) => {
		const classId = e.currentTarget.dataset.id
		p.getClassStudentList({
			classId,
			success: (studentList) => {
				p.setClassStudentList(classId, studentList)
			},
			fail: (code, msg) => {
				util.error('获取名单失败', util.errorHandler(code, msg))
			}
		})
	},

	getClassStudentList: (obj) => {
		const {classId, success, fail} = obj
		util.showLoadingPanel(p, '获取中')
		util.request({
			method: 'get',
			uri: `user/teacher/class/${classId}/students`,
			data: {
				session: util.globalData('session')
			},
			source: 'callName',
			success: (studentList) => {
				success(studentList)
			},
			fail: (code, msg) => {
				util.hideLoadingPanel(p)
				fail(code, msg)
			}
		})
	},

	setClassStudentList: (classId, studentList) => {
		const classData = p.getClassData(classId)
		wx.setNavigationBarTitle({
			title: `${classData.classGrade}${classData.className} (${studentList.length}人)`,
		})
		p.temp.classId = classId
		p.setData({
			studentList,
			classSelectorOpened: false
		})
		util.hideLoadingPanel(p)
	},

	chooseMode: () => {
		wx.showActionSheet({
			itemList: ['出勤', '迟到', '旷课', '请假', '早退'],
			success: (res) => {
				if (!res.tapIndex && res.tapIndex != 0)
					return
				p.setData({
					chooseAll: false,
					mode: res.tapIndex + 1
				})
				
			}
		})
	},

	chooseAll: () => {
		for(let index in p.data.studentList) {
			if (p.data.chooseAll)
				p.data.studentList[index].mode = 0
			else
				p.data.studentList[index].mode = p.data.mode
			if(p.data.studentList[index].stuSuspend)
				p.data.studentList[index].mode = 6
		}
		p.setData({
			chooseAll: !p.data.chooseAll,
			studentList: p.data.studentList
		})
	},

	choose: (e) => {
		const stuIndex = e.currentTarget.dataset.index
		const { mode, studentList } = p.data
		if (studentList[stuIndex].stuSuspend) {
			wx.showModal({
				title: '停课状态',
				content: '此学生被标记为停课状态，是否解除标记？',
				confirmText: '解除',
				confirmColor:  '#ff6759',
				success: (res) =>{
					if(res.confirm)
						p.updateStuSuspendStatus({
							stuId: studentList[stuIndex].stuId,
							status: 0,
							success: () => {
								util.success('解除成功')
								studentList[stuIndex].stuSuspend = false
								p.setData({
									studentList
								})
							},
							fail: (code, msg) => {
								util.error('解除失败', util.errorHandler(code, msg))
							}
						})
				}
			})
			return
		}
		p.data.studentList[stuIndex].mode = studentList[stuIndex].mode == mode ? 0 : mode
		p.setData({
			studentList: p.data.studentList
		})
	},

	showSuspendPanel: (e) => {
		const stuIndex = e.currentTarget.dataset.index
		const { studentList } = p.data
		if (studentList[stuIndex].stuSuspend)
			return
		wx.showActionSheet({
			itemList: ['标记所选学生为停课状态'],
			success: (res) => {
				if(res.tapIndex == 0)
					p.updateStuSuspendStatus({
						stuId: studentList[stuIndex].stuId,
						status: 1,
						success: () => {
							util.success('标记成功')
							studentList[stuIndex].stuSuspend = true
							p.setData({
								studentList
							})
						},
						fail: (code, msg) => {
							util.error('标记失败', util.errorHandler(code, msg))
						}
					})
			}
		})
	},

	updateStuSuspendStatus: ({ stuId, status, success, fail }) => {
		util.request({
			source: 'repair',
			method: 'put',
			uri: `user/student/${stuId}/suspend/${status}`,
			data: {
				session: util.globalData('session')
			},
			success,
			fail
		})
	},

	getCallModeStudentIds: (studentList) => {
		let data = { ignore: [], sign: [], late: [], absent: [], leave: [], earlyLeave: [], suspend: [] }
		for (let s of studentList) {
			data[['ignore', 'sign', 'late', 'absent', 'leave', 'earlyLeave', 'suspend'][s.mode]].push(s.stuId)
		}
		return data
	},

	submitCallName: () => {
		const { studentList } = p.data
		const { ignore, sign, late, absent, leave, earlyLeave, suspend } = p.getCallModeStudentIds(studentList)
		wx.showModal({
			title: '确认提交？',
			content: `出勤 ${sign.length} 人，迟到 ${late.length} 人，旷课 ${absent.length} 人，请假 ${leave.length} 人，早退 ${earlyLeave.length} 人${suspend.length > 0 ? ("，停课 " + suspend.length + " 人") : ""}${ignore.length > 0 ? ("，忽略 " + ignore.length + " 人") : ""}，共 ${studentList.length} 人，确认提交此点名单吗？`,
			success: (res) => {
				if(res.confirm) {
					util.request({
						method: 'post',
						uri: 'user/teacher/callName',
						data: {
							classId: p.temp.classId,
							callNameResult: {
								ignore,
								sign,
								late,
								absent,
								leave,
								earlyLeave,
								suspend
							},
							session: util.globalData('session')
						},
						success: (data) => {
							console.log(data)
						},
						fail: (code, msg) => {
							util.error('提交失败', util.errorHandler(code, msg))
						}
					})
				}
			}
		})
	},

	syncTchClassList: ({success, fail}) => {
		wx.stopPullDownRefresh()
		util.showLoadingPanel(p, '同步中')
		util.request({
			source: 'callName',
			method: 'put',
			uri: 'user/teacher/classes',
			data: {
				session: util.globalData('session')
			},
			success,
			fail: (code, msg) => {
				util.hideLoadingPanel(p)
				fail(code, msg)
			}
		})
	},

	onPullDownRefresh: () => {
		if (!p.data.classSelectorOpened) {
			wx.stopPullDownRefresh()
			return
		}
		wx.showModal({
			title: '操作确认',
			content: '此操作将同步最新的任课班级列表及学生列表，是否同步？',
			success: (res) => {
				if(res.confirm) {
					p.syncTchClassList({
						success: (classList) => {
							p.setTchClassList(classList)
							util.hideLoadingPanel(p)
						},
						fail: (code, msg) => {
							util.error('同步失败', util.errorHandler(code, msg))
						}
					})
				}
			}
		})
	}

})
let app = getApp()
let config = app.config
let crypto = require('./crypto')

class util {

    constructor() {
		this.msgBack = {}
        this.tipCloseTimer = null
		this.heartTimer = null
		this.wsConnTryCount = 0
		this.requestSources = []
    }

	isJson(obj) {
		try {
			return JSON.parse(obj)
		}
		catch(err) {
			console.error(err)
			return null
		}
	}

	checkAuth(callback) {
		let {auth, success, fail} = callback
		if (wx.getSetting && wx.authorize) {
			wx.getSetting({
				success: (res) => {
					res.authSetting[`scope.${auth}`] ? success() : fail()
				},
				fail: () => {
					console.error('check auth failed')
				}
			})
		}
		else {
			fail()
		}
	}

	unloadPage(pageName) {
		const sIndex = this.requestSources.indexOf(pageName)
		if (sIndex != -1)
			this.requestSources.splice(sIndex, 1)
	}

    config(key, val) {
        if(val && config[key]) {
            return config[key] = val
        }
        return config[key]
    }

	globalData(key, val) {
		try {
			if ((val || this.isString(val) || val === false) && (app.globalData[key] || this.isString(app.globalData[key]) || app.globalData[key] === null || app.globalData[key] === false)) {
				return app.globalData[key] = val
			}
			if (key.indexOf('Temp') != -1) {
				let temp = app.globalData[key]
				app.globalData[key] = null
				return temp	
			}
			return app.globalData[key]
		}
		catch (err) {
			console.error('set/get global data failed')
		}
	}

	getUserType() {
		const userData = this.globalData('userData')
		return userData ? userData.userType : 'normal'
	}

	showLoadingPanel(p, tip = '请稍候') {
		p.setData({
			loadingPanel: { tip }
		})
	}

	hideLoadingPanel(p) {
		p.setData({
			loadingPanel: { tip: false }
		})
	}

	showPanelErr(obj) {
		let { p, icon = 'failed', msg = '加载失败', use = '请下拉重试', title = '出错啦' } = obj
		wx.hideNavigationBarLoading()
		wx.stopPullDownRefresh()
		wx.setNavigationBarTitle({
			title,
		})
		p.setData({ err: { icon, msg, use } })
	}

	hidePanelErr(p) {
		if (!p.data.err)
			return
		p.setData({ err: null })
	}

	showLoadingBox(p) {
		p.setData({ loadingBox: { show: true } })
	}

	hideLoadingBox(p) {
		p.setData({ loadingBox: { show: false } })
	}

	tip(obj) {
		let { p, text, timeout = 3000, done } = obj
		clearTimeout(this.tipCloseTimer)
		p.setData({
			tip: {
				text: false
			}
		})
		p.setData({
			tip: {
				text,
				timeout
			}
		})
		this.tipCloseTimer = setTimeout(() => {
			p.setData({
				tip: {
					text: false
				}
			})
			if (done) {
				done()
			}
		}, timeout)
	}

    success(successMsg = '操作成功', timeout = 1500) {
        wx.showToast({
            title: successMsg,
            icon: 'success',
            duration: timeout,
            mask: true
        })
    }

    error(errTitle = '发生错误', errMsg, callback) {
        wx.showModal({
            title: errTitle,
            content: errMsg,
			cancelText: '反馈问题',
			cancelColor: '#ff6759',
            success: (res) => {
				if(res.cancel) {
					wx.redirectTo({
						url: '/pages/feedback/feedback',
					})
					return
				}
                if (callback)
                    callback()
            },
            fail: () => {
                if (callback)
                    callback()
            }
        })
    }

    modal(title = '系统提示', msg, callback) {
		wx.showModal({
			title,
			content: msg,
			showCancel: false,
			success: (res) => {
				if (callback)
					callback()
			},
			fail: () => {
				if (callback)
					callback()
			}
		})
    }

    iconBox(icon, title = '系统提示') {
        wx.showToast({
            title: title,
            icon: 'success',
            image: `${icon}.png`
        })
    }

    loading(title) {
        wx.showToast({
            title: title,
            icon: 'loading',
            duration: 60000,
            mask: true
        })
    }

	hideLoading() {
		wx.hideToast()
	}

	sign(data = {}) {
		let signStr = ''
		let keys = Object.keys(data)
		keys = keys.sort()
		for (let key of keys) {
			if (!isNaN(key)) {
				continue
			}
			else if (Object.prototype.toString.call(data[key]) == '[object Object]' || Object.prototype.toString.call(data[key]) == '[object Array]') {
				signStr += `&${key}=${JSON.stringify(data[key])}`
			}
			else if (typeof (data[key]) != 'undefined')
				signStr += `&${key}=${encodeURI(data[key])}`
		}
		signStr += `&token=${this.globalData('token')}`
		signStr = signStr.substring(1)
		console.log('sign str is ' + signStr)
		return crypto.md5(signStr)
	}

	buildSignUrl(uri = config.requestDomain, data = {}) {
		data.grantType = 'token'
		data.timestamp = this.timestamp()
		data.nonce = this.createRandomStr(10)
		uri += '?'
		for (let key in data) {
			uri += `${key}=${data[key]}&`
		}
		uri += `sign=${this.sign(data)}`
		return config.requestDomain + uri
	}

	stringToBuffer(str) {
		const buf = new ArrayBuffer(str.length)
		let arrayBuffer = new Uint8Array(buf)
		for (let i = 0; i < str.length; i++) {
			arrayBuffer[i] = str.charCodeAt(i);
		}
		return arrayBuffer
	}

	request(obj, retryNum) {
		const key = this.createRandomStr(16)
		console.log('加密key', key)
		let { source, method, uri, data = {}, success, fail, complete = () => { }, sign, encrypt = true } = obj
		try {
			let requestData = {}
			if (encrypt && data) {
				console.log('加密数据', data)
				const iv = this.createRandomStr(16)
				requestData.iv = wx.arrayBufferToBase64(this.stringToBuffer(iv))
				requestData.encryptData = crypto.encryptAES(JSON.stringify(data), key, iv)
				requestData.encryptKey = crypto.encryptRSA(key, config.publicKey)
			}
			else
				requestData = data
			if (sign !== false) {
				Object.assign(requestData, {
					grantType: 'token',
					timestamp: this.timestamp(),
					nonce: this.createRandomStr(10),
					session: this.globalData('session')
				})
				requestData.sign = this.sign(requestData)
			}
			this.requestSources.push(source)
			wx.request({
				url: config.requestDomain + uri,
				method: method,
				dataType: 'json',
				data: requestData,
				success: (res) => {
					console.log(this.requestSources, source)
					const sIndex = this.requestSources.indexOf(source)
					if (sIndex == -1)
						return
					this.requestSources.splice(sIndex, 1)
					if (res.data.status == 'success') {
						//如果存在加密数据则解密先
						if (res.data.encryptData && res.data.iv) {
							const decryptData = crypto.decryptAES(res.data.encryptData, key, res.data.iv)
							const temp = this.isJson(decryptData)
							res.data = temp ? temp : decryptData
							console.log('解密数据：', res.data)
						}
						success(res.data.data)
						complete()
					}
					else {
						let { code, msg } = res.data
						console.error(code, msg)
						if (code == '-1004' && uri != 'user/checkLogin') {
							this.startLogin({
								success: () => {
									console.log(retryNum)
									if (!retryNum && retryNum != 0)
										retryNum = 0
									else if (retryNum == 2) {
										complete()
										fail(code, msg)
										return
									}
									console.log(retryNum)
									delete obj.data.sign
									this.request(obj, retryNum++)
								},
								fail: () => {
									this.error('登录失败', '登录您的微信用户失败，请稍后重试')
									fail(code, msg)
									complete()
								}
							})
						}
						else {
							fail(code, msg)
							complete()
						}
					}
				},
				fail: (err) => {
					const sIndex = this.requestSources.indexOf(source)
					if (sIndex == -1)
						return
					this.requestSources.splice(sIndex, 1)
					console.error(err)
					fail('-2', 'request failed', err)
					complete()
				}
			})
		}
		catch (err) {
			console.error('create request failed', err)
			fail('-1', 'create request failed')
			complete()
		}
	}

	uploadFile(obj, retryNum) {
		const { uri, data, name, file, sign, success, fail } = obj
		try {
			if (sign !== false) {
				data.grantType = 'token'
				data.timestamp = this.timestamp()
				data.nonce = this.createRandomStr(10)
				data.sign = this.sign(data)
			}
			wx.uploadFile({
				url: config.requestDomain + uri,
				filePath: file,
				name,
				formData: data,
				success: (res) => {
					const result = this.isJson(res.data)
					if (!data) {
						fail('-2', 'upload failed')
						return
					}
					if (result.status == 'success') {
						success(result.data)
					}
					else {
						let { code, msg } = result
						console.error(code, msg)
						if (code == '-1004' && uri != 'user/checkLogin') {
							this.startLogin({
								success: () => {
									console.log(retryNum)
									if (!retryNum && retryNum != 0)
										retryNum = 0
									else if (retryNum == 2) {
										fail(code, msg)
										return
									}
									console.log(retryNum)
									delete obj.data.sign
									this.uploadFile(obj, retryNum++)
								},
								fail: () => {
									fail(code, msg)
								}
							})
						}
						else
							fail(code, msg)
					}
				},
				fail: (err) => {
					console.error(err)
					fail('-2', 'upload failed')
				}
			})
		}
		catch (err) {
			console.error('create upload failed', err)
			fail('-1', 'create upload failed')
		}
	}

	goPage(pageName, data, notBack = false) {
		let uri = ''
		for (let key in data) {
			uri += `&${key}=${data[key]}`
		}
		uri = uri != '' ? ('?' + uri.substr(1, uri.length - 1)) : ''
		if (!notBack)
			wx.navigateTo({
				url: `/pages/${pageName}/${pageName}${uri}`,
				fail: (err) => {
					console.error(err)
					this.error('跳转失败', '悲剧了，跳转到该页面失败，请返回上一层')
				}
			})
		else
			wx.redirectTo({
				url: `/pages/${pageName}/${pageName}${uri}`,
				fail: (err) => {
					console.error(err)
					this.error('跳转失败', '悲剧了，跳转到该页面失败，请返回上一层')
				}
			})
	}

    getRange(lat1 = 0, lng1 = 0, lat2 = 0, lng2 = 0, unitConvert = false) {
        const r = 6378137
        let rad1 = lat1 * Math.PI / 180.0
        let rad2 = lat2 * Math.PI / 180.0
        let a = rad1 - rad2
        let b = lng1 * Math.PI / 180.0 - lng2 * Math.PI / 180.0
        let range = r * 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(rad1) * Math.cos(rad2) * Math.pow(Math.sin(b / 2), 2)))
        if (unitConvert) {
            if (range < 1000) {
                range = `${parseInt(range)}m`
            }
            else {
                range = `${parseFloat((range / 1000).toFixed(1))}km`
            }
        }
        return range
    }

	timestamp() {
		return parseInt(Date.now() / 1000)
	}

	timestamp2str(timestamp, year = true, month = true, day = true, hours, min, second, split = '-') {
		let date = new Date(timestamp * 1000)
		let _year = `${year ? (date.getFullYear() > 9 ? date.getFullYear() : '0' + date.getFullYear()) + split : ''}`
		let _month = `${month ? (date.getMonth() + 1 > 9 ? date.getMonth() + 1 : '0' + (date.getMonth() + 1)) + split : ''}`
		let _day = `${day ? (date.getDate() > 9 ? date.getDate() : '0' + date.getDate()) : ''} `
		let _hours = `${hours ? (date.getHours() > 9 ? date.getHours() : '0' + date.getHours()) + ':' : ''}`
		let _min = `${min ? (date.getMinutes() > 9 ? date.getMinutes() : '0' + date.getMinutes()) + ':' : ''}`
		let _second = `${second ? (date.getSeconds() > 9 ? date.getSeconds() : '0' + date.getSeconds()) : ''}`
		return `${_year}${_month}${_day}${_hours}${_min}${_second}`
	}

    isString(val) {
        return Object.prototype.toString.call(val) === '[object String]'
    }

	createRandomStr(len, material) {
        let randomStr = ''
		material = material || 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        for (var i = 0; i < len; i++) {
            //添加在素材字符串随机找到的字符
			randomStr += material[parseInt(Math.random() * material.length)];
        }
        return randomStr
    }

	md5(data) {
		return crypto.md5(data)
	}

	errorHandler(code = '-1', msg='未知错误') {
		wx.hideToast()
		wx.hideNavigationBarLoading()
		wx.stopPullDownRefresh()
		if (app.errList[code])
			return `[${code}] ${app.errList[code]}`
		else
			return `[${code}] ${msg}`
	}

	checkFirstOpen() {
		
	}

	showNotices(p) {
		if (this.globalData('noticeShowed'))
			return
		const show = (id) => {
			const noticeId = id || '000000'
			this.request({
				source: 'util',
				method: 'put',
				uri: `sys/notices/${noticeId}`,
				success: (data) => {
					this.globalData('noticeShowed', true)
					if (!data || !data.noticeId || !data.notices instanceof Array || data.notices.length == 0)
						return
					const { noticeId, notices } = data
					if (!p.temp)
						p.temp = {}
					p.temp.noticeId = noticeId
					p.checkNotice = () => this.checkNotice(p)
					p.setData({
						notice: {
							show: true,
							data: notices
						}
					})
				},
				fail: (code, msg) => {
					console.error(code, msg)
				}
			})
		}
		wx.getStorage({
			key: 'noticeId',
			success: (res) => {
				show(res.data)
			},
			fail: (err) => {
				show()
			}
		})
	}

	checkNotice(p) {
		p.setData({
			notice: {
				show: false
			}
		})
		if (p.temp.noticeId) {
			wx.setStorage({
				key: 'noticeId',
				data: p.temp.noticeId,
			})
		}
	}

	addExtDataNeed(name) {
		let needExtData = this.globalData('needExtData')
		needExtData.push(name)
		this.globalData('needExtData', needExtData)
	}

	userLogin (obj) {
		const { p, scene, success, fail, logined, complete, notBind, notAuth } = obj
		if (this.globalData('logined')) {
			wx.stopPullDownRefresh()
			const data = this.globalData('userData')
			if (data.userType == 'normal' && logined) {
				logined(data)
			}
			if (complete)
				complete(data)
		}
		else {
			this.showLoadingPanel(p, '登录中')
			this.startLogin({
				scene,
				toast: !p.data.loadingPanel || !p.data.loadingPanel.tip,
				success: (userData) => {
					this.hideLoadingPanel(p)
					if (userData.userType == 'normal' && notBind) {
						notBind(userData)
						return
					}
					if (success)
						success(userData)
					if (complete)
						complete(userData)
					wx.onSocketOpen(() => {
						console.log('ws connect success')
						this.globalData('notReconnect', false)
						this.globalData('wsConnected', true)
						this.heartTimer = setInterval(() => {
							wx.sendSocketMessage({
								data: '@heart'
							})
						}, 10000)
					})
					wx.onSocketClose(() => {
						console.error('close ws connect')
						clearInterval(this.heartTimer)
						this.globalData('wsConnected', false)
						setTimeout(() => {
							if (this.globalData('notReconnect') == true)
								return
							if (this.wsConnTryCount > 10)
								return
							this.wsConnTryCount++
							wx.connectSocket({
								url: `${this.config('wsDomain')}message/student?session=${this.globalData('session')}`,
								fail: (err) => {
									console.error('ws connect failed', err)
								}
							})
						}, 60000)
					})
					wx.onSocketError((err) => {
						console.error('ws connect failed', err)
					})
					wx.onSocketMessage((res) => {
						this.wsHandler(res.data)
					})
					if (!this.globalData('wsConnected') && userData.userType != 'normal') {
						wx.connectSocket({
							url: `${config.wsDomain}message/student?session=${this.globalData('session')}`,
							fail: (err) => {
								console.error(err)
							}
						})
					}
				},
				fail: (title, msg) => {
					this.hideLoadingPanel(p)
					if (fail)
						fail(title, msg)
				},
				notAuth: () => {
					this.hideLoadingPanel(p)
					if(notAuth)
						notAuth()
				}
			})
		}
	}

	startLogin(callback) {
		let { success, fail, notAuth, scene, toast = true } = callback
		if (this.globalData('logined')) {
			wx.stopPullDownRefresh()
			success(this.globalData('userData'))
		}
		if (this.globalData('logining')) {
			wx.stopPullDownRefresh()
			return
		}
		if (toast)
			this.loading('登录中')
		this.globalData('logining', true)
		this.checkLogin({
			scene,
			success: (userData) => {
				wx.hideToast()
				wx.stopPullDownRefresh()
				this.globalData('logining', false)
				this.globalData('logined', true)
				success(userData)
			},
			fail: () => {
				console.log('user not login')
				this.login({
					scene,
					success: (userData) => {
						wx.hideToast()
						this.globalData('logined', true)
						success(userData)
					},
					fail: (code) => {
						wx.hideToast()
						wx.stopPullDownRefresh()
						switch (code) {
							case 0:
								console.error('user not auth')
								fail('未获得授权', '您未授权我们获取您的用户信息，您可以点击右上角三个点按钮->关于粿团->再点右上角按钮->设置->开启"用户信息"开关->回到此页面重试')
								console.log(this.globalData('logined'))
								break
							case -1:
								console.error('get user info failed')
								fail('登录失败', '获取用户信息失败啦，请下拉刷新重试哦')
								break
							case -2:
								console.error('user login failed')
								fail('登录失败', '微信登录失败啦，请下拉刷新重试哦')
								break
							case -3:
								console.error('send login request failed')
								fail('登录失败', '用户登录失败啦，请下拉刷新重试哦')
								break
							case -4:
								notAuth()
						}

					}
				})
				this.globalData('logining', false)
			}
		})
	}

	checkLogin(callback) {
		let { success, fail, scene } = callback
		let session = this.globalData('session')
		let token = this.globalData('token')
		if (!session || !token || token == '' || session == '') {
			fail()
			return
		}
		console.log(`now session is ${session}`)
		wx.checkSession({
			success: () => {
				this.request({
					source: 'util',
					method: 'get',
					uri: 'user/checkLogin',
					success: (userData) => {
						wx.stopPullDownRefresh()
						this.globalData('userData', userData)
						this.globalData('logined', true)
						wx.setStorage({
							key: 'userData',
							data: userData
						})
						success(userData)
					},
					fail: (code, msg) => {
						this.globalData('logined', false)
						fail()
					}
				})
			},
			fail
		})
	}

	login(callback) {
		let { success, fail, scene } = callback
		wx.login({
			success: (res) => {
				let { code } = res
				wx.getSetting({
					success: (res) => {
						if (res.authSetting['scope.userInfo']) {
							wx.getUserInfo({
								withCredentials: true,
								success: (res) => {
									let { encryptedData, iv, userInfo } = res
									this.request({
										source: 'util',
										method: 'post',
										uri: 'user/login/wx',
										data: {
											loginType: 'wxapp',
											wxCode: code,
											wxEncrypytedData: encryptedData,
											wxIv: iv
										},
										sign: false,
										success: (data) => {
											let { session, token, userData } = data
											console.log(userData)
											this.globalData('session', session)
											this.globalData('token', token)
											this.globalData('userData', userData)
											wx.setStorage({
												key: 'session',
												data: session
											})
											wx.setStorage({
												key: 'token',
												data: token
											})
											wx.setStorage({
												key: 'userData',
												data: userData
											})
											this.globalData('logined', true)
											success(userData)
										},
										fail: (err) => {
											console.error(err)
											this.globalData('logined', false)
											fail(-3)
										}
									})
								},
								fail: (err) => {
									this.globalData('logined', false)
									if (err.errMsg.indexOf('auth deny') != -1) {
										fail(0)
									}
									else {
										fail(-1)
									}
								}
							})
						}
						else if(['touch', 'other'].indexOf(scene) != -1) {
							this.goPage('userAuth')
						}
						else
							fail(-4)
					}
				})
			},
			fail: (err) => {
				console.error(err)
				this.globalData('logined', false)
				fail(-2)
			}
		})
	}

	bindMsgBack(type, callback) {
		this.msgBack[type] = callback
	}

	uniteMsgBack(type) {
		delete this.msgBack[type]
	}

	wsHandler(res) {
		if(res == '@heart') {
			this.wsConnTryCount = 0
			return
		}	
		const msg = JSON.parse(res)
		console.log(msg)
		const { id, type, data, time } = msg
		if (!id) {
			switch(type) {
				case 'authError':
					console.error('ws auth failed')
				break
				case 'serverError':
					console.error('ws server error')
				break
				case 'otherDevLogined':
					this.globalData('notReconnect', true)
					console.error('other device is connect')
				break
				default:
					console.error(`unknown error:${type}`)
			}
			return
		}
		let newMsgList = this.globalData('newMsgList')
		if (!newMsgList instanceof Array)
			newMsgList = []
		newMsgList.push({ id, type, data, time })
		this.globalData('newMsgList', newMsgList)
		wx.setStorage({
			key: 'newMsgList',
			data: newMsgList
		})
		for (let t in this.msgBack)
			this.msgBack[t](msg)
	}

}

module.exports = new util
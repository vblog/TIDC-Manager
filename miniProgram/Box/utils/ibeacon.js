class ibeacon {
	constructor() {

	}
	search(callback) {
		let { uuids, success, fail, timeout } = callback
		let devices = []
		wx.startBeaconDiscovery({
			uuids,
			success: () => {
				console.log('开始扫描设备')
				wx.onBeaconUpdate(function(res){
					if(res && res.beacons && res.beacons.length > 0) {
						devices = res.beacons
					}
				})
			},
			fail: (err) => {
				fail(-1)
			}
		})
		setTimeout(() => {
			wx.stopBeaconDiscovery({
				success: () => {
					console.log('已停止扫描设备')
					for(let index in devices) {
						let irssi = Math.abs(devices[index].rssi)
						let power = (irssi - 59) / (10 * 2)
						devices[index].range = Math.pow(10, power)
					}
					success(devices)
				},
				fail: (err) => {
					fail(-2)
				}
			})
		}, timeout || 5000)
	}
}

module.exports = new ibeacon
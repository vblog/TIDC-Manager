let config = require('./config')

App({

	onLaunch: function (launchData) {
		let app = this
		const { path, query, scene, shareTicket, referrerInfo } = launchData
		if (scene) {
			const sceneId = parseInt(launchData.scene)
			app.globalData.sceneId = sceneId
		}
		app.globalData.session = wx.getStorageSync('session')
		app.globalData.token = wx.getStorageSync('token')
		//检查更新
		if (wx.getUpdateManager) {
			const updateManager = wx.getUpdateManager()
			updateManager.onCheckForUpdate((res) => {
				if (res.hasUpdate) {
					wx.showModal({
						title: '检测到新版本',
						content: '创新盒子发现有新版本待更新，后台已开始自动更新，更新完成后将会再次提示您',
						showCancel: false
					})
					updateManager.onUpdateReady(() => {
						wx.showModal({
							title: '新版本准备完毕',
							content: '创新盒子的最新版本已经准备完毕，需要现在创新盒子嘛？',
							success: (res) => {
								if(res.confirm) {
									updateManager.applyUpdate()
								}
							}
						})
					})
				}
			})
		}
	},

	onShow: function (data) {
		//授权登录
		if (data.scene == 1039 && data.referrerInfo && data.referrerInfo.extraData)
			this.globalData.extDataTemp = data.referrerInfo.extraData
	},

	globalData: {
		needExtData: [],
		sceneId: null,
		logined: null,
		logining: null,
		binded: null,
		wsConnected: null,
		notReconnect: null,
		noticeShowed: null,
		session: '',
		token: '',
		msgList: [],
		newMsgList: [],
		userData: {}
	},

	errList: config.errList,

	//production || development
	config: config.development

})
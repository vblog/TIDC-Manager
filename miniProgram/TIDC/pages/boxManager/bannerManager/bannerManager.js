const util = require('../../../utils/util.js')
let p

Page({

	data: {
		staticDomain: util.config('staticDomain'),
		bannerList: []
	},

	onLoad: function (params) {
		p = this
		p.getBannerList({
			success: (bannerList) => {
				p.setBannerList(bannerList)
			},
			fail: (code, msg) => {
				util.error('获取横幅列表失败', util.errorHandler(code, msg))
			}
		})
	},

	getBannerList: ({ success, fail }) => {
		util.request({
			source: 'bannerManager',
			method: 'get',
			uri: 'sys/banner',
			success,
			fail
		})
	},

	setBannerList: (bannerList) => {
		p.setData({
			bannerList
		})
	},

	//修改老横幅
	changeBanner: () => {
		wx.showActionSheet({
			itemList: ['更换', '删除', '上移', '下移'],
			success: (res) => {
				switch(res.tapIndex) {
					case 0:

					break
					case 1:

					break
					case 2:

					break
					case 3:
					
				}
			}
		})
	},

	//添加新横幅
	addBanner: () => {
		wx.chooseImage({
			count: 10,
			sourceType: ['album'],
			success: (res) => {
				console.log(res)	
			},
			fail: (err) => {

			}
		})
	}

})
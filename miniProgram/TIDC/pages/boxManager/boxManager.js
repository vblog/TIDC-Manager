const util = require('../../utils/util.js')
let p

Page({

    data: {
		staticDomain: util.config('staticDomain'),
		noticeContent: '',
		bannerList: [],
		courseYearList: [],
		courseYearIndex: 0,
		courseTermList: [],
		courseTermIndex: 0,
		courseStartDate: '2018-09-03',
		courseEndDate: '2019-01-20',
		scoreOpen: false,
		certScoreOpen: false,
		repairOpen: false,
		lostFoundInfo: {},
		lostFoundList: [],
		feedbackList: [],
		loadingPanel: {
			tip: '构建中'
		}
    },

	temp: {
		scoreOpened: false,
		certScoreOpened: false,
		repairOpened: false
	},

    onLoad: function() {
		p = this
		p.getBoxManagerData({
			success: (managerData) => {
				p.loadBoxManagerData(managerData)
				util.hideLoadingPanel(p)
			},
			fail: (code, msg) => {
				util.error('获取管理数据失败', util.errorHandler(code, msg), wx.naviagteBack)
			}
		})
    },

	onUnload: function () {
		util.unloadPage('boxManager')
	},

	getBoxManagerData: ({ success, fail }) => {
		util.request({
			source: 'boxManager',
			method: 'get',
			uri: 'studio/manager/box/info',
			success,
			fail
		})
	},

	loadBoxManagerData: ({ commonData, feedbackList, lostFoundInfo, lostFoundList }) => {
		const { notices, banners, scoreOpen, certScoreOpen, repairOpen, courseYear, courseTerm, courseStartDate, courseEndDate, courseYearList, courseTermList } = commonData
		p.setData({
			noticeContent: notices.join('\n'),
			courseYearList,
			courseTermList,
			bannerList: banners,
			courseYearIndex: courseYearList.indexOf(courseYear),
			courseTermIndex: courseTermList.indexOf(courseTerm),
			scoreOpen: scoreOpen == 1 ? true : false,
			certScoreOpen: certScoreOpen == 1 ? true : false,
			repairOpen: repairOpen == 1 ? true : false,
			courseStartDate: util.timestamp2str(courseStartDate, true, true, true),
			courseEndDate: util.timestamp2str(courseEndDate, true, true, true),
			lostFoundInfo,
			lostFoundList,
			feedbackList
		})
	},
	
	showLostPanel: (e) => {
		const lostId = e.currentTarget.dataset.id
		const { lostPass, lostFinish, lostContact } = p.getLostData(lostId)
		wx.showActionSheet({
			itemList: ['通过此项', '退回此项', lostFinish == 0 ? '结束此项' : '重启此项', '删除此项', '发送消息', '复制联系方式'],
			success: (res) => {
				if (!res.tapIndex && res.tapIndex != 0)
					return
				[
					() => {
						if (lostFinish == 1) {
							util.error('已结束', '此失物招领项已经结束了，不能进行审核操作')
							return
						}
						if (lostPass == 1) {
							util.error('已通过', '此失物招领项已经通过审核，无需多次操作')
							return
						}
						util.request({
							method: 'put',
							uri: 'studio/manager/box/lost/:lostId/resolve',
							data: {
								lostId
							},
							success: () => {
								util.success('审核通过')
								p.setLostData(lostId, {
									lostPass: 1
								})
							},
							fail: (code, msg) => {
								util.error('审核失败', util.errorHandler(code, msg))
							}
						})
					},
					() => {
						if (lostFinish == 1) {
							util.error('已结束', '此失物招领项已经结束了，不能进行审核操作')
							return
						}
						if (lostPass == 2) {
							util.error('已退回', '此失物招领项已经退回，无需多次操作')
							return
						}
						util.request({
							method: 'put',
							uri: 'studio/manager/box/lost/:lostId/reject',
							data: {
								lostId
							},
							success: () => {
								util.success('退回完毕')
								p.setLostData(lostId, {
									lostPass: 2
								})
							},
							fail: (code, msg) => {
								util.error('退回失败', util.errorHandler(code, msg))
							}
						})
					},
					() => {
						if(lostPass != 1) {
							util.error('此项未过审', '该项失物招领还未通过审核，不能结束此项，请先通过审核再操作')
							return
						}
						util.request({
							method: 'put',
							uri: 'studio/manager/box/lost/:lostId/:method',
							data: {
								lostId,
								method: ['finish', 'recover'][lostFinish]
							},
							success: () => {
								util.success(`${['结束', '重启'][lostFinish]}成功`)
								p.setLostData(lostId, {
									lostFinish: lostFinish == 0 ? 1 : 0
								})
							},
							fail: (code, msg) => {
								util.error(`${['结束', '重启'][lostFinish]}失败`, util.errorHandler(code, msg))
							}
						})
					},
					() => {
						util.request({
							method: 'delete',
							uri: 'studio/manager/box/lost/:lostId',
							data: {
								lostId
							},
							success: () => {
								util.success('删除成功')
								p.delLostData(lostId)
							},
							fail: (code, msg) => {
								util.error('删除失败', util.errorHandler(code, msg))
							}
						})
					},
					() => {

					},
					() => {
						wx.setClipboardData({
							data: lostContact,
							success: () => {
								util.modal('复制成功', lostContact)
							}
						})
					}
				][res.tapIndex]()
			}
		})
	},

	showFeedbackPanel: (e) => {
		const feedbackId = e.currentTarget.dataset.id
		wx.showActionSheet({
			itemList: ['处理完毕', '删除反馈'],
			success: () => {
				if(res.tapIndex == 0) {
					const { feedbackFinish } = p.getFeedbackData(feedbackId)
				}
				else if (res.tapIndex == 1) {

				}
			}
		})
	},

	getLostData: (lostId) => {
		for(let lostData of p.data.lostFoundList)
			if(lostData.lostId == lostId)
				return lostData
	},

	setLostData: (lostId, newData) => {
		for(let i = 0;i < p.data.lostFoundList.length;i++) {
			if (p.data.lostFoundList[i].lostId == lostId) {
				Object.assign(p.data.lostFoundList[i], newData)
				p.setData({
					lostFoundList: p.data.lostFoundList
				})
				break
			}
		}
	},

	delLostData: (lostId) => {
		for (let i = 0; i < p.data.lostFoundList.length; i++) {
			if (p.data.lostFoundList[i].lostId == lostId) {
				p.data.lostFoundList.splice(i, 1)
				p.setData({
					lostFoundList: p.data.lostFoundList
				})
				break
			}
		}
	},

	getFeedbackData: (feedbackId) => {
		for (let i = 0; i < p.data.feedbackList.length; i++) {
			if (p.data.feedbackList[i].feedbackId == feedbackId)
				return p.data.feedbackList[i]
		}
	},

	changeCourseYear: (e) => {
		p.setData({
			courseYearIndex: e.detail.value
		})
	},
	
	changeCourseTerm: (e) => {
		p.setData({
			courseTermIndex: e.detail.value
		})
	},

	changeCourseStartDate: (e) => {
		p.setData({
			courseStartDate: e.detail.value
		})
	},

	changeCourseEndDate: (e) => {
		p.setData({
			courseEndDate: e.detail.value
		})
	},

	changeScoreSwitch: (e) => {
		p.temp.scoreOpened = e.detail.value
	},

	changeCertScoreSwitch: () => {
		p.temp.certScoreOpened = e.detail.value
	},

	changeRepairSwitch: () => {
		p.temp.repairOpened = e.detail.value
	},

	goBannerManager: () => {
		wx.navigateTo({
			url: '/pages/boxManager/bannerManager/bannerManager',
		})
	},

	goUserManager: () => {
		wx.navigateTo({
			url: '/pages/boxManager/userManager/userManager',
		})
	},

	submitBoxData: (e) => {
		wx.showModal({
			title: '确认保存？',
			content: '请确认所有信息无误再保存，这将关系到创新盒子能否正常运作！如有任何问题请联系Vinlic',
			success: (res) => {
				if(res.confirm) {
					console.log(e.detail.value)
					const { notices, courseYearIndex, courseTermIndex, courseStartDate, courseEndDate } = e.detail.value
					const { scoreOpened, certScoreOpened, repairOpened } = p.temp

				}
			}
		})
	}

})
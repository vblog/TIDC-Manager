const util = require('../../utils/util.js')
let p

Page({

	data: {
		majorIndex: 0,
		majorList: ["Web开发", "移动开发", "物联网开发", "工作室管理层", "其它（在介绍详述）"],
		userPhoto: '',
		submitting: false
	},

	onLoad: function (options) {
		p = this
	},

	onUnload: function () {
		util.unloadPage('apply')
	},

	submitApply: (e) => {
		let { userId, userName, userSex, userClass, userEmail, userWechat, userPhone, userMajor, userIntroduce } = e.detail.value
		const {userPhoto} = p.data
		if (!/^(1\d{9}|C1\d{9})$/.test(userId)) {
			util.error('学号格式不正确', '请填写正确的学号哦')
			return
		}
		if (!/^\W{2,}$/.test(userName)) {
			util.error('姓名格式不正确', '请填写正确的姓名哦')
			return
		}
		if (!/^\d{2}\W{2}\d班$/.test(userClass)) {
			util.error('班级格式不正确', '请填写正确的班级哦，正确格式如：18软件1班')
			return
		}
		if (!/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z0-9]{2,6}$/.test(userEmail)) {
			util.error('邮箱格式不正确', '请填写正确的邮箱哦')
			return
		}
		if (!/^([a-zA-Z][-_a-zA-Z0-9]{5,19})|(1[3-9]\d{9})$/.test(userWechat)) {
			util.error('微信号不正确', '请填写正确的微信号哦')
			return
		}
		if (!/^1[3-9]\d{9}$/.test(userPhone)) {
			util.error('手机号码不正确', '请填写正确的手机号码哦')
			return
		}
		if (!userPhoto) {
			util.error('个人照片未上传', '请上传一张个人照片方便我们辨认本人')
			return
		}
		userIntroduce = userIntroduce.replace(/[\uD83C|\uD83D|\uD83E][\uDC00-\uDFFF][\u200D|\uFE0F]|[\uD83C|\uD83D|\uD83E][\uDC00-\uDFFF]|[0-9|*|#]\uFE0F\u20E3|[0-9|#]\u20E3|[\u203C-\u3299]\uFE0F\u200D|[\u203C-\u3299]\uFE0F|[\u2122-\u2B55]|\u303D|[\A9|\AE]\u3030|\uA9|\uAE|\u3030/ig, '')
		if (userIntroduce.length < 30) {
			util.error('完善自我介绍', '请填写至少30字的自我介绍方便我们了解你哦')
			return
		}
		wx.showModal({
			title: '确认提交？',
			content: '请确认申请信息无误再提交哦，个人照片如果非本人将无法通过审核',
			confirmText: '提交',
			cancelText: '取消',
			success: (res) => {
				if(res.confirm) {
					p.setData({
						submitting: true
					})
					util.uploadOSSFile({
						path: userPhoto,
						dir: 'apply/',
						prefix: '.jpg',
						success: (url) => {
							const applyData = {
								userId,
								userName,
								userSex: parseInt(userSex),
								userClass,
								userEmail,
								userWechat,
								userPhone,
								userMajor: p.data.majorList[userMajor],
								userIntroduce,
								userPhoto: url
							}
							util.request({
								source: 'apply',
								method: 'post',
								uri: 'studio/apply',
								data: applyData,
								success: (data) => {
									util.modal('提交成功', '我们将尽快审核您的申请并联系你，选择科创，走上人生巅峰~', () => {
										wx.switchTab({
											url: '/pages/home/home',
										})
									})
								},
								fail: (code, msg) => {
									p.setData({
										submitting: false
									})
									util.error('提交失败', util.errorHandler(code, msg))
								}
							})
						},
						fail: (code, msg) => {
							p.setData({
								submitting: false
							})
							util.error('上传照片失败', util.errorHandler(code, msg))
						}
					})
				}
			}
		})
	},

	changeMajor: (e) => {
		p.setData({
			majorIndex: e.detail.value
		})
	},

	choosePhoto: () => {
		wx.chooseImage({
			count: 1,
			sizeType: ['compressed'],
			success: function(res) {
				const path = res.tempFilePaths[0]
				if (res.tempFiles[0].size > util.config('upload').maxSize.applyPhoto) {
					util.error('上传照片过大', '只支持上传10M以内的照片，请重新选择照片')
					return
				}
				wx.getImageInfo({
					src: path,
					success: ({ type }) => {
						if(type != 'jpeg' && type != 'jpg') {
							util.error('不支持此图片格式', '只支持上传jpg格式的图片哦，请确保上传的是你的个人照片')
							return
						}
						p.setData({
							userPhoto: path
						})
					},
					fail: (err) => {
						console.error(err)
						util.error('读取照片信息失败', '请换一张照片重试哦')
					}
				})
			}
		})
	},

	onShareAppMessage: function () {
		return {
			title: '科创工作室招新啦，戳我申请！',
			path: '/pages/apply/apply',
			imageUrl: 'https://pub.cx-tidc.com/image/cxxy/tidc/photos/0003.jpg'
		}
	}

})
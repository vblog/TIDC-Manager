const util = require('../../utils/util.js')
let p

Page({

	data: {
		logined: false,
		userHead: '',
		userName: '',
		userType: 1,
		userSignDuration: 0,
		userScore: 0,
		loadingPanel: {
			tip: '构建中'
		}
	},

	temp: {
		userData: null
	},

	onLoad: function (options) {
		p = this
		util.checkLogin({
			success: () => {
				util.checkCommonData({
					success: (commonData) => {
						const { userId, userType, userHead, userName, userNickName, userSex, userSignDuration, userScore } = util.globalData('userData')
						p.setData({
							userId,
							userType,
							userHead,
							userName,
							userSex,
							userSignDuration,
							userScore,
							logined: true
						})
						p.getUserData({
							success: (userData) => {
								const data = {
									userId: userData.userId,
									userType: userData.userType,
									userHead,
									userName: userData.userName,
									userSex: userData.userSex,
									userSignDuration: userData.userSignDuration,
									userScore: userData.userScore
								}
								p.setData(data)
								util.globalData('userData', data)
								wx.setStorage({
									key: 'userData',
									data: data,
								})
								util.hideLoadingPanel(p)
							},
							fail: (code, msg) => {
								util.hideLoadingPanel(p)
								util.error('获取用户信息失败', util.errorHandler(code, msg))
							}
						})
					},
					fail: (code, msg) => {
						p.setData({
							logined: false
						})
						util.hideLoadingPanel(p)
						util.error('首页获取数据失败', '无法获取到首页数据哦，请检查网络稍后再试~')
					}
				})
			},
			fail: () => {
				p.setData({
					logined: false
				})
				util.hideLoadingPanel(p)
			}
		})
	},

	onUnload: function () {
		util.unloadPage('my')
	},

	onShow: () => {
		if (p.temp.userData) {
			const { userId, userType, userHead, userName, userNickName, userSex, userSignDuration, userScore } = p.temp.userData
			util.success('登录成功', 800)
			p.setData({
				userId,
				userType,
				userHead,
				userName,
				userSex,
				userSignDuration,
				userScore,
				logined: true
			})
			p.temp.userData = null
		}
		else if (util.globalData('logined')) {
			const { userId, userType, userHead, userName, userNickName, userSex, userSignDuration, userScore } = util.globalData('userData')
			p.setData({
				userId,
				userType,
				userHead,
				userName,
				userSex,
				userSignDuration,
				userScore,
				logined: true
			})
		}
		else if (util.globalData('logined') != p.data.logined) {
			p.setData({
				logined: util.globalData('logined')
			})
		}
	},

	getUserData: ({ sync, success, fail }) => {
		util.request({
			source: 'my',
			method: sync ? 'put' : 'get',
			uri: 'studio/user/info',
			success,
			fail
		})
	},

	goLogin: (e) => {
		if (e.detail.errMsg.indexOf('auth deny') != -1) {
			util.error('授权失败', '点击立即授权后要点击允许才能登录噢')
		}
		else {
			util.loading('授权中')
			wx.getUserInfo({
				withCredentials: false,
				success: (res) => {
					wx.hideToast()
					util.goPage('login', res.userInfo)
				},
				fail: (err) => {
					wx.hideToast()
					console.error(err)
					util.error('获取用户信息失败', '由于某些未知原因无法获得您的用户信息，请重新添加小程序再试')
				}
			})
		}
	},

	goApply: () => {
		util.goPage('apply')
	},

	goSignDetail: () => {
		if (!p.data.logined)
			return
		util.goPage('signDetail')
	},

	goScoreDetail: () => {
		if (!p.data.logined)
			return
		util.goPage('scoreDetail')
	},

	goManager: () => {
		if (!p.data.logined)
			return
		util.goPage('manager')
	},

	goInfo: () => {
		if (!p.data.logined)
			return
		util.goPage('userInfo')
	},

	goProblem: () => {
		if (!p.data.logined)
			return
		util.goPage('problem')
	},

	goBox: () => {
		if (!p.data.logined)
			return
		wx.navigateToMiniProgram({
			appId: 'wx7305eb5d5801811f'
		})
	},

	goFeedback: () => {
		if (!p.data.logined)
			return
		util.goPage('feedback')
	},

	goAbout: () => {
		if (!p.data.logined)
			return
		util.goPage('about')
	},

	logOut: () => {
		if (!p.data.logined)
			return
		wx.showModal({
			title: '退出登录',
			content: '确定要退出登录吗？',
			success: (res) => {
				if(res.confirm) {
					util.loading('登出中')
					util.request({
						source: 'my',
						method: 'delete',
						uri: 'studio/logout',
						success: () => {
							util.globalData('logined', false)
							util.globalData('commonData', null)
							util.globalData('userData', null)
							wx.clearStorageSync()
							p.setData({
								logined: false
							})
							wx.hideToast()
							util.success('登出成功')
						},
						fail: (code, msg) => {
							if(code == '-1004') {
								util.globalData('logined', false)
								util.globalData('commonData', null)
								util.globalData('userData', null)
								wx.clearStorageSync()
								p.setData({
									logined: false
								})
								wx.hideToast()
								util.success('登出成功', 800)
							}
							wx.hideToast()
							util.error('登出失败', util.errorHandler(code, msg))
						}
					})
				}
			}
		})
	},

	onPullDownRefresh: () => {
		wx.stopPullDownRefresh()
		if(util.globalData('logined')) {
			util.showLoadingPanel(p, '构建中')
			p.getUserData({
				sync: true,
				success: (userData) => {
					const { userId, userType, userName, userNickName, userSex, userSignDuration, userScore } = userData
					const data = {
						userId: userId,
						userType: userType,
						userHead: util.globalData('userData').userHead,
						userName: userName,
						userSex: userSex,
						userSignDuration: userSignDuration,
						userScore: userScore
					}
					p.setData(data)
					util.hideLoadingPanel(p)
					util.globalData('userData', data)
					wx.setStorage({
						key: 'userData',
						data: data,
					})
				},
				fail: (code, msg) => {
					util.hideLoadingPanel(p)
					util.error('获取用户信息失败', util.errorHandler(code, msg))
				}
			})
		}
	},

	onShareAppMessage: function () {
		return {
			title: '微科创，个人中心',
			path: '/pages/my/my'
		}
	}

})
let util = require('../../utils/util')
let p

Page({

    data: {
        feedBackContent: '',
        feedBackContact: '',
        submitting: false
    },

    onLoad: function (options) {
        p = this
    },

	onUnload: () => {
		util.unloadPage('feedback')
	},

    feedBackContentInput: (e) => {
        p.data.feedBackContent = e.detail.value
    },

    feedBackContactInput: (e) => {
        p.data.feedBackContact = e.detail.value
    },

    submitFeedBack: () => {
        let { feedBackContent, feedBackContact } = p.data
        if (feedBackContent == '' || feedBackContact == '') {
            util.error('未填写完整', '请输入反馈内容和联系方式噢')
            return
        }
        p.setData({
            submitting: true
        })
        util.request({
			source: 'feedback',
            method: 'POST',
            uri: 'studio/feedback',
            data: {
				content: feedBackContent,
				contact: feedBackContact
            },
            success: () => {
				util.success('提交成功', 1000)
				setTimeout(() => {
					wx.switchTab({
						url: '/pages/home/home',
					})
				}, 800)
            },
            fail: (err) => {
                console.error(err)
				p.setData({
					submitting: false
				})
                util.error('提交失败', '提交反馈失败啦，请重试哦')
            }
        })
    },

	onShareAppMessage: function () {
		return {
			title: '创新校园汇邀您献出宝贵的意见',
			path: '/pages/home/home?go=feedback'
		}
	}

})
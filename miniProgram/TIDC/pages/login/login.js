const util = require('../../utils/util.js')
let p

Page({

	data: {
		formSubmiting: false
	},

	temp: {
		userData: {}
	},

	onLoad: function (params) {
		if(!params) {
			util.error('数据错误', '数据加载错误请返回主页', wx.navigateBack)
			return
		}
		p = this
		const { avatarUrl, gender, nickName } = params
		p.temp.userData = Object.assign(p.temp.userData, {
			userHead: avatarUrl,
			userSex: gender,
			userNickName: nickName
		})
	},

	onShow: () => {
		//判断是否授权登录成功
		const extData = util.globalData('extDataTemp')
		if(extData) {
			p.loginDataHandle(extData)
		}
	},

	onUnload: function () {
		util.unloadPage('login')
	},

	login: (e) => {
		const { userId, userPwd } = e.detail.value
		if (!/^(1\d{9}|C1\d{9})$/.test(userId)) {
			util.error('学号不正确', '请填写正确的学号哦')
			return
		}
		if (!userPwd) {
			util.error('密码不能为空', '请填写正确的密码哦')
			return
		}
		p.setData({ formSubmiting: true })
		util.request({
			source: 'login',
			method: 'post',
			uri: 'studio/login',
			data: {
				userId,
				userPwd
			},
			success: (loginData) => {
				p.loginDataHandle(loginData)
			},
			fail: (code, msg) => {
				p.setData({ formSubmiting: false })
				if(code == '-1020')
					util.error('登录失败', '工作室认证服务器无法通信，请稍后重试')
				else
					util.error('登录失败', util.errorHandler(code, msg))
			}
		})
	},

	loginDataHandle(loginData) {
		const { session, token, userData } = loginData
		p.temp.userData = Object.assign(p.temp.userData, userData)
		util.globalData('loginChecked', false)
		util.globalData('session', session)
		util.globalData('token', token)
		util.globalData('userData', p.temp.userData)
		util.globalData('logined', true)
		wx.setStorage({
			key: 'session',
			data: session
		})
		wx.setStorage({
			key: 'token',
			data: token
		})
		wx.setStorage({
			key: 'userData',
			data: p.temp.userData
		})
		util.checkCommonData({
			success: () => {
				util.goLastPage({
					userData: p.temp.userData
				})
			},
			fail: (code, msg) => {
				p.setData({ formSubmiting: false })
				util.error('首页获取数据失败', '无法获取到首页数据哦，请检查网络稍后再试~')
			}
		})
	},

	boxLogin: () => {
		if (p.data.formSubmiting)
			return
		util.addExtDataNeed('authLogin')
		wx.navigateToMiniProgram({
			appId: 'wx7305eb5d5801811f',
			path: '/pages/authLogin/authLogin',
			extraData: {
				appId: 'm-0001'
			},
			envVersion: 'release',
			success: () =>{
				
			},
			fail: () => {
				util.error('跳转失败', '无法跳转到创新盒子，可能是您的微信版本过低，请升级后重试')
			}
		})
	}

})
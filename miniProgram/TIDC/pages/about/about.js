const util = require('../../utils/util')
let p

Page({

	data: {
		staticDomain: util.config('staticDomain'),
		version: util.config('version'),
		beta: true
	},

	temp: {
		openToolsMark: 0
	},

	onLoad: function (options) {
		p = this
	},

	onUnload: () => {
		util.unloadPage('about')
	},

	openTools: () => {
		p.temp.openToolsMark++
		if (p.temp.openToolsMark == 3) {
			p.temp.openToolsMark = 0
			wx.showActionSheet({
				itemList: ['我从哪来？', '开启调试模式', '清除程序缓存', '检查服务器连通性'],
				itemColor: '#ff6759',
				success: (res) => {
					switch (res.tapIndex) {
						case 0:
							util.modal('我从哪来？', '五个人吃着关东煮突发奇想为科创留下的非物质文化遗产')
							break
						case 1:
							p.openDebug()
							break
						case 2:
							p.clearStorage()
							break
						case 3:
							p.checkServerConn()
							break
					}
				}
			})
		}
	},

	//打开调试
	openDebug: () => {
		if (wx.getStorageSync('debug')) {
			wx.setStorageSync('debug', false)
			wx.setEnableDebug({
				enableDebug: false
			})
		}
		else {
			wx.setStorageSync('debug', true)
			wx.setEnableDebug({
				enableDebug: true
			})
		}
	},

	//清除程序缓存
	clearStorage: () => {
		wx.showModal({
			title: '是否清除？',
			content: '将会清除本小程序在设备上存储的所有数据',
			success: (res) => {
				if (res.confirm) {
					wx.clearStorageSync()
					util.modal('清除成功', '请重启小程序')
				}
			}
		})
	},

	//检查TIDC服务器连通性
	checkServerConn: () => {
		util.loading('检查中')
		const timestamp = util.timestamp()
		util.request({
			source: 'about',
			method: 'get',
			uri: 'sys/test/connect',
			sign: false,
			success: (data) => {
				wx.hideToast()
				console.log(data)
				util.modal('服务器正常', `服务器正确的响应请求 ${data} [${util.timestamp() - timestamp}ms]`)
			},
			fail: (code, msg) => {
				wx.hideToast()
				console.error(code, msg)
				util.error('响应不正确', `服务器不正确的响应，响应内容已打印控制台[${code} ${msg} ${util.timestamp() - timestamp}ms]`)
			}
		})
	},

	callPhone: () => {
		wx.makePhoneCall({
			phoneNumber: '15625571656',
		})
	},

	onShareAppMessage: function () {
		return {
			title: 'TIDC科创工作室小程序',
			path: '/pages/about/about'
		}
	}

})
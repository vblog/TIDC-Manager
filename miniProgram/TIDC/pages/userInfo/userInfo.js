const util = require('../../utils/util.js')
let p

Page({

	data: {
		userData: {
			userType: 2,
			userId: '',
			userName: '',
			userSex: 1,
			userGrade: '',
			userClass: '',
			userEmail: '',
			userNativePlace: '',
			userIdentity: '',
			userQQ: '',
			userWechat: '',
			userBlog: '',
			userMajor: '',
			userPhone: ''
		},
		loadingPanel: {
			tip: '构建中'
		}
	},

	onLoad: function (options) {
		p = this
		p.loadUserData()
	},

	onUnload: function () {
		util.unloadPage('userInfo')
	},

	loadUserData: (obj) => {
		const { sync } = obj || {}
		p.getUserData({
			sync,
			success: (userData) => {
				p.setUserData(userData)
				util.hideLoadingPanel(p)
			},
			fail: (code, msg) => {
				util.hideLoadingPanel(p)
				util.error('获取用户信息失败', util.errorHandler(code, msg))
			}
		})
	},

	getUserData: ({sync, success, fail}) => {
		util.request({
			source: 'userInfo',
			method: sync ? 'put' : 'get',
			uri: 'studio/user/info',
			success,
			fail
		})
	},

	setUserData: (userData) => {
		p.setData({
			userData
		})
	},

	onPullDownRefresh: function () {
		wx.stopPullDownRefresh()
		util.showLoadingPanel(p, '构建中')
		p.loadUserData({
			sync: true
		})
	}

})
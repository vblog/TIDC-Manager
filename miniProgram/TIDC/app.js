let config = require('./config')

App({

	onLaunch: function (launchData) {
		let app = this
		const { path, query, scene, shareTicket, referrerInfo } = launchData
		if (scene) {
			const sceneId = parseInt(launchData.scene)
			app.globalData.sceneId = sceneId
		}
		app.globalData.session = wx.getStorageSync('session')
		app.globalData.token = wx.getStorageSync('token')
		//检查更新
		if (wx.getUpdateManager) {
			const updateManager = wx.getUpdateManager()
			updateManager.onCheckForUpdate((res) => {
				if (res.hasUpdate) {
					wx.showModal({
						title: '检测到新版本',
						content: '小程序发现有新版本待更新，后台已开始自动更新，更新完成后将会再次提示您',
						showCancel: false
					})
					updateManager.onUpdateReady(() => {
						wx.showModal({
							title: '新版本准备完毕',
							content: '小程序的最新版本已经准备完毕，需要现在更新小程序嘛？',
							success: (res) => {
								if (res.confirm) {
									updateManager.applyUpdate()
								}
							}
						})
					})
				}
			})
		}
	},

	onShow: function (data) {
		if (data.scene == 1038 && data.referrerInfo && data.referrerInfo.extraData && this.globalData.needExtData.indexOf('authLogin') != -1) {
			this.globalData.extDataTemp = data.referrerInfo.extraData
			this.globalData.needExtData.splice(this.globalData.needExtData.indexOf('authLogin'), 1)
		}	
	},

	globalData: {
		needExtData: [],
		extData: null,
		session: null,
		token: null,
		logined: false,
		loginChecked: false,
		userData: null,
		commonData: null
	},

	errList: config.errList,

	//production || development
	config: config.development


})
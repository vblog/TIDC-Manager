package com;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.*;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;
import java.util.Collections;

/**
 * Description:
 *
 * @author 张朝锋
 * @date 2018-09-14
 */
@Data
@Configuration
@EnableSwagger2
public class Swagger2 {

    @Value("${swagger2.show}")
    private boolean show;

    /**
     * 认证服务器认证接口
     */
    @Value("${ResourceServerConfiguration.oauthTokenUrl}")
    private String oauthTokenUrl;

    /**
     * 客户端账户Id
     */
    @Value("${ResourceServerConfiguration.clientId}")
    private String clientId;

    /**
     * 客户端秘文
     */
    @Value("${ResourceServerConfiguration.clientSecret}")
    private String clientSecret;


    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .enable(show)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.tidc.manage.controller"))
                .paths(PathSelectors.any())
                .build()
                .securityContexts(Collections.singletonList(securityContext()))
                .securitySchemes(Collections.singletonList(securityScheme()));
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("TIDC Manage Center")
                .description("科创开发中心管理系统")
                .termsOfServiceUrl("http://localhost:8080")
                .version("0.0.1-SNAPSHOT")
                .build();
    }

    private SecurityScheme securityScheme() {
        GrantType grantType = new AuthorizationCodeGrantBuilder()
                .tokenEndpoint(new TokenEndpoint(oauthTokenUrl, "access_token"))
                .tokenRequestEndpoint(
                        new TokenRequestEndpoint(
                                "https://oauth.cx-tidc.com/authorizationserver/oauth/authorize",
                                "client_id", "client_secret"))
                .build();

        return new OAuthBuilder().name("spring_oauth")
                .grantTypes(Collections.singletonList(grantType))
                .scopes(Arrays.asList(scopes()))
                .build();
    }

    private AuthorizationScope[] scopes() {
        return new AuthorizationScope[]{
                new AuthorizationScope("all", "all scope")};
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(Collections.singletonList(new SecurityReference("spring_oauth", scopes())))
                .forPaths(PathSelectors.regex("/manage/*"))
                .build();
    }
}

package com.tidc.manage.exception;

/**
 * Description:
 * 参数不匹配异常
 *
 * @author 张朝锋
 * @date 2018-09-25
 */
public class MismatchException extends Exception{

    private String[] messages;

    private static final long serialVersionUID = -1171511073720147359L;

    /**
     * 构造器-两个或两个以上的参数匹配失败
     * @param args 参数名
     */
    public MismatchException(String... args) {
        this.messages = args;
    }

    @Override
    public String getMessage(){
        return String.join(" and ", messages) +" do not match.";
    }
}

package com.tidc.manage.exception;

/**
 * Description:
 *  处理参数为零的异常
 * @author 张朝锋
 * @date 2018-09-22
 */
public class ZeroException extends Exception {

    private String valueName;

    public String getValueName() {
        return valueName;
    }

    public ZeroException(String valueName) {
        this.valueName = valueName;
    }
}

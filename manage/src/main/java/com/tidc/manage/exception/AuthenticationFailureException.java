package com.tidc.manage.exception;

/**
 * Description:
 *  认证失败异常
 * @author 张朝锋
 * @date 2018-09-30
 */
public class AuthenticationFailureException extends Exception {

    private static final long serialVersionUID = 6236792631210415600L;

    @Override
    public String getMessage() {
        return "username or password is wrong";
    }
}

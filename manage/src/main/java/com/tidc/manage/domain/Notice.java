package com.tidc.manage.domain;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.Null;
import javax.validation.constraints.Pattern;

/**
 * <p>
 * 公告实体类
 * </p>
 *
 * @author 张朝锋
 * @since 2018-10-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Notice extends SuperEntity<Notice> implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 标题
     */
    @Pattern(regexp = "^.{1,45}$", message = "The length of 'title' is between 1 and 45.")
    private String title;
    /**
     * 公告内容
     */
    @Pattern(regexp = "^.{3,200}$", message = "The length of 'title' is between 3 and 200.")
    private String content;
    /**
     * 发布日期
     */
    @ApiModelProperty(hidden = true)
    @Null(message = "You can't dominate the time!")
    private Long date;

}

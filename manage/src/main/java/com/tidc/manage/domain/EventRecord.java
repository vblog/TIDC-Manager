package com.tidc.manage.domain;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Null;
import javax.validation.constraints.Pattern;

/**
 * <p>
 * 事件记录实体类
 * </p>
 *
 * @author 张朝锋
 * @since 2018-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(description = "事件记录")
public class EventRecord extends SuperEntity<EventRecord> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("学生id")
    @Pattern(regexp = "^1\\d{9}$", message = "Error in studentId format.")
    private String studentId;

    @ApiModelProperty(value = "学生姓名", hidden = true)
    @Null(message = "I saw you!")
    private String studentName;

    @ApiModelProperty("事件id，为1时为自定义事件")
    @Min(value = 1, message = "Not less than 1.")
    private Integer eventId;

    @ApiModelProperty(value = "事件被记录时的时间", hidden = true)
    @Null(message = "You can't dominate the time!")
    private Long time;

    @ApiModelProperty("分数")
    @Min(value = -99, message = "Not less than -100")
    @Max(value = 99, message = "No more than 100.")
    private Integer score;

}

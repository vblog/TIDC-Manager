package com.tidc.manage.domain;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.Null;

/**
 * <p>
 *  用户权限关联 实际为DO
 * </p>
 *
 * @author 张伟杰
 * @since 2018-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "用户权限")
public class UserRole extends SuperEntity<UserRole> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("用户Id")
    private String userId;

    @ApiModelProperty("用户权限的Id")
    private Integer roleId;

    @ApiModelProperty(hidden = true)
    @TableField(exist = false)
    @Null(message = "(level)This is a read-only field.")
    private Integer level;

}

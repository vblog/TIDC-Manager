package com.tidc.manage.domain;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *  权限DO
 * </p>
 *
 * @author 张朝锋
 * @author 张伟杰
 * @since 2018-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "权限类")
public class Role extends SuperEntity<Role> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 权限级别
     */
    @ApiModelProperty("权限代码")
    private Integer level;

    @ApiModelProperty("权限名称")
    private String roleName;

}

package com.tidc.manage.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Null;
import java.io.Serializable;

/**
 * Description:
 * 对于有自增主键id的表，建议继承本类，而不是自实现id属性
 * @author 张朝锋
 * @date 2018-09-14
 */
public class SuperEntity <T extends Model> extends Model<T> {

    private static final long serialVersionUID = -9189687199157580877L;

    @TableId(value = "id", type = IdType.NONE)
    @ApiModelProperty(value = "自增Id", hidden = true)
    @Null(message = "id must be empty.")
    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}

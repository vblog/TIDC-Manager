package com.tidc.manage.domain;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

/**
 * <p>
 * 事件实体类
 * </p>
 *
 * @author 张朝锋
 * @since 2018-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(description = "事件")
public class Event extends SuperEntity<Event> implements Serializable {


    private static final long serialVersionUID = 1L;

    /**
     * 事件的分数
     */
    @ApiModelProperty("事件对用的分数")
    @Min(value = -99, message = "Not less than -100")
    @Max(value = 99, message = "No more than 100.")
    private Integer score;

    /**
     * 事件得分说明，如签到，比赛获奖，完成项目，参加大扫除……
     */
    @ApiModelProperty("事件对应的描述")
    @Pattern(regexp = "^\\W{5,200}$", message = "Chinese characters must be used, ranging from 5 to 200.")
    private String description;
}

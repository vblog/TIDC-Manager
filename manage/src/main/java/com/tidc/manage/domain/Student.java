package com.tidc.manage.domain;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.*;

/**
 * <p>
 * 学生信息实体类
 * </p>
 *
 * @author 张朝锋
 * @author 张伟杰
 * @since 2018-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(description = "学生信息")
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("学号")
    @TableId(value = "student_id", type = IdType.INPUT)
    @Pattern(regexp = "^1\\d{9}$", message = "Error in studentId format.")
    @NotNull
    private String studentId;

    @ApiModelProperty("姓名")
    @Pattern(regexp = "^\\W{2,20}$", message = "Error in className format.")
    @Size(min = 2, max = 20,
            message = "The length of the name should not exceed 20 Chinese characters and not less than 2 Chinese characters.")
    private String name;

    @ApiModelProperty(value = "班级", example = "如: 16软件1班,请使用小写数字")
    @Pattern(regexp = "^\\d{2}\\W{2}\\d{1,2}班$", message = "Error in className format.")
    private String className;

    @ApiModelProperty(value = "年级", example = "完整的年份，如: 2016")
    @Pattern(regexp = "^[2-9]\\d{3}$", message = "Error in grade format.")
    private String grade;

    @ApiModelProperty("邮箱地址")
    @Email(message = "Error in mailbox format.")
    private String email;

    @ApiModelProperty("微信号")
    @Pattern(regexp = "^([a-zA-Z][-_a-zA-Z0-9]{5,19})|(1[3-9]\\d{9})$",
            message = "Error in wechat format.")
    private String wechat;

    @ApiModelProperty(value = "性别", example = "1")
    @Min(value = 0, message = "Select only number 0 or 1 or 2.")
    @Max(value = 2, message = "Select only number 0 or 1 or 2.")
    private Integer gender;

    @ApiModelProperty(value = "籍贯", example = "如: 广东汕头")
    @Pattern(regexp = "^\\W{4,20}$", message = "Error in nativePlace format.")
    private String nativePlace;

    /**
     * 身份证号的正则校验规则，暂时不用
     * //@Pattern(regexp = "^\\d{6}(18|19|20)?\\d{2}(0[1-9]|1[012])(0[1-9]|[12]\\d|3[01])\\d{3}(\\d|[xX])$", message = "身份证号格式错误")
     */
    @ApiModelProperty(value = "身份证号", example = "用于比赛报名等用途")
    private String identity;

    @ApiModelProperty("qq号")
    @Pattern(regexp = "^[1-9]\\d{4,14}$", message = "Error in QQ number format.")
    private String qq;

    @ApiModelProperty(value = "博客地址", example = "这里选填，其他为必填")
    private String blogUrl;

    @ApiModelProperty(value = "方向", example = "不确定的话可以为空,但尽量确定,如；Web开发/移动开发/物联网开发/或者自行描述")
    private String major;

    @ApiModelProperty("手机号")
    @Pattern(regexp = "^1[3-9]\\d{9}$", message = "Error in phoneNumber format.")
    private String phoneNumber;

    @ApiModelProperty(value = "权限等级", hidden = true)
    @TableField(exist = false)
    @Null(message = "You can't choose your birth, but one day you can change your destiny.")
    private Integer level;
}
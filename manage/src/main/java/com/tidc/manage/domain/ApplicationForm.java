package com.tidc.manage.domain;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.*;

/**
 * <p>
 * 申请表 前端控制器
 * </p>
 *
 * @author 张伟杰
 * @since 2018-09-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ApplicationForm extends SuperEntity<ApplicationForm> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("学号")
    @Pattern(regexp = "^1\\d{9}$", message = "Error in studentId format.")
    private String studentId;

    @ApiModelProperty("姓名")
    @Pattern(regexp = "^\\W{2,20}$", message = "Error in className format.")
    @Size(min = 2, max = 20,
            message = "The length of the name should not exceed 20 Chinese characters and not less than 2 Chinese characters.")
    private String name;

    @ApiModelProperty(value = "班级", example = "如: 16软件1班,请使用小写数字")
    @Pattern(regexp = "^\\d{2}\\W{2}\\d班$", message = "Error in className format.")
    private String className;

    @ApiModelProperty(value = "性别", example = "男: 1; 女: 2;")
    @Min(value = 0, message = "Select only number 0 or 1 or 2.")
    @Max(value = 2, message = "Select only number 0 or 1 or 2.")
    private Integer gender;

    @ApiModelProperty("微信号")
    @Pattern(regexp = "^([a-zA-Z][-_a-zA-Z0-9]{5,19})|(1[3-9]\\d{9})$",
            message = "Error in wechat format.")
    private String wechat;

    @ApiModelProperty("手机号")
    @Pattern(regexp = "^1[3-9]\\d{9}$", message = "Error in phoneNumber format.")
    private String phoneNumber;

    @ApiModelProperty(value = "方向", example = "不确定的话可以为空,但尽量确定,如；Web开发/移动开发/物联网开发/或者自行描述")
    private String major;

    @ApiModelProperty(value = "自我介绍", example = "不多于200字的自我介绍")
    @Pattern(regexp = "^(.|\\n){30,200}$", message = "The length of 'introduce' is between 30 and 200.")
    private String introduce;

    /**
     * 提交时间
     */
    @ApiModelProperty(value = "提交日期", hidden = true)
    @Null(message = "You can't dominate the time!")
    private Long postDate;

    @ApiModelProperty("头像")
    private String portrait;

    @ApiModelProperty("邮箱地址")
    @Email(message = "Error in mailbox format.")
    private String email;

    @ApiModelProperty(value = "审核状态", hidden = true)
    @Min(value = 0, message = "Want to participate in the audit? Come back next year.")
    @Max(value = 2, message = "Want to participate in the audit? Come back next year.")
    private Integer state;

}

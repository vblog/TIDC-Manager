package com.tidc.manage.tool;

import java.time.LocalDateTime;

/**
 * @author innoyiya
 * @date 2018/9/23.
 */
public class TimeTools {

    public static LocalDateTime getLocalDateTimeByNow(){
        return LocalDateTime.now();
    }
}

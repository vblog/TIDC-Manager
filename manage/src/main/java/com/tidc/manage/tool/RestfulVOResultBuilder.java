package com.tidc.manage.tool;

import com.tidc.manage.vo.RestfulVO;

/**
 * Description:
 * restful风格的结果"便捷"构造器
 * <p>
 * 每一个controller都处理成功数据的封装？尝试性的做一个简约方式
 * </p>
 *
 * @author 张朝锋
 * @date 2018-10-04
 */
public interface RestfulVOResultBuilder {
    /**
     * 成功Status标识
     */
    String STATUS_SUCCESS = "success";
    /**
     * 失败Status标识
     */
    String STATUS_FAILED = "failed";

    /**
     * 可能减少几个字节的代码甚至是负优化
     * 成功数据封装
     *
     * @param t   数据对象
     * @param <T> 数据类型
     * @return 自定义restful风格的成功数据封装结果
     */
    default <T> RestfulVO<T> data(T t) {
        return new RestfulVO<>(STATUS_SUCCESS, t);
    }

    /**
     * 错误信息封装
     *
     * @param code    错误代码号
     * @param message 错误信息
     * @return 自定义restful风格的错误信息封装结果
     */
    default RestfulVO failedMessage(Integer code, String message) {
        return new RestfulVO(STATUS_FAILED, code, message);
    }
}

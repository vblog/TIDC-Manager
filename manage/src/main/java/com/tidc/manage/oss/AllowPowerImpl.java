package com.tidc.manage.oss;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.http.ProtocolType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.aliyuncs.sts.model.v20150401.AssumeRoleRequest;
import com.aliyuncs.sts.model.v20150401.AssumeRoleResponse;
import org.springframework.stereotype.Service;

/**
 * @author innoyiya
 * @date 2018/10/7.
 */
@Service
public class AllowPowerImpl implements AllowPower {

    /**
     * 只有 RAM用户（子账号）才能调用 AssumeRole 接口
     * 阿里云主账号的AccessKeys不能用于发起AssumeRole请求
     * 请首先在RAM控制台创建一个RAM用户，并为这个用户创建AccessKeys
     */
    private String accessKeyId = "LTAIf5jmGCd5rnHg";
    private String accessKeySecret = "CrjQSKSAKGbSJz9WzkQy52gwgFjdro";

    /**
     * RoleArn 需要在 RAM 控制台上获取
     */
    private String roleArn = "acs:ram::1356897029005017:role/tidc";

    private String roleSessionName = "alice-001";

    private String policy = null;

    private final ProtocolType protocolType = ProtocolType.HTTPS;

    @Override
    public AssumeRoleResponse getAssumeRoleResponse() throws ClientException {
        // 创建一个 Aliyun Acs Client, 用于发起 OpenAPI 请求
        IClientProfile profile = DefaultProfile.getProfile(REGION_CN_HANGZHOU, accessKeyId, accessKeySecret);
        DefaultAcsClient client = new DefaultAcsClient(profile);
        // 创建一个 AssumeRoleRequest 并设置请求参数
        final AssumeRoleRequest request = new AssumeRoleRequest();
        request.setVersion(STS_API_VERSION);
        request.setMethod(MethodType.POST);
        request.setProtocol(protocolType);
        request.setRoleArn(roleArn);
        request.setRoleSessionName(roleSessionName);
        request.setPolicy(policy);
        // 发起请求，并得到response
        return client.getAcsResponse(request);
    }

    @Override
    public boolean hasIdentity(String userNumber, String identity) {
        return false;
    }
}

package com.tidc.manage.oss;

import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.sts.model.v20150401.AssumeRoleResponse;

/**
 * @author innoyiya
 * @date 2018/10/7.
 */
public interface AllowPower {

    /**
     * 目前只有"cn-hangzhou"这个region可用, 不要使用填写其他region的值
     */
    String REGION_CN_HANGZHOU = "cn-hangzhou";
    /**
     * 当前 STS API 版本
     */
    String STS_API_VERSION = "2015-04-01";

    AssumeRoleResponse getAssumeRoleResponse()throws ClientException;

    boolean hasIdentity(String userNumber, String identity);
}

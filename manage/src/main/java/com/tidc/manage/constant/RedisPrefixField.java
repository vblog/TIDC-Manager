package com.tidc.manage.constant;

/**
 * Description:
 *  用于reid的key命名
 *  redis Key的前缀
 * @author zcolder
 * @date 2018-10-12
 */
public class RedisPrefixField {

    public static final String USER_TOKEN_DTO = "UserTokenDTO:";

    public static final String USER = "User:";

    public static final String EVENT = "Event:";

    public static final String APPLICATION_FORM = "ApplicationForm:";

    public static final String EVENT_RECORD = "EventRecord:";

    public static final String NOTICE = "Notice:";

    public static final String Role = "Role:";

    public static final String User_Role = "UserRole:";
}

package com.tidc.manage.constant;

/**
 * @author innoyiya
 * @date 2018/10/9.
 */
public class ConstantField {

    /**
     * 一个月
     */
    public static final int ONE_MONTH= 30*24*60*60;
}

package com.tidc.manage.constant;

/**
 * @author innoyiya
 * @date 2018/9/23.
 */
public class DatabaseField {
    public static final String STUDENT_ID = "student_id";

    public static final String USER_ID = "id";
}

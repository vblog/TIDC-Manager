package com.tidc.manage.vo;

import com.tidc.manage.dto.DurationRankingDTO;
import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * Description:
 *  用于接收
 * @author zcolder
 * @date 2018-10-15
 */
@ToString
@Data
public class WeChatApiVO {
    private String status;
    private Integer code;
    private List<DurationRankingDTO> data;
}

package com.tidc.manage.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Description:
 *  学生排行榜视图类
 * @author 张朝锋
 * @date 2018-09-16
 */
@Data
@ApiModel(description = "排行榜视图")
public class ScoreRankingVO implements Serializable{

    private static final long serialVersionUID = 7816272481282186600L;

    @ApiModelProperty("学号")
    private String studentId;

    @ApiModelProperty("学生姓名")
    private String studentName;

    @ApiModelProperty("总分")
    private Integer totalScore;
}

package com.tidc.manage.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Description:
 * 自定义Restful风格的json格式封装类，用于解决前端的SB格式要求
 * 信息承载视图
 * <p>
 *  两位开发者一致认为restful风格的json格式，由控制层决定路径，也由控制层解决数据的封装问题。
 * </p>
 * @author 张朝锋
 * @date 2018-09-21
 */
@Data
@ApiModel(description = "Restful风格的json格式封装类")
public class RestfulVO<T> implements Serializable{

    private static final long serialVersionUID = -7586542076451832865L;

    @ApiModelProperty("状态标识")
    private String status;

    @ApiModelProperty("数据对象")
    private T data;

    @ApiModelProperty("错误代码")
    private Integer code;

    @ApiModelProperty("错误详细信息")
    private String msg;

    /**
     * 自定义功数据构造器
     *
     * @param status 成功
     * @param data   数据
     */
    public RestfulVO(String status, T data) {
        this.status = status;
        this.data = data;
    }

    /**
     * 自定义错误信息构造器
     *
     * @param code    错误码
     * @param msg 错误信息
     * @param status  状态表示
     */
    public RestfulVO(String status, Integer code, String msg) {
        this.code = code;
        this.msg = msg;
        this.status = status;
    }

}

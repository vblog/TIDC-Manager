package com.tidc.manage.vo;

import com.tidc.manage.domain.EventRecord;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Description:
 *  学生积分事件记录视图
 * @author 张朝锋
 * @date 2018-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "学生事件记录视图")
public class StudentEventRecordVO extends EventRecord{

    private static final long serialVersionUID = 4081356527570569642L;

    @ApiModelProperty("事件描述")
    private String description;

}

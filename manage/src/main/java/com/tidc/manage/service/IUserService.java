package com.tidc.manage.service;

import com.tidc.manage.dto.UserTokenDTO;
import com.tidc.manage.domain.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.tidc.manage.exception.AuthenticationFailureException;
import org.apache.ibatis.javassist.NotFoundException;

import java.io.IOException;

/**
 * <p>
 * 用户服务类接口
 * </p>
 *
 * @author 张朝锋
 * @since 2018-09-14
 */
public interface IUserService extends IService<User> {

    /**
     * 登录
     *
     * @param user 装用户信息的对象，包括了用户名和密码
     * @return 如果认证成功，则返回用户的token
     * @throws IOException                    使用了http请求认证服务器
     * @throws AuthenticationFailureException 认证失败异常
     */
    UserTokenDTO login(User user) throws IOException, AuthenticationFailureException;

    /**
     * 登出
     *
     * @param userId   用户id
     * @param accessToken 用户的token
     * @return 1？无论都会成功？不，因为无论如何失败都不会返回了
     */
    Integer logout(String userId, String accessToken);

    /**
     * 查询学生是否存在
     *
     * @param userId 学生id
     * @return 如果有对应学号的学生，则为true
     */
    boolean hasUser(String userId);

    /**
     * 为学生用户修改密码，通过学生id
     *
     * @param studentId   学生id
     * @param user        封装的用户信息
     * @param accessToken 用户的token
     * @return 1？无论都会成功？不，因为无论如何失败都不会返回了
     */
    Integer updateUserOfStudentForPasswordByStudentId(String studentId, User user, String accessToken);

    /**
     * 删除指定id的用户数据，物理删除
     *
     * @param id 用户id
     * @throws NotFoundException 可能因为是孤儿数据，所以找不到这个批
     */
    void deleteUserById(String id) throws NotFoundException;
}

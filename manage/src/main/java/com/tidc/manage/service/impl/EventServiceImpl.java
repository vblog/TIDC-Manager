package com.tidc.manage.service.impl;

import com.tidc.manage.domain.Event;
import com.tidc.manage.exception.ZeroException;
import com.tidc.manage.mapper.EventMapper;
import com.tidc.manage.service.IEventService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 事件基本服务实现类
 * </p>
 *
 * @author 张朝锋
 * @since 2018-09-14
 */
@Service
public class EventServiceImpl extends ServiceImpl<EventMapper, Event> implements IEventService {

    @Override
    public Integer addEvent(Event event) throws ZeroException {
        if (event.getDescription().trim().length() <= 0) {
            throw new ZeroException("Description.length");
        }
        if (event.getScore() == 0) {
            throw new ZeroException("score");
        }
        return event.insert() ? 1 : 0;
    }

}

package com.tidc.manage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tidc.manage.domain.EventRecord;
import com.baomidou.mybatisplus.extension.service.IService;
import com.tidc.manage.exception.MismatchException;
import com.tidc.manage.exception.ZeroException;
import com.tidc.manage.vo.RestfulVO;
import com.tidc.manage.vo.ScoreRankingVO;
import com.tidc.manage.vo.StudentEventRecordVO;
import org.apache.ibatis.javassist.NotFoundException;

import java.util.Map;

/**
 * <p>
 * 事件记录服务类
 * </p>
 *
 * @author 张朝锋
 * @author 张伟杰
 * @since 2018-09-14
 */
public interface IEventRecordService extends IService<EventRecord> {
    /**
     * 查询指定学号的学生所有总积分
     *
     * @param studentId 学号
     * @return 对应学生id(学号)的学生总积分
     */
    Map<String, Integer> getTotalScoreByStudentId(String studentId);

    /**
     * 如果grade是0，则为在校生排行榜，N年的N~(N-3)年级学生的排行榜
     *
     * @param order   指定顺序
     * @param current 页数
     * @param size    每页需要的条数
     * @param grade   年级数
     * @return 指定年级数以及排序方式的排行榜
     * @throws ZeroException order不可为零
     */
    IPage<ScoreRankingVO> getRankingListByGrade(int order, long current, long size, int grade) throws ZeroException;

    /**
     * 分页获取所有学生事件记录
     *
     * @param current 当前页数
     * @param size    每页显示条数
     * @return 学生事件记录，按时间降序
     */
    IPage<StudentEventRecordVO> getAllEventRecordOfPage(long current, long size);

    /**
     * 分页获取指定学生的所有事件记录
     *
     * @param studentId 学号
     * @param current   页数
     * @param size      每页显示条数
     * @return 指定学生的所有事件记录
     */
    IPage<StudentEventRecordVO> getEventRecordByStudentId(String studentId, long current, long size);

    /**
     * 通过对指定学生添加事件
     *
     * @param eventRecord 事件记录
     * @return 执行结果视图
     * @throws ZeroException     自定义的事件score 不可为零
     * @throws NotFoundException 有可能找不到evenId
     */
    Integer saveEventRecordForStudentId(EventRecord eventRecord) throws ZeroException, NotFoundException;

    /**
     * 通过指定id删除事件
     *
     * @param id eventRecordId 事件id
     * @return 执行结果
     * @throws NotFoundException 可能找不到id
     */
    Integer deleteEventRecordById(long id) throws NotFoundException;
}

package com.tidc.manage.service;

import com.tidc.manage.dto.DurationRankingDTO;

import java.util.List;

/**
 * Description:
 * 调用微信小程序接口服务接口
 *
 * @author zcolder
 * @date 2018-10-15
 */
public interface IWeChatApiService {

    /**
     * 获取签到时间持续累计排行榜，由小程序提供
     *
     * @param current 页数
     * @param size    每页条数
     * @param sort    是否自定义排序
     * @return 结果
     */
    List getDurationRanking(long current, long size, int sort);
}

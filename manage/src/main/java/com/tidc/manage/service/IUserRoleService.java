package com.tidc.manage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tidc.manage.domain.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.javassist.NotFoundException;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 张朝锋
 * @author 张伟杰
 * @since 2018-09-14
 */
public interface IUserRoleService extends IService<UserRole> {

    Integer DEFAULT_USER_LEVEL = 3;

    /**
     * 通过学号返回角色码
     *
     * @param userId 用户id
     * @return RoleLevel
     */
    int getRoleLevelByUserId(String userId);

    /**
     * 分页查询所有的用户权限
     *
     * @param current 页数
     * @param size    每页条数
     * @param level   指定查询用户权限等级
     * @return IPage<UserRole>
     */
    IPage<UserRole> getUserRoleOfPage(long current, long size, int level);

    /**
     * 通过id修改用户权限
     *
     * @param userRole 封装的用户权限数据对象
     * @return 1=成功/0=失败
     * @throws NotFoundException 找不到指定的用户
     */
    Integer updateUserRoleByUserId(UserRole userRole) throws NotFoundException;

    /**
     * 为学生初始化用户权限
     *
     * @param studentId 学生Id
     */
    void initializeUserRoleForStudentByStudentId(String studentId);

    /**
     * 删除指定用户的所有权限数据
     * 貌似本来就不能有多条，这个是bug？？
     *
     * @param userId 用户id
     * @throws NotFoundException 可能因为是孤儿数据，所以找不到这个批
     */
    void deleteUserRoleByUserId(String userId) throws NotFoundException;
}

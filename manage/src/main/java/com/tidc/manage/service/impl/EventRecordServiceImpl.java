package com.tidc.manage.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tidc.manage.domain.Event;
import com.tidc.manage.domain.EventRecord;
import com.tidc.manage.domain.Student;
import com.tidc.manage.exception.ZeroException;
import com.tidc.manage.mapper.EventRecordMapper;
import com.tidc.manage.mapper.StudentMapper;
import com.tidc.manage.service.IEventRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tidc.manage.vo.ScoreRankingVO;
import com.tidc.manage.vo.StudentEventRecordVO;
import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Year;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 张朝锋
 * @since 2018-09-14
 */
@Service
public class EventRecordServiceImpl extends ServiceImpl<EventRecordMapper, EventRecord> implements IEventRecordService {

    @Autowired
    private StudentMapper studentMapper;

    @Override
    public Map<String, Integer> getTotalScoreByStudentId(String studentId) {
        HashMap<String, Integer> scoreKeyValue = new HashMap<>(2);
        scoreKeyValue.put("score", baseMapper.getTotalScoreByStudentId(studentId));
        return scoreKeyValue;
    }

    @Override
    public IPage<ScoreRankingVO> getRankingListByGrade(int order, long current, long size, int grade) throws ZeroException {
        Page<ScoreRankingVO> scoreRankingVOPage = new Page<>(current, size);
        // 选择分数的排序方式，1为降序，-1为升序
        switch (order) {
            case 1:
                scoreRankingVOPage.setDesc("totalScore");
                break;
            case -1:
                scoreRankingVOPage.setAsc("totalScore");
                break;
            default:
                throw new ZeroException("order");
        }
        List<Integer> grades = new ArrayList<>();
        int thisYear = Year.now().getValue();
        //起源年(本系统开始运作的年份)，本系统开始开发的年份 大三学长开发 所以减二
        int originateYear = 2018 - 2;
        //如果不在数据库已有的年份中查询，则默认查询在校生的排行榜
        if (grade >= originateYear && grade <= thisYear) {
            grades.add(grade);
        } else {
            grades.add(thisYear);
            grades.add(thisYear - 1);
            grades.add(thisYear - 2);
        }
        return baseMapper.getScoreRanking(scoreRankingVOPage, grades);
    }

    @Override
    public IPage<StudentEventRecordVO> getAllEventRecordOfPage(long current, long size) {
        Page<StudentEventRecordVO> studentEventRecordVOPage = new Page<>(current, size);
        //按照时间降序
        studentEventRecordVOPage.setDesc("time");
        return baseMapper.getAllEventRecord(studentEventRecordVOPage);
    }

    @Override
    public IPage<StudentEventRecordVO> getEventRecordByStudentId(String studentId, long current, long size) {
        Page<StudentEventRecordVO> eventRecordPage = new Page<>(current, size);
        //按照时间降序
        eventRecordPage.setDesc("time");
        return baseMapper.getEventRecordByStudentId(eventRecordPage, studentId);
    }

    @Override
    public Integer saveEventRecordForStudentId(EventRecord eventRecord) throws ZeroException, NotFoundException {
        int eventId = eventRecord.getEventId();
        if (new Event().selectById(eventId) == null) {
            // 事件不存在
            throw new NotFoundException("eventId(" + eventId + ")");
        }
        // 自定义事件必须指定分数
        if (eventId == 1 && eventRecord.getScore() == 0) {
            throw new ZeroException("score");
        }else {
            eventRecord.setScore(0);
        }
        Student student = studentMapper.selectById(eventRecord.getStudentId());
        if (student == null) {
            throw new NotFoundException("studentId(" + eventRecord.getStudentId() + ")");
        }
        eventRecord.setStudentName(student.getName());
        eventRecord.setTime(System.currentTimeMillis() / 1000);
        return baseMapper.insert(eventRecord);
    }

    @Override
    public Integer deleteEventRecordById(long id) throws NotFoundException {
        if (new EventRecord().selectById(id) == null) {
            throw new NotFoundException("EventRecordId(" + id + ") ");
        }
        return baseMapper.deleteById(id);
    }

}

package com.tidc.manage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tidc.manage.domain.Student;
import com.baomidou.mybatisplus.extension.service.IService;
import com.tidc.manage.enums.StateEnum;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.javassist.NotFoundException;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 张伟杰
 * @since 2018-09-14
 */
public interface IStudentService extends IService<Student> {
    /**
     * 保存学生信息
     *
     * @param student 封装学生信息的Student对象
     * @return <code>false</code>失败 / <code>true</code>成功
     */
    StateEnum saveStudent(Student student);

    /**
     * 默认查询所有学生，如果没有传入条件的话
     *
     * @param current 页数
     * @param size    每页条数
     * @param student 学生的查询条件
     * @return 符合条件的所有学生信息
     */
    IPage<Student> getStudentOfPage(long current, long size, Student student);

    /**
     * 通过学号返回学生信息
     *
     * @param studentId 学号
     * @return 如果学号存在，返回对应的学生信息
     */
    Student getStudentById(String studentId);

    /**
     * 通过学号踢出指定学生出工作室
     * 此操作为物理删除
     *
     * @param studentId 学号
     * @return 如果学号存在，踢出对应的学生
     * @throws NotFoundException 可能因为是孤儿数据，所以找不到这个批
     */
    Integer deleteStudentByStudentId(String studentId) throws NotFoundException;
}

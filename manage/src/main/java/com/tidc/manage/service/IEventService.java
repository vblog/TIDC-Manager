package com.tidc.manage.service;

import com.tidc.manage.domain.Event;
import com.baomidou.mybatisplus.extension.service.IService;
import com.tidc.manage.exception.ZeroException;
import com.tidc.manage.vo.RestfulVO;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 张朝锋
 * @since 2018-09-14
 */
public interface IEventService extends IService<Event> {

    /**
     * 添加事件
     * @param event 事件
     * @return data == 1 ? 成功 : 失败;
     * @throws ZeroException 禁止event.score为零，孤儿数据
     */
    Integer addEvent(Event event) throws ZeroException;
}

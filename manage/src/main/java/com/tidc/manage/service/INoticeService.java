package com.tidc.manage.service;

import com.tidc.manage.domain.Notice;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.javassist.NotFoundException;

import java.util.List;

/**
 * <p>
 *  公告服务类接口
 * </p>
 *
 * @author 张朝锋
 * @since 2018-10-04
 */
public interface INoticeService extends IService<Notice> {
    /**
     * 添加 公告
     * @param notice 公告数据对象
     * @return 成功data为1，失败为0
     */
    Integer saveNotice(Notice notice);

    /**
     * 根据id删除 公告
     * @param id 公告id
     * @return 成功data为1，失败为0
     * @throws NotFoundException id数据库不存在
     */
    Integer deleteNotice(Long id) throws NotFoundException;

    /**
     * 更新公告
     * @param notice 公告数据对象
     * @return 成功data为1，失败为0
     * @throws NotFoundException id数据库不存在
     */
    Integer updateNotice(Notice notice) throws NotFoundException;

    /**
     * 获取所有公告
     * @return 所有的公告
     */
    List<Notice> getListNotice();
}

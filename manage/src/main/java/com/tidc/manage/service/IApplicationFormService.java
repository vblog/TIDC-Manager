package com.tidc.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tidc.manage.domain.ApplicationForm;
import com.tidc.manage.exception.ZeroException;
import org.apache.ibatis.javassist.NotFoundException;

import java.util.List;

/**
 * <p>
 * 申请表 服务类接口
 * </p>
 *
 * @author 张朝锋
 * @author 张伟杰
 * @since 2018-09-23
 */
public interface IApplicationFormService extends IService<ApplicationForm> {

    /**
     * 保存申请表
     *
     * @param form 申请表内容
     * @return 执行结果
     */
    Integer saveApplicationForm(ApplicationForm form) throws ZeroException;

    /**
     * 根据学号获取单个申请表
     *
     * @param studentId 学号
     * @return 执行结果
     */
    ApplicationForm getSingleApplicationForm(String studentId);

    /**
     * 获取所有申请表
     * 只查询一个月内的申请
     *
     * @return 执行结果
     */
    List<ApplicationForm> listApplicationForms();

    /**
     * 修改申请表的审核状态
     *
     * @param id              申请表的id
     * @param applicationForm 修改的内容
     * @return 执行结果
     * @throws NotFoundException 申请表不存在
     */
    Integer updateApplicationFormOfState(int id, ApplicationForm applicationForm) throws NotFoundException;

    /**
     * 根据id删除申请表
     *
     * @param id 申请表id
     * @return 执行结果
     * @throws NotFoundException 找不到id对应数据表
     */
    Integer deleteApplicationFormById(Long id) throws NotFoundException;
}

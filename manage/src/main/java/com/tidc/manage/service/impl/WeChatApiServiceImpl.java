package com.tidc.manage.service.impl;

import com.tidc.manage.dto.DurationRankingDTO;
import com.tidc.manage.service.IWeChatApiService;
import com.tidc.manage.vo.WeChatApiVO;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Description:
 *
 * @author zcolder
 * @date 2018-10-15
 */
@Service
public class WeChatApiServiceImpl implements IWeChatApiService {

    @Override
    public List getDurationRanking(long current, long size, int sort) {
        RestTemplate restTemplate = new RestTemplate();
        List list = restTemplate.getForObject(
                "https://api.cx-tidc.com/studio/sign/ranking?current={current}&count={size}&sort={sort}&raw=1",
                List.class,
                current,
                size,
                sort == 0 ? "desc" : "asc");
        if (list == null) {
            throw new NullPointerException("Response Data");
        }
        return list;
    }

}

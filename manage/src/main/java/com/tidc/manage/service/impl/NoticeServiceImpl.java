package com.tidc.manage.service.impl;

import com.tidc.manage.domain.Notice;
import com.tidc.manage.mapper.NoticeMapper;
import com.tidc.manage.service.INoticeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 张朝锋
 * @author 张伟杰
 * @since 2018-10-04
 */
@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice> implements INoticeService {

    @Override
    public Integer saveNotice(Notice notice) {
        notice.setDate(System.currentTimeMillis() / 1000);
        return baseMapper.insert(notice);
    }

    @Override
    public Integer deleteNotice(Long id) throws NotFoundException {
        if (baseMapper.selectById(id) == null) {
            throw new NotFoundException("Notice("+id+")");
        }
        return baseMapper.deleteById(id);
    }

    @Override
    public Integer updateNotice(Notice notice) throws NotFoundException {
        if (baseMapper.selectById(notice.getId()) == null) {
            throw new NotFoundException("Notice("+notice.getId()+")");
        }
        return baseMapper.updateById(notice);
    }

    @Override
    public List<Notice> getListNotice() {
        return new Notice().selectAll();
    }
}

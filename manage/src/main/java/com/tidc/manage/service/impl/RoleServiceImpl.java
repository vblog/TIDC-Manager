package com.tidc.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tidc.manage.domain.Role;
import com.tidc.manage.mapper.RoleMapper;
import com.tidc.manage.service.IRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 张朝锋
 * @since 2018-09-14
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

    @Override
    public List<Role> getListRole() {
        return baseMapper.selectList(new QueryWrapper<>());
    }
}

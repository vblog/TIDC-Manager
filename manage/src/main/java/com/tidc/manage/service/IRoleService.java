package com.tidc.manage.service;

import com.tidc.manage.domain.Role;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 张朝锋
 * @author 张伟杰
 * @since 2018-09-14
 */
public interface IRoleService extends IService<Role> {

    /**
     * 获取所有的等级权限
     * @return roleList
     */
    List<Role> getListRole();

}

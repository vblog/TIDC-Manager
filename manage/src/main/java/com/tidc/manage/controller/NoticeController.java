package com.tidc.manage.controller;

import com.tidc.manage.domain.Notice;
import com.tidc.manage.service.INoticeService;
import com.tidc.manage.tool.RestfulVOResultBuilder;
import com.tidc.manage.vo.RestfulVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 公告 前端控制器
 * </p>
 *
 * @author 张朝锋
 * @since 2018-10-04
 */
@RestController
@RequestMapping("/manage/notice")
@Api(tags = "公告Api")
public class NoticeController implements RestfulVOResultBuilder {

    @Autowired
    private INoticeService noticeService;

    @ApiOperation(value = "添加公告")
    @PostMapping("/")
    @PreAuthorize(value = "hasAuthority('ROOT') or hasAuthority('SUPER_ROOT')")
    public RestfulVO<Integer> saveNotice(@Valid Notice notice) {
        return data(noticeService.saveNotice(notice));
    }

    @ApiOperation(value = "删除公告")
    @DeleteMapping("/{id:\\d+}")
    @PreAuthorize(value = "hasAuthority('ROOT') or hasAuthority('SUPER_ROOT')")
    public RestfulVO<Integer> deleteNoticeById(
            @Valid @PathVariable Long id)
            throws NotFoundException {
        return data(noticeService.deleteNotice(id));
    }

    @ApiOperation(value = "更新公告")
    @PutMapping("/{id:\\d+}")
    @PreAuthorize(value = "hasAuthority('ROOT') or hasAuthority('SUPER_ROOT')")
    public RestfulVO<Integer> updateNotice(
            @Valid @PathVariable Long id,
            @RequestBody Notice notice)
            throws NotFoundException {
        notice.setId(id);
        return data(noticeService.updateNotice(notice));
    }

    @ApiOperation(value = "获取公告，并不提供获取单个公告的接口!")
    @GetMapping("/")
    public RestfulVO<List<Notice>> getAllNotice() {
        return data(noticeService.getListNotice());
    }
}
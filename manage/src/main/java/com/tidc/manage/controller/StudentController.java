package com.tidc.manage.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tidc.manage.domain.Student;
import com.tidc.manage.enums.StateEnum;
import com.tidc.manage.service.IStudentService;
import com.tidc.manage.tool.RestfulVOResultBuilder;
import com.tidc.manage.vo.RestfulVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 张伟杰
 * @since 2018-09-14
 */
@RestController
@RequestMapping("/manage/students")
@Api(tags = "学生Api")
public class StudentController implements RestfulVOResultBuilder {

    @Autowired
    private IStudentService studentService;

    @ApiOperation(value = "添加或更新学生用户信息接口", notes = "当提交的信息存在于数据库中，则覆盖，否则添加")
    @PostMapping("/")
    @PreAuthorize(value = "hasAuthority('ROOT') or hasAuthority('SUPER_ROOT')")
    public RestfulVO<StateEnum> addStudentInfo(@Valid @RequestBody Student student){
        return data(studentService.saveStudent(student));
    }

    @ApiOperation(value = "获取所有的学生信息")
    @GetMapping("/")
    @PreAuthorize(value = "hasAuthority('ROOT') or hasAuthority('SUPER_ROOT')")
    public RestfulVO<IPage<Student>> getAllStudent(
            @RequestParam(defaultValue = "1") long current, @RequestParam(defaultValue = "8") long size, Student student){
        return data(studentService.getStudentOfPage(current, size, student));
    }

    @ApiOperation(value = "通过学号获取单个学生信息")
    @PreAuthorize(value = "principal.equals(#studentId) or hasAuthority('ROOT') or hasAuthority('SUPER_ROOT')")
    @GetMapping("/{studentId:^1\\d{9}$}")
    public RestfulVO<Student> getStudent(@PathVariable String studentId){
        return data(studentService.getStudentById(studentId));
    }

    @ApiOperation(value = "踢出成员，此操作为物理删除！！！")
    @PreAuthorize(value = "hasAuthority('ROOT') or hasAuthority('SUPER_ROOT')")
    @DeleteMapping("/{studentId:^1\\d{9}$}")
    public RestfulVO<Integer> deleteStudentByStudentId(@PathVariable String studentId) throws NotFoundException {
        return data(studentService.deleteStudentByStudentId(studentId));
    }
}

package com.tidc.manage.controller;

import com.tidc.manage.domain.Role;
import com.tidc.manage.service.IRoleService;
import com.tidc.manage.tool.RestfulVOResultBuilder;
import com.tidc.manage.vo.RestfulVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 权限前端控制器
 * </p>
 *
 * @author 张朝锋
 * @since 2018-09-14
 */
@RestController
@RequestMapping("/manage/role")
@Api(tags = "权限Api")
public class RoleController implements RestfulVOResultBuilder {

    @Autowired
    private IRoleService roleService;

    @ApiOperation("获取所有权限")
    @GetMapping("/")
    public RestfulVO<List<Role>> getAllRole() {
        return data(roleService.getListRole());
    }
}

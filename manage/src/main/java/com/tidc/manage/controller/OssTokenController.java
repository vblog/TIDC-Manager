package com.tidc.manage.controller;

import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.sts.model.v20150401.AssumeRoleResponse;
import com.tidc.manage.dto.CredentialsDTO;
import com.tidc.manage.oss.AllowPower;
import com.tidc.manage.tool.RestfulVOResultBuilder;
import com.tidc.manage.vo.RestfulVO;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 特殊的接口
 * 用于对象存储oss
 *
 * @author innoyiya
 * @date 2018/10/7.
 */
@Api(tags = "对象存储Api")
@RestController
@RequestMapping("/manage/ossToken")
public class OssTokenController implements RestfulVOResultBuilder {

    @Autowired
    private AllowPower allowPower;

    @GetMapping("/")
    public RestfulVO<CredentialsDTO> result() throws ClientException {
        AssumeRoleResponse response = allowPower.getAssumeRoleResponse();
        AssumeRoleResponse.Credentials credentials = response.getCredentials();
        return data(new CredentialsDTO(
                credentials.getExpiration(),
                credentials.getAccessKeyId(),
                credentials.getAccessKeySecret(),
                credentials.getSecurityToken()
        ));
    }
}

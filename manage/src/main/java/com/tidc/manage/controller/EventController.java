package com.tidc.manage.controller;

import com.tidc.manage.domain.Event;
import com.tidc.manage.enums.StateEnum;
import com.tidc.manage.exception.ZeroException;
import com.tidc.manage.service.IEventService;
import com.tidc.manage.tool.RestfulVOResultBuilder;
import com.tidc.manage.vo.RestfulVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 事件event 前端控制器
 * </p>
 *
 * @author 张朝锋
 * @since 2018-09-14
 */
@RestController
@RequestMapping("/manage/events")
@Api(tags = "事件Api")
public class EventController implements RestfulVOResultBuilder {

    @Autowired
    private IEventService eventService;

    @ApiOperation(value = "添加事件", notes = "id自增")
    @PostMapping("/")
    @PreAuthorize(value = "hasAuthority('ROOT') or hasAuthority('SUPER_ROOT')")
    public RestfulVO<Integer> addEvent(@Valid @RequestBody Event event) throws ZeroException {
        return data(eventService.addEvent(event));
    }

    @ApiOperation(value = "删除事件", notes = "通过id删除指定事件")
    @DeleteMapping("/{id:\\d+}")
    @PreAuthorize(value = "hasAuthority('ROOT') or hasAuthority('SUPER_ROOT')")
    public RestfulVO<Integer> deleteEvent(@Valid @PathVariable Long id) {
        boolean flag = eventService.removeById(id);
        return data(flag ? StateEnum.SUCCEED.ordinal() : StateEnum.FAIL.ordinal());
    }

    @ApiOperation("获取所有事件的接口")
    @GetMapping("/")
    @PreAuthorize(value = "hasAuthority('ROOT') or hasAuthority('SUPER_ROOT')")
    public RestfulVO<List<Event>> getAllEvent() {
        return data(new Event().selectAll());
    }

}

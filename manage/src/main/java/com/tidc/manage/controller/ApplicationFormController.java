package com.tidc.manage.controller;

import com.tidc.manage.domain.ApplicationForm;
import com.tidc.manage.exception.ZeroException;
import com.tidc.manage.service.IApplicationFormService;
import com.tidc.manage.tool.RestfulVOResultBuilder;
import com.tidc.manage.vo.RestfulVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * ApplicationForm科创工作室申请表 前端控制器
 * </p>
 *
 * @author 张伟杰
 * @since 2018-09-23
 */
@RestController
@Api(tags = "申请表Api")
@RequestMapping("/manage/application-form")
public class ApplicationFormController implements RestfulVOResultBuilder {

    @Autowired
    private IApplicationFormService applicationFormService;

    @ApiOperation(value = "提交申请表", notes = "不用添加时间数据")
    @PostMapping("/")
    public RestfulVO<Integer> postApplicationForm(@Valid @RequestBody ApplicationForm form) throws ZeroException {
        return data(applicationFormService.saveApplicationForm(form));
    }

    @ApiOperation(value = "根据学号获取申请表")
    @PreAuthorize(value = "hasAuthority('ROOT') or hasAuthority('SUPER_ROOT')")
    @GetMapping("/{studentId:^1\\d{9}$}")
    public RestfulVO<ApplicationForm> getApplicationForm(@Valid @PathVariable String studentId) {
        return data(applicationFormService.getSingleApplicationForm(studentId));
    }

    @ApiOperation(value = "获取所有的申请表")
    @PreAuthorize(value = "hasAuthority('ROOT') or hasAuthority('SUPER_ROOT')")
    @GetMapping("/")
    public RestfulVO<List<ApplicationForm>> listApplicationForms() {
        return data(applicationFormService.listApplicationForms());
    }

    @ApiOperation(value = "修改申请表审核状态", notes = "只需的id和state状态码(0未审核, 1通过 , 2不通过)")
    @PreAuthorize(value = "hasAuthority('ROOT') or hasAuthority('SUPER_ROOT')")
    @PatchMapping("/{id:\\d+}")
    public RestfulVO<Integer> updateApplicationFormOfState(
            @Valid @PathVariable int id,
            @Valid @RequestBody ApplicationForm applicationForm) throws NotFoundException {
        return data(applicationFormService.updateApplicationFormOfState(id, applicationForm));
    }

    @PreAuthorize(value = "hasAuthority('ROOT') or hasAuthority('SUPER_ROOT')")
    @ApiOperation("删除申请表")
    @DeleteMapping("/{id:\\d+}")
    public RestfulVO<Integer> deleteApplicationFormById(@PathVariable Long id) throws NotFoundException {
        return data(applicationFormService.deleteApplicationFormById(id));
    }
}

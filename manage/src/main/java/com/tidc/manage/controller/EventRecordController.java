package com.tidc.manage.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tidc.manage.domain.EventRecord;
import com.tidc.manage.exception.ZeroException;
import com.tidc.manage.service.IEventRecordService;
import com.tidc.manage.tool.RestfulVOResultBuilder;
import com.tidc.manage.vo.RestfulVO;
import com.tidc.manage.vo.ScoreRankingVO;
import com.tidc.manage.vo.StudentEventRecordVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

/**
 * <p>
 * 学生积分事件记录 前端控制器
 * </p>
 *
 * @author 张朝锋
 * @since 2018-09-14
 */
@RestController
@RequestMapping("/manage/event-records")
@Api(tags = "事件记录Api")
@Log4j2
public class EventRecordController implements RestfulVOResultBuilder {

    @Autowired
    private IEventRecordService eventRecordService;

    @ApiOperation(value = "单个学生的总积分接口")
    @PreAuthorize(value = "principal.equals(#studentId)")
    @GetMapping("/totalScore/{studentId:^1\\d{9}$}")
    public RestfulVO<Map<String, Integer>> getTotalScoreByStudentId(@Valid @PathVariable String studentId) {
        return data(eventRecordService.getTotalScoreByStudentId(studentId));
    }

    @ApiOperation(
            value = "积分排行榜接口",
            notes = "如果order=0 则不排序 " +
                    "如果grade=0 在校生积分排行榜，否则 如N年的N~(N-2)年级的学生排行")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order", value = "如果order为1，则分数(score)为1 降序，为-1 升序"),
            @ApiImplicitParam(name = "current", value = "当前页数，从1开始", required = true),
            @ApiImplicitParam(name = "size", value = "每页所需条数"),
            @ApiImplicitParam(name = "grade", value = "如果grade是一个存在的年级，则只查询这个年级:" +
                    "如果grade是一个不存的年级，则返回在校生的排行榜")
    })
    @GetMapping("/rankingList/")
    public RestfulVO<IPage<ScoreRankingVO>> getRankingListByGrade(
            @RequestParam(value = "grade", defaultValue = "0") int grade,
            @RequestParam(defaultValue = "1") long current,
            @RequestParam(defaultValue = "8") long size,
            @RequestParam(value = "order", defaultValue = "1") int order) throws ZeroException {
        return data(eventRecordService.getRankingListByGrade(order, current, size, grade));
    }

    @ApiOperation(value = "所有学生事件记录接口", notes = "获取所有学生事件记录，按时间顺序")
    @PreAuthorize(value = "hasAuthority('STUDENT') or hasAuthority('ROOT') or hasAuthority('SUPER_ROOT')")
    @GetMapping("/")
    public RestfulVO<IPage<StudentEventRecordVO>> getAllEventRecord(
            @RequestParam(defaultValue = "1") long current,
            @RequestParam(defaultValue = "8") long size) {
        log.debug(Long.toString(size));
        return data(eventRecordService.getAllEventRecordOfPage(current, size));
    }

    @ApiOperation(value = "通过对指定学生添加事件，来达到加扣分的目的", notes = "这里只需要三个参数eventId事件id, studentId学号, score分数")
    @PreAuthorize(value = "hasAuthority('ROOT') or hasAuthority('SUPER_ROOT')")
    @PostMapping("/")
    public RestfulVO<Integer> addEventRecordForStudentId(
            @Valid @RequestBody EventRecord eventRecord) throws ZeroException, NotFoundException {
        return data(eventRecordService.saveEventRecordForStudentId(eventRecord));
    }

    @ApiOperation(value = "单个学生的所有事件记录接口", notes = "获取单个学生的所有事件记录")
    @PreAuthorize(value = "principal.equals(#studentId)")
    @GetMapping("/{studentId:^1\\d{9}$}")
    public RestfulVO<IPage<StudentEventRecordVO>> getEventRecordByStudentId(
            @Valid @PathVariable String studentId,
            @RequestParam(defaultValue = "1") long current,
            @RequestParam(defaultValue = "8") long size) {
        return data(eventRecordService.getEventRecordByStudentId(studentId, current, size));
    }

    @ApiOperation(value = "通过指定的Id,删除事件")
    @PreAuthorize(value = "hasAuthority('ROOT') or hasAuthority('SUPER_ROOT')")
    @DeleteMapping("/{id:\\d+}")
    public RestfulVO<Integer> deleteEventRecordById(@Valid @PathVariable long id) throws NotFoundException {
        return data(eventRecordService.deleteEventRecordById(id));
    }

}

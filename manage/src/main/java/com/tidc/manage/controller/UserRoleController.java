package com.tidc.manage.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tidc.manage.domain.UserRole;
import com.tidc.manage.service.IUserRoleService;
import com.tidc.manage.tool.RestfulVOResultBuilder;
import com.tidc.manage.vo.RestfulVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 用户权限 前端控制器
 * </p>
 *
 * @author 张朝锋
 * @since 2018-09-14
 */
@RestController
@RequestMapping("/manage/user-role")
@Api(tags = "用户权限Api")
public class UserRoleController implements RestfulVOResultBuilder {

    @Autowired
    private IUserRoleService userRoleService;

    @ApiOperation(value = "修改用户权限", notes = "需要的参数是用户id(userId)和新的权限id(roleId)")
    @PreAuthorize(value = "hasAuthority('SUPER_ROOT')")
    @PostMapping("/")
    public RestfulVO<Integer> updateUserRoleByUserId(
            @Valid @RequestBody UserRole userRole) throws NotFoundException {
        return data(userRoleService.updateUserRoleByUserId(userRole));
    }

    @ApiOperation(value = "分页获取所有的用户权限",
            notes = "current:页数, size:每页条数, level:指定权限等级")
    @PreAuthorize(value = "hasAuthority('ROOT') or hasAuthority('SUPER_ROOT')")
    @GetMapping("/")
    public RestfulVO<IPage<UserRole>> getUserRoleOfPage(
            @RequestParam(defaultValue = "1") long current,
            @RequestParam(defaultValue = "8") long size,
            @RequestParam(defaultValue = "-1") int level) {
        return data(userRoleService.getUserRoleOfPage(current, size, level));
    }
}

package com.tidc.manage.controller;

import com.tidc.manage.service.IWeChatApiService;
import com.tidc.manage.tool.RestfulVOResultBuilder;
import com.tidc.manage.vo.RestfulVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description:
 * 微信小程序接口，提供获取微信小程序独有的数据，如：签到
 *
 * @author zcolder
 * @date 2018-10-15
 */
@Api(tags = "微信小程序接口Api")
@RestController
@RequestMapping("/manage/wechat-api")
public class WeChatApiController implements RestfulVOResultBuilder {

    @Autowired
    private IWeChatApiService weChatApiService;

    @ApiOperation("获取小时数排行接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "current", value = "默认是0，小程序的默认接口是0页起数"),
            @ApiImplicitParam(name = "size", value = "每页请求条数，这里默认是8"),
            @ApiImplicitParam(name = "sort", value = "如果sort为0，则为降序，为1升序")
    })
    @GetMapping("/ranking")
    public RestfulVO<?> getDurationRanking(
            @RequestParam(defaultValue = "0") long current,
            @RequestParam(defaultValue = "8") long size,
            @RequestParam(defaultValue = "0") int sort) {
        return data(weChatApiService.getDurationRanking(current, size, sort));
    }

}

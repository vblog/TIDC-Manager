package com.tidc.manage.security.controller;

import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description:
 *  用于暴露给前端的csrf接口
 *  csrf防御已经关闭
 * @author 张朝锋
 * @date 2018-09-29
 */
@Controller
public class CsrfController {

    /**
     * 暴露给部分支持csrf的前端框架
     * @return csrfToken
     */
    @GetMapping("/csrf")
    public String csrf(){
        return "/manage/csrf";
    }

    /**
     * 暴露给前端使用的csrf接口
     * @param token csrfToken
     * @return csrfToken
     */
    @GetMapping("/manage/csrf")
    @ResponseBody
    public CsrfToken manageCsrf(CsrfToken token) {
        return token;
    }
}

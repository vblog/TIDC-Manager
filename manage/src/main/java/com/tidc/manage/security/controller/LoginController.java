package com.tidc.manage.security.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Description:
 *
 * @author 张朝锋
 * @date 2018-09-29
 */
@Controller
public class LoginController {

    /**
     * 请求登录页面，这个View主要用来解决CSRF，传递CSRF到登录页
     * @return 逻辑视图 login
     */
    @GetMapping("/login")
    public String login(){
        return "manage/users/login";
    }
}

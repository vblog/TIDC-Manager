package com.tidc.manage.mapper;

import com.tidc.manage.domain.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 张朝锋
 * @author 张伟杰
 * @since 2018-09-14
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

}

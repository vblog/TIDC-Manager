package com.tidc.manage.mapper;

import com.tidc.manage.domain.Notice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  公告 Mapper 接口
 * </p>
 *
 * @author 张朝锋
 * @since 2018-10-04
 */
public interface NoticeMapper extends BaseMapper<Notice> {

}

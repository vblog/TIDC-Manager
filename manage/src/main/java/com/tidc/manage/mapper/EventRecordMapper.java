package com.tidc.manage.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tidc.manage.domain.EventRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tidc.manage.vo.ScoreRankingVO;
import com.tidc.manage.vo.StudentEventRecordVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 张朝锋
 * @author 张伟杰
 * @since 2018-09-14
 */
@Repository
public interface EventRecordMapper extends BaseMapper<EventRecord> {
    /**
     * 查询指定学号的学生的总分
     *
     * @param studentId 学号
     * @return 对应学生id(学号)的学生总积分
     */
    int getTotalScoreByStudentId(String studentId);

    /**
     * 按年级查询分数排行榜
     *
     * @param page   分页对象
     * @param grades 年级
     * @return 分数排行榜
     */
    IPage<ScoreRankingVO> getScoreRanking(Page<ScoreRankingVO> page, @Param("grades") List<Integer> grades);

    /**
     * 分页查询所有学生事件记录，默认10条一页
     *
     * @param page 分页对象
     * @return 所有学生事件记录，默认按时间降序
     */
    IPage<StudentEventRecordVO> getAllEventRecord(Page<StudentEventRecordVO> page);

    /**
     * 分页查询指定学生事件记录，默认10条一页
     * @param page 分页对象
     * @param studentId 学号
     * @return 指定学生的所有事件记录
     */
    IPage<StudentEventRecordVO> getEventRecordByStudentId(Page<StudentEventRecordVO > page, @Param("studentId") String studentId);
}

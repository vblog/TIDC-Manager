package com.tidc.manage.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tidc.manage.domain.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 张朝锋
 * @since 2018-09-14
 */
@Repository
public interface UserRoleMapper extends BaseMapper<UserRole> {

    /**
     * 分页获取所有用户
     * @param userRolePage 用户权限分页对象
     * @param level 是否指定查询等级
     * @return IPage<UserRole>
     */
    IPage<UserRole> getUserRoleOfPage(Page<UserRole> userRolePage, @Param("level") int level);
}

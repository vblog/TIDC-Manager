package com.tidc.manage.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tidc.manage.domain.Student;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 张朝锋
 * @author 张伟杰
 * @since 2018-09-14
 */
@Repository
public interface StudentMapper extends BaseMapper<Student> {
    /**
     * 分页查询指定条件的学生
     *
     * @param studentPage 分页对象
     * @param student     封装查询条件的学生类
     * @return 执行结果
     */
    IPage<Student> getStudentAllOfPage(Page<Student> studentPage, @Param("student") Student student);
}

package com.tidc.manage.mapper;

import com.tidc.manage.domain.ApplicationForm;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 张伟杰
 * @since 2018-09-23
 */
@Repository
public interface ApplicationFormMapper extends BaseMapper<ApplicationForm> {
    /**
     * 修改申请表的state状态
     *
     * @param id    申请表id
     * @param state 新的state状态
     */
    void updateApplicationFormForStateById(@Param("id") int id, @Param("state") int state);
}

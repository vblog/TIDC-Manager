package com.tidc.manage.dto;

import lombok.Data;
import lombok.ToString;

/**
 * Description:
 * 用于封装 微信小程序小时数排行版的信息实体类
 *
 * @author zcolder
 * @date 2018-10-15
 */
@Data
@ToString
public class DurationRankingDTO {
    private String userId;
    private String userName;
    private String userSignDuration;
}

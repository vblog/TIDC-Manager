package com.tidc.manage.dto;

import lombok.AllArgsConstructor;

/**
 * Description:
 * 用于封装 oss凭据信息实体类
 *
 * @author zcolder
 * @date 2018-10-15
 */
@AllArgsConstructor
public class CredentialsDTO {
    String Expiration;
    String AccessKeyId;
    String AccessKeySecret;
    String SecurityToken;
}

package com;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import org.springframework.beans.factory.annotation.Value;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Description:
 * mybatis代码自动生成器
 * <p>
 * 并不是所有的表都有自增id字段，所以在策略配置中
 * {@link StrategyConfig#getSuperEntityClass()
 * 并没有配置继承
 * @link com.tidc.manage.domain.SuperEntity}
 * 这里需要针对自增id的映射实体类手动继承
 * </p>
 * <p>
 * 后期维护的时候，建议main局部变量tables，填写新增的表名，而不是全局
 * </p>
 * @date 2018-09-13
 */
@Deprecated
public class CodeGenerator {

    /**
     * 数据库名
     */
    @Value("${spring.database.username}")
    private static String databaseUsername;

    /**
     * 数据库密码
     */
    @Value("${spring.database.password}")
    private static String databasePassword;

    public static void mian(String[] args) {
        //数据库url
        String databaseUrl = "jdbc:mysql://211.159.186.188/tidc_manage_db?characterEncoding=utf-8";

        //模块名
        String moduleName = "manage";
        //数据库表
        String[] tables = new String[]{"notice"};

        //每个class文件上的注解
        String author = "张朝锋\n * @author 张伟杰";

        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = System.getProperty("user.dir");
        gc.setOutputDir(projectPath + "/src/main/java");
        gc.setAuthor(author);
        gc.setOpen(false);
        gc.setBaseResultMap(true);
        gc.setBaseColumnList(true);
        mpg.setGlobalConfig(gc);

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl(databaseUrl);
        dsc.setDriverName("com.mysql.jdbc.Driver");
        dsc.setUsername(databaseUsername);
        dsc.setPassword(databasePassword);
        mpg.setDataSource(dsc);

        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setModuleName(moduleName);
        pc.setParent("com.tidc");
        pc.setEntity("domain");
        mpg.setPackageInfo(pc);

        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };
        List<FileOutConfig> focList = new ArrayList<>();
        focList.add(new FileOutConfig("/templates/mapper.xml.ftl") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输入文件名称
                return projectPath + "/src/main/resources/mapper/" + pc.getModuleName()
                        + "/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
            }
        });
        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);
        mpg.setTemplate(new TemplateConfig().setXml(null));

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        strategy.setEntityLombokModel(true);
        strategy.setInclude(tables);
        strategy.setSuperEntityColumns("id");
        strategy.setControllerMappingHyphenStyle(true);
        strategy.setRestControllerStyle(true);
        mpg.setStrategy(strategy);
        mpg.setTemplateEngine(new FreemarkerTemplateEngine());
        mpg.execute();
    }

}

import Home from '../views/Home.vue'

// import ProductShow from '../views/ProductShow.vue'
// import Rank from '../views/Rank.vue'
// import ResourceHub from '../views/ResourceHub.vue'
// import InnovateBox from '../views/InnovateBox.vue'

export default [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/products',
        name: 'product show',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import('../views/ProductShow.vue')
    },
    {
        path: '/rank',
        name: 'rank',
        component: () => import('../views/Rank.vue')
    },
    {
        path: '/resource',
        name: 'ResourceHub',
        component: () => import("../views/ResourceHub.vue")
    },
    {
        path: '/box',
        name: 'innovate box',
        component: () => import("../views/InnovateBox.vue")
    }
]
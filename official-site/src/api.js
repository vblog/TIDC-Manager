import axios from 'axios'

axios.defaults.baseURL = 'https://www.cx-tidc.com/api'

export default {
    getTest() {
        return unbufferGet('comments')
    },
    getCarousel() {
        return unbufferGet('carousel')
    },
    getRank() {
        return unbufferGet('rank')
    },
    getProductList() {
        return unbufferGet('product')
    },
    getContactList() {
        return unbufferGet('contact')
    },
    pushComment(data) {
        return ajax('comments', 'post', data)
    },
    getComment() {
        return unbufferGet('comments')
    },
}

function ajax(url, method, options) {
    if (options !== undefined) {
        var { params = {}, data = {} } = options
    } else {
        params = data = {}
    }
    return new Promise((resolve, reject) => {
        axios({
            url,
            method,
            params,
            data
        }).then(res => {
            resolve(res)
        }, res => {
            reject(res)
        })
    })
}

function unbufferGet(url) {
    return ajax(url + '?_t=' + new Date().valueOf(), 'get')
}
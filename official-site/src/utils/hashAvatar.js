import Identicon from "identicon.js";
import md5 from "md5";

function makeHashAvatar(str, size) {
    const data = new Identicon(
        md5(str),
        Number(size)
    ).toString();
    return `data:text/html;base64,${data}`;
}


export {
    makeHashAvatar
};
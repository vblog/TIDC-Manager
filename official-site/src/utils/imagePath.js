function getPath(url) {
    if (url.indexOf('//') !== -1) {
        return url;
    } else {
        // return require(`@/assets/${url}`)
        return `https://tidc-drive.oss-cn-shenzhen.aliyuncs.com/static/${url}`;
    }
}

export default getPath;
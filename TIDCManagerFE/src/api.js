import _axios from "axios";
import store from "@/store";
import types from "@/store/types";
// import reqstatus from "@/utils/requestStatus";

const axios = _axios.create({
  baseURL: "https://java.cx-tidc.com/manage",
  // baseURL: "//10.0.19.140:8080/manage",
  // baseURL: "//10.0.19.35:8080/manage",
  timeout: 2000,
  // withCredentials: true,
  retry: 8,
  retryDelay: 1000,
});

axios.interceptors.response.use(
  undefined,
  error => {
    const { config } = error;
    // 服务器存在response并返回了code，进行全局错误处理
    if (error.response) {
      switch (error.response.data.code) {
      case 1005:
        // TODO 服务器返回1005时，刷新Token
        store.dispatch("clearLocalData");
        return Promise.reject(error);

      case 1006:
      default:
        return Promise.reject(error);
      }
    }

    // 服务器没response，说明请求超时或服务器炸了，尝试重新发起请求
    if (!config || !config.retry) return Promise.reject(error);
    config.__retryCount = config.__retryCount || 0;

    if (config.__retryCount >= config.retry) {
      return Promise.reject(error);
    }
    config.__retryCount += 1;

    // 推迟执行
    let backoff = new Promise(function (resolve) {
      setTimeout(function () {
        resolve();
      }, config.retryDelay || 16);
    });

    return backoff.then(function () {
      return axios(config);
    });
  }
);

const api = {
  test() {
  },
  getCsrfToken() {
    return ajax("csrf", "get");
  },
  /* -----用户API----- */
  /**
     * 登陆
     * @param {Object} data
     * @param {String} data.id
     * @param {String} data.password
     */

  login(data) {
    return ajax("users/login", "post", {
      data
    });
  },
  /**
   * 登出
   */
  logout(params) {
    return ajax(`users/logout/${params.userId}`, "get");
  },
  /**
   * 通过token获取登陆用户权限等级
   * @param {Object} data 
   * @param {String} data.accessToken 
   */
  getUserLevel(data) {
    return;
  },
  /** 
   * 通过token获取登陆用户的个人资料
   * @param {Object} data 
   * @param {String} data.accessToken 
   */
  getProfile(data) {
    return;
  },

  /** 
 * 修改用户的密码
 * @param {Object} data 
 * @param {String} data.password
 */
  updateUserForPassword(userId, data) {
    return ajax(`/users/${userId}`, "put", {
      data
    });
  },

  /* -----学生API----- */
  /**
     * 添加新成员
     * @param {Object} data
     * @param {String} [data.blogUrl]   博客地址
     * @param {String} data.className   班级
     * @param {String} data.email       邮箱
     * @param {String} data.gender      性别，男: 1; 女: 2;
     * @param {String} data.grade       完整年纪，如：2016
     * @param {String} data.identity    身份证号
     * @param {String} data.major       发展学习方向
     * @param {String} data.name        名字
     * @param {String} data.nativePlace 籍贯
     * @param {String} data.phoneNumber 手机号
     * @param {String} data.qq          QQ号
     * @param {String} data.studentId   学号
     * @param {String} data.wechat      微信号
     */
  updateMember(data) {
    return ajax("students/", "post", {
      data
    });
  },
  /**
   * 获取成员列表
   * @param {Object} params
   * @param {Number} params.size
   * @param {Number} params.current
   */
  getMembers(params) {
    return ajax("students/", "get", {
      params
    });
  },
  /**
   * 通过学号获取单个成员信息
   * @param {Object} params
   * @param {String} params.studentId
   */
  getMember(params) {
    return ajax(`students/${params.id || params.studentId}`, "get");
  },
  hasMember(params) {
    return ajax(`users/has/${params.studentId}`, "get");
  },
  deleteMember(params) {
    return ajax(`students/${params.studentId}`, "delete");
  },

  /* -----事件记录API----- */
  /**
     * 通过对指定学生添加事件，进行加扣分
     * @param {Object} data  需要三个参数 eventId 事件id, studentId 学号, score 分数
     * @param {String} data.eventId
     * @param {Number} data.score
     * @param {String} data.studentId
     */
  addEventRecord(data) {
    return ajax("event-records/", "post", {
      data,
    });
  },
  /**
     * 获取所有学生事件记录，按时间顺序
     * @param {Object} params 分页数据
     * @param {Number} params.current
     * @param {Number} params.size 
     */
  getEventRecords(params) {
    return ajax("event-records/", "get", {
      params
    });
  },
  /**
     * 获取单个学生的所有事件记录
     * @param {Object} params {页码，学号}
     * @param {Number} params.current
     * @param {Number} params.size 
     * @param {String} params.studentId
     */
  getEventRecord(params) {
    return ajax("event-records", "get", {
      params
    });
  },
  /**
     * 获取积分排行榜, { 页码, 年级, 升降排序{-1|1} }
     * @param {Object} params
     * @param {Number} params.current
     * @param {String} params.grade
     * @param {String} params.order
     */
  getRankingList(params) {
    return ajax("event-records/rankingList", "get", {
      params
    });
  },
  /**
     * 获取学生的总积分
     * @param {Object} params
     * @param {String} params.studentId
     */
  getTotalScore(params) {
    return ajax("event-records/totalScore", "get", {
      params
    });
  },

  /* -----事件API----- */
  /**
     * 获取所有事件
     */
  getEvents() {
    return ajax("events/", "get");
  },
  /**
     * 添加事件 {事件描述, 分数}
     * @param {Object} data
     * @param {String} data.description
     * @param {Number} data.score
     */
  addEvent(data) {
    return ajax("events/", "post", {
      data
    });
  },

  /**
     * 删除事件
     * @param {Object} params
     * @param {Number} params.id
     */
  deleteEvent(params) {
    return ajax(`events/${params.id}`, "delete");
  },

  /* -----申请表API----- */
  submitApplyForm(data) {
    return ajax("application-form/", "post", {
      data
    });
  },
  getAllApplyForm() {
    return ajax("application-form/", "get");
  },
  /**
   * 修改申请表审核状态
   * @param {Object} options 
   * @param {Object} options.params
   * @param {Number} options.params.id
   * @param {Object} options.data
   * @param {Number} options.data.state
   */
  changeApplyStatus(options) {
    const { data, params } = options;
    return ajax(`application-form/${params.id}`, "patch", {
      data
    });
  },
  /**
   * 删除申请表
   * @param {Object} params 
   * @param {Number} params.id
   */
  deleteApplyForm(params) {
    return ajax(`application-form/${params.id}`, "delete");
  },

  /**
   * 迁移申请表用户信息到成员表里申请表
   * @param {Object} params 
   * @param {Number} params.id
   */
  migrateApplyForm(params) {
    return ajax(`application-form/migrate/${params.id}`, "post");
  }
};
export default api;

/**
 * @param url
 * @param method get|post|put|delete...
 * @param params like queryString. if a url is index?a=1&b=2, params = {a: '1', b: '2'}
 * @param data post data, use for method put|post
 * @returns {Promise}
 */
function ajax(url, method, options) {
  let params = {}, data = {};
  if (options !== undefined) {
    params = options.params || {};
    data = options.data || {};
  } else {
    params = data = {};
  }
  if (store.getters.hasToken) {
    params["access_token"] = data["access_token"] = store.getters.accessToken;
  }

  return new Promise((resolve, reject) => {
    axios({
      url,
      method,
      params,
      data,
    }).then((res) => {
      resolve(res);
    }, (error) => {
      // 将服务器的错误码存入vuex中，用于全局错误提示
      store.commit(types.CHANGE_REQUEST_ERROR_STATUS, error.response.data);
      reject(error);
    });
  });
}

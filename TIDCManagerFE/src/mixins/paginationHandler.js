/**
 * mixins的组件需要先定义以下属性 
 *  this.methods.updateTableData  从服务器获取数据
 */
export default {
  data() {
    return {
      current: 1,
      size: 8,
      pages: 0,
      total: 0,
    };
  },
  methods: {
    handleSizeChange(size) {
      this.size = size;
      this.updateTableData();
    },
    handleCurrentChange(current) {
      this.current = current;
      this.updateTableData();
    },
  }
};

import XLSX from "xlsx";

export default {
  methods: {
    exportDataToExcel() {
      const { data = [], name = "null" } = this;
      const ws = XLSX.utils.json_to_sheet(data);
      const wb = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, name);
      XLSX.writeFile(wb, `${name}.xlsx`);
    }
  }
};

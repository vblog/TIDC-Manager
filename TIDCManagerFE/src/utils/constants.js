export const REQUEST_STATUS = {
  SUCCESS: "success",
  FAILED: "failed"
};

export const USER_TYPE = {
  SUPER_ADMIN: 0,
  ADMIN: 1,
  REGULAR_USER: 2
};

export const APPLY_STATUS = {
  AWAIT: 0,
  PASSED: 1,
  FAILED: 2
};

export const APPLY_STATUS_TITLE = {
  0: { type: "info", text: "await" },
  1: { type: "success", text: "passed" },
  2: { type: "danger", text: "failed" }
};
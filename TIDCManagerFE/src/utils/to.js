export default (promise) => {
  return promise
    .then(val => [null, val])
    .catch(err => [err]);
};

// 上面的处理法
// const [err, val] = await to(api.request())
// if (err !== null) {
//   //TODO
// }

// 一般处理请求异常
// try {
//     const val = await api.request();
// } catch (err) {
//     console.error(err);
// }
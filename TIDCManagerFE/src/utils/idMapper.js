export default (arrLike) => {
  const arr = [];
  arrLike.forEach(el => {
    arr[el.id] = el;
  });
  return arr;
};
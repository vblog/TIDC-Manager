export default (func, wait) => {
  let timeout;
  return () => {
    const args = arguments;

    if (timeout) {
      clearTimeout(timeout);
    }

    timeout = setTimeout(() => {
      func(args);
    }, wait);
  };
};

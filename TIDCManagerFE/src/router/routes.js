import AddMember from "@/views/addMember";
import IntegralApplication from "@/views/integralApplication";

export default [
  {
    path: "/user",
    meta: {
      title: "个人中心",
      icon: "el-icon-setting",
    },
    component: () => import("@/views/userProfile"),

  },
  {
    path: "/review",
    name: "review",
    meta: {
      title: "审核申请",
      ruleLevel: 2
    },
    component: () => import("@/views/ReviewList.vue")
  },
  {
    path: "/members",
    name: "members",
    component: () => import("@/views/members"),
    meta: {
      title: "成员",
      ruleLevel: 2
    },
    children: [
      {
        path: "",
        component: () => import("@/views/members/MemberList"),
        meta: {
          title: "成员列表",
        },
      },
      {
        path: "add",
        component: () => import("@/components/MemberEditer"),
        meta: {
          title: "添加成员",
        },
      },
      {
        path: "edit/:id",
        component: () => import("@/views/members/MemberList"),
      },
      {
        path: "apply-score/:id",
        component: () => import("@/views/members/MemberList"),
      }
    ]
  },
  {
    path: "/events",
    name: "events",
    component: () => import("@/views/events"),
    meta: {
      title: "事件",
      ruleLevel: 2
    },
    children: [
      {
        path: "",
        meta: {
          title: "事件列表"
        },
        component: () => import("@/views/events/EventList"),
      },
      {
        path: "add",
        component: () => import("@/views/events/EventEditer"),
      }
    ]
  },
  {
    path: "/score",
    name: "score",
    meta: {
      title: "积分",
      ruleLevel: 2
    },
    component: () => import("@/views/score"),
    children: [
      {
        path: "",
        meta: {
          title: "加扣分记录"
        },
        component: () => import("@/views/score/ScoreList"),
      },
      {
        path: "apply",
        component: () => import("@/views/score/ScoreApplyList"),
      }
    ]
  },
  {
    path: "/register",
    name: "register",
    meta: {
      title: "签到",
      ruleLevel: 2
    },
    component: () => import("@/views/register")
  },
  {
    path: "/innovative-box",
    name: "innovative-box",
    meta: {
      title: "创新盒子",
      ruleLevel: 2
    }
  },
  {
    path: "*",
    redirect: "/user"
  }
];



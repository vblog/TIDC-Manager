function keyMirror(obj) {
  if (obj instanceof Object) {
    const _obj = Object.assign({}, obj);
    const _keyArray = Object.keys(obj);
    _keyArray.forEach((key) => {
      _obj[key] = key;
    });
    return _obj;
  }
}

export default keyMirror({
  CHANGE_REQUEST_ERROR_STATUS: null,
  CHANGE_PROFILE: null,
  CHANGE_ACCESS_TOKEN: null,
  CHANGE_USER_LEVEL: null,
  CHANGE_CSRF_TOKEN: null,
  CHANGE_SIGN_STATUS: null,
});

import Vue from "vue";
import Vuex from "vuex";
import user from "./modules/user";
import types from "./types";

Vue.use(Vuex);

const state = {
  requestError: {
    code: null,
    msg: ""
  }
};
const mutations = {
  [types.CHANGE_REQUEST_ERROR_STATUS](state, { code, msg }) {
    state.requestError = {
      code,
      msg
    };
  }
};
const actions = {

};
const getters = {
  requestErrorCode: state => state.requestError.code
};
export default new Vuex.Store({
  modules: {
    user
  },
  state,
  getters,
  mutations,
  actions
});

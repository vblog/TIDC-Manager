module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    "plugin:vue/essential",
  ],
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    // 使用 === 替代 ==  
    "eqeqeq": [2, "allow-null"],
    "quotes": [2, "double", "avoid-escape"],
    semi: "error",
    "linebreak-style": 0,
    "indent": ["error", 2],
  },
  parserOptions: {
    parser: "babel-eslint",
  },
};

const str =
  " _________  ________  ______   ______      \n" +
  "/________/\\/_______/\\/_____/\\ /_____/\\     \n" +
  "\\__.::.__\\/\\__.::._\\/\\:::_ \\ \\\\:::__\\/     \n" +
  "   \\::\\ \\     \\::\\ \\  \\:\\ \\ \\ \\\\:\\ \\  __   \n" +
  "    \\::\\ \\    _\\::\\ \\__\\:\\ \\ \\ \\\\:\\ \\/_/\\  \n" +
  "     \\::\\ \\  /__\\::\\__/\\\\:\\/.:| |\\:\\_\\ \\ \\ \n" +
  "      \\__\\/  \\________\\/ \\____/_/ \\_____\\/ \n";

export default () => {
  console.log("%c%s", "color: #409EFF; font-size: 24px", str)
};
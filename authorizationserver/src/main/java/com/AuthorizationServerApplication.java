package com;

import com.tidc.security.config.MyUserDetailsServiceImpl;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

import java.util.Properties;

/**
 * @author:innoyiya
 */
@SpringBootApplication
@EnableAuthorizationServer
@MapperScan({
		"com.baomidou.mybatisplus.samples.quickstart.mapper",
		"com.tidc.security.mapper"
})
public class AuthorizationServerApplication extends SpringBootServletInitializer{

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(AuthorizationServerApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(AuthorizationServerApplication.class, args);
    }
    private static MyUserDetailsServiceImpl myUserDetailsService = new MyUserDetailsServiceImpl();

	public static MyUserDetailsServiceImpl getMyUserDetailsService() {
		return myUserDetailsService;
	}
}

package com.tidc.security.config;


import com.tidc.security.mapper.UserMapper;
import com.tidc.security.service.IUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * @author innoyiya
 * @date 2018/9/22.
 */
public class MyUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private IUserRoleService userRoleService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        com.tidc.security.domain.User priUser = userMapper.selectById(username);
        String role = userRoleService.getRoleStringByUserId(username);
        UserDetails userDetailsBySql = User.withUsername(username).password(priUser.getPassword()).authorities(role).build();
        return new User(userDetailsBySql.getUsername(), userDetailsBySql.getPassword(), userDetailsBySql.isEnabled(),
                userDetailsBySql.isAccountNonExpired(), userDetailsBySql.isCredentialsNonExpired(),
                userDetailsBySql.isAccountNonLocked(), userDetailsBySql.getAuthorities());
    }
}

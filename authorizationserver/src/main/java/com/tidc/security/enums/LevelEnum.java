package com.tidc.security.enums;

/**
 * @author innoyiya
 * @date 2018/9/14.
 */
public enum LevelEnum {

    /**
     * 超级管理员
     */
    SUPER_ROOT,
    /**
     * 普通管理员
     */
    ROOT,
    /**
     * 学生
     */
    STUDENT

}

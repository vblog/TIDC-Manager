package com.tidc.security.enums;

/**
 * @author innoyiya
 * @date 2018/9/15.
 */
public enum StateEnum {

    /**
     * 失败
     */
    FAIL,
    /**
     * 成功
     */
    SUCCEED,
    /**
     * 错误
     */
    ERROR,
    /**
     * 更新成功
     */
    UPDATE
}

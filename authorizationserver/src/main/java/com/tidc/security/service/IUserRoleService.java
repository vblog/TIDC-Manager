package com.tidc.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tidc.security.domain.UserRole;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 张朝锋
 * @author 张伟杰
 * @since 2018-09-14
 */
public interface IUserRoleService extends IService<UserRole> {

    /**
     * 通过学号获取用户角色
     * @param userId
     * @return
     */
    String getRoleStringByUserId(String userId);

    /**
     * 通过学号返回角色码
     * @param userId
     * @return
     */
    int getRoleLevelByUserId(String userId);
}

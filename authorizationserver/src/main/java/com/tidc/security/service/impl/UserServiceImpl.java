package com.tidc.security.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tidc.security.domain.User;
import com.tidc.security.mapper.RoleMapper;
import com.tidc.security.mapper.UserMapper;
import com.tidc.security.mapper.UserRoleMapper;
import com.tidc.security.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 张朝锋
 * @author 张伟杰
 * @since 2018-09-14
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserRoleMapper userRoleMapper;
    @Autowired
    private RoleMapper roleMapper;

    @Override
    public boolean checkPassword(String id, String password) {
        User user = userMapper.selectById(id);
        return user != null && password.equals(user.getPassword());
    }

}

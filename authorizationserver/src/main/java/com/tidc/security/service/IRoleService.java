package com.tidc.security.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.tidc.security.domain.Role;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 张朝锋
 * @author 张伟杰
 * @since 2018-09-14
 */
public interface IRoleService extends IService<Role> {

}

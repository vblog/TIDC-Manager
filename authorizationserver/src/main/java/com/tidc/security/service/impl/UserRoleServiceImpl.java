package com.tidc.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tidc.security.domain.Role;
import com.tidc.security.domain.UserRole;
import com.tidc.security.mapper.RoleMapper;
import com.tidc.security.mapper.UserRoleMapper;
import com.tidc.security.service.IUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tidc.security.tool.GetRoleByLevel;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 张朝锋
 * @author 张伟杰
 * @since 2018-09-14
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements IUserRoleService {

    @Autowired
    private UserRoleMapper userRoleMapper;
    @Autowired
    private RoleMapper roleMapper;

    @Override
    public String getRoleStringByUserId(String userId) {
        UserRole userRole = userRoleMapper.selectOne(new QueryWrapper<UserRole>().eq("user_id",userId));
        Role role = roleMapper.selectById(userRole.getRoleId());
        return GetRoleByLevel.transLevelToString(role.getLevel());
    }

    @Override
    public int getRoleLevelByUserId(String userId) {
        UserRole userRole = userRoleMapper.selectOne(new QueryWrapper<UserRole>().eq("user_id",userId));
        Role role = roleMapper.selectById(userRole.getRoleId());
        return role.getLevel();
    }
}

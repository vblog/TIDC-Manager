package com.tidc.security.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *  权限DO
 * </p>
 *
 * @author 张朝锋
 * @author 张伟杰
 * @since 2018-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Role extends SuperEntity<Role> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 权限级别
     */
    private Integer level;

}

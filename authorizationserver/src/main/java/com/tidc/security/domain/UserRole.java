package com.tidc.security.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *  用户权限关联 实际为DO
 * </p>
 *
 * @author 张伟杰
 * @since 2018-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UserRole extends SuperEntity<UserRole> implements Serializable {

    private static final long serialVersionUID = 1L;

    private String userId;

    private Integer roleId;


}

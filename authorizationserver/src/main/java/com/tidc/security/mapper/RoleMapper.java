package com.tidc.security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tidc.security.domain.Role;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 张朝锋
 * @author 张伟杰
 * @since 2018-09-14
 */
@Repository
public interface RoleMapper extends BaseMapper<Role> {

}

package com.tidc.security.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tidc.security.domain.UserRole;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 张朝锋
 * @author 张伟杰
 * @since 2018-09-14
 */
@Repository
public interface UserRoleMapper extends BaseMapper<UserRole> {

    /**
     * 通过用户的id返回角色的id
     * @param userId
     * @return
     */
    @Select("select role_id from user_role where user_id=#{userId}")
    int getRoleIdByUserId(@Param("userId") String userId);
}

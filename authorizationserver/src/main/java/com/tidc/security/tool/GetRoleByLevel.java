package com.tidc.security.tool;


import com.tidc.security.enums.LevelEnum;

/**
 * @author innoyiya
 * @date 2018/9/21.
 */
public class GetRoleByLevel {

    public static String transLevelToString(int level){
        switch (level) {
            case 0:
                return LevelEnum.SUPER_ROOT.toString();
            case 1:
                return LevelEnum.ROOT.toString();
            case 2:
                return LevelEnum.STUDENT.toString();
            default:
                return null;
        }
    }
}

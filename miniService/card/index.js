const autossh = require('autossh')
const client = autossh({
	host: '47.94.87.158',
	username: 'root',
	localPort: 22,
	remotePort: 6766,
	serverAliveInterval: 20,
	serverAliveCountMax: 2,
	sshPort: 4236,
//	privateKey: './privateKey',
	reverse: true
})
client.on('error', err => {
	console.error('ERROR: ', err);
})
client.on('connect', connection => {
	console.log('Tunnel established on port ' + connection.localPort);
	console.log('pid: ' + connection.pid);
});

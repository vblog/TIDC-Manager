module.exports = {

	get: {

		//获取微科创基本信息
		'/studio/common': async ctx => {
			await ctx.auth.allow('studio')
			let commonData = await ctx.hd.studio.getHomeDataCache()
			if(!commonData)
				commonData = await ctx.hd.studio.getHomeData()
			return commonData
		},

		//检查工作室人员登录状态
		'/studio/checkLogin': async ctx => {
			const {session} = await ctx.auth.allow('studio')
			return await ctx.hd.studio.checkLogin(session)
		},

		//获取当前签到状态
		'/studio/sign/status': async ctx => {
			const {userId} = await ctx.auth.allow('studio')
			return await ctx.hd.studio.checkSignStatus(userId)
		},

		//获取签到签退记录
		'/studio/sign/record': async ctx => {
			const {userId} = await ctx.auth.allow('studio')
			let signRecord = await ctx.hd.studio.getSignRecordCache(userId)
			if(!signRecord)
				signRecord = await ctx.hd.studio.getSignRecord(userId)
			return signRecord
		},

		//获取签到排行榜数据
		'/studio/sign/ranking': async ctx => {
			await ctx.auth.allow('nosign')
			const {current, count, sort, raw} = ctx.queryData
			let signRanking = await ctx.hd.studio.getSignRankingCache(current, count, sort)
			if(!signRanking)
				signRanking = await ctx.hd.studio.getSignRanking(current, count, sort)
			if(raw)
				return ctx.echo(signRanking)
			return signRanking
		},

		//获取用户签到时长
		'/studio/sign/duration': async ctx => {
			const {userId} = await ctx.auth.allow('studio')
			return await ctx.hd.studio.getUserSignDuration(userId)
		},

		//获取用户信息
		'/studio/user/info': async ctx => {
			const {userId, session} = await ctx.auth.allow('studio')
			let userData = await ctx.hd.studio.getUserDataCache(userId)
			if(!userData) {
				const {accessToken} = await ctx.hd.studio.checkAccessToken(session)
				userData = await ctx.hd.studio.getUserData(userId, accessToken)
			}
			return userData
		},

		//获取创新盒子管理数据
		'/studio/manager/box/info': async ctx => {
			const user = await ctx.auth.allow('studio')
			await ctx.hd.studio.allow(user, 0, 1)
			const commonData =  await ctx.hd.sys.getCommonData('notices', 'banners', 'courseYear', 'courseTerm', 'courseStartDate', 'courseEndDate', 'courseTime', 'repairOpen', 'repairTypes', 'scoreOpen', 'certScoreOpen', 'courseYearList', 'courseTermList')
			const lostFoundInfo = await ctx.hd.lost.getLostDataInfo()
			const lostFoundList = await ctx.hd.lost.getAllLostList(0, 15)
			const feedbackList = await ctx.hd.sys.getFeedbackList()
			return {
				commonData,
				feedbackList,
				lostFoundInfo,
				lostFoundList
			}
		}

	},

	post: {

		//工作室人员登录
		'/studio/login': async ctx => {
			await ctx.auth.allow('nosign')
			const {userId, userPwd} = ctx.bodyData
			if(!ctx.hd.student.checkStuId(userId))
				return ctx.error('-3054', 'studio user id invalid')
			if(!userPwd)
				return ctx.error('-3055', 'studio user pwd invalid')
			return await ctx.hd.studio.login({userId, userPwd})
		},

		//工作室用户反馈
		'/studio/feedback': async ctx => {
			const {userId} = await ctx.auth.allow('studio')
			const {content, contact} = ctx.bodyData
			const feedbackId = await ctx.hd.studio.addFeedback(userId, content, contact)
			return feedbackId
		},

		//工作室人员签到
		'/studio/sign/in': async ctx => {
			const {userId, data} = await ctx.auth.allow('studio')
			await ctx.hd.studio.checkSignOpenStatus(0)
			let {deviceId} = ctx.bodyData
			if(!deviceId || !ctx.hd.studio.checkDeviceId(deviceId))
				return ctx.error('-3072', 'sign device id invalid')
			//设备ID转小写
			deviceId = deviceId.toLowerCase()
			return await ctx.hd.studio.signIn(userId, data.userName, deviceId)
		},

		//工作室人员签退
		'/studio/sign/out': async ctx => {
			const {userId} = await ctx.auth.allow('studio')
			await ctx.hd.studio.checkSignOpenStatus(1)
			let {signId, deviceId} = ctx.bodyData
			if(!deviceId || !ctx.hd.studio.checkDeviceId(deviceId))
				return ctx.error('-3072', 'sign device id invalid')
			//设备ID转小写
			deviceId = deviceId.toLowerCase()
			return await ctx.hd.studio.signOut(signId, userId, deviceId)
		},

		//工作室入室申请提交
		'/studio/apply': async ctx => {
			await ctx.auth.allow('nosign')
			const {userId, userName, userSex, userClass, userEmail, userWechat, userPhone, userMajor, userPhoto, userIntroduce} = ctx.bodyData
			console.log(userId, userName, userSex, userClass, userEmail, userWechat, userPhone, userMajor, userIntroduce, userPhoto)
			if(!ctx.hd.student.checkStuId(userId) || !/^\W{2,}$/.test(userName) || !/^\d{2}\W{2}\d班$/.test(userClass) || !/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z0-9]{2,6}$/.test(userEmail) || !userWechat || !/^1\d+$/.test(userPhone) || userPhoto.indexOf('https') == -1 || !userIntroduce || userIntroduce.length < 30 || userIntroduce.length > 200)
				return ctx.error('-3058', 'apply data invalid')
			return await ctx.hd.studio.submitApply({userId, userName, userSex, userClass, userEmail, userWechat, userPhone, userMajor, userPhoto, userIntroduce})
		}

	},

	put: {

		//获取最新微科创基本信息
		'/studio/common': async ctx => {
			await ctx.auth.allow('studio')
			return await ctx.hd.studio.getHomeData()
		},

		//获取最新签到状态
		'/studio/sign/status': async ctx => {
			const {userId} = await ctx.auth.allow('studio')
			return await ctx.hd.studio.checkSignStatus(userId)
		},

		//获取最新签到记录
		'/studio/sign/record': async ctx => {
			const {userId} = await ctx.auth.allow('studio')
			return await ctx.hd.studio.getSignRecord(userId)
		},

		//获取最新用户信息
		'/studio/user/info': async ctx => {
			const {userId, session} = await ctx.auth.allow('studio')
			const {accessToken} = await ctx.hd.studio.checkAccessToken(session)
			return await ctx.hd.studio.getUserData(userId, accessToken)
		},

		//通过创新盒子失物招领
		'/studio/manager/box/lost/:lostId/resolve': async ctx => {
			const user = await ctx.auth.allow('studio')
			await ctx.hd.studio.allow(user, 0)
			const {lostId} = ctx.params
			return await ctx.hd.lost.updateLostPassStatusFromAdmin(lostId, 1)
		},

		//通过创新盒子失物招领
		'/studio/manager/box/lost/:lostId/reject': async ctx => {
			const user = await ctx.auth.allow('studio')
			await ctx.hd.studio.allow(user, 0)
			const {lostId} = ctx.params
			return await ctx.hd.lost.updateLostPassStatusFromAdmin(lostId, 2)
		},

		//结束创新盒子失物招领
		'/studio/manager/box/lost/:lostId/finish': async ctx => {
			const user = await ctx.auth.allow('studio')
			await ctx.hd.studio.allow(user, 0)
			const {lostId} = ctx.params
			return await ctx.hd.lost.updateLostFinishStatusFromAdmin(lostId, 1)
		},

		//恢复创新盒子失物招领
		'/studio/manager/box/lost/:lostId/recover': async ctx => {
			const user = await ctx.auth.allow('studio')
			await ctx.hd.studio.allow(user, 0)
			const {lostId} = ctx.params
			return await ctx.hd.lost.updateLostFinishStatusFromAdmin(lostId, 0)
		},

		//设置创新盒子反馈信息已处理状态
		'/studio/manager/box/feedback/status': async ctx => {
			const user = await ctx.auth.allow('studio')
			
		}

	},

	delete: {

		//工作室人员注销登录状态
		'/studio/logout': async ctx => {
			const {userId, session} = await ctx.auth.allow('studio')
			return await ctx.hd.studio.logout(userId, session)
		},

		//删除创新盒子失物招领
		'/studio/manager/box/lost/:lostId': async ctx => {
			const user = await ctx.auth.allow('studio')
			await ctx.hd.studio.allow(user, 0)
			const {lostId} = ctx.params
			return await ctx.hd.lost.delLostDataFromAdmin(lostId, 1)
		}

	}

}
module.exports = {
	get: {

		// '/sys/handler': async ctx => {
		// 	await ctx.auth.allow('nosign')
		// 	if(ctx.queryData.yes == 1) {
		// 		await ctx.hd.sys.handler()
		// 		return 1
		// 	}
		// 	else
		// 		return 0
		// },
		
		'/sys/test/connect': async ctx => {
			return 'OK';
		},

		'/sys/apps/:appId/authInfo': async ctx => {
			await ctx.auth.allow('student', 'teacher', 'inside')
			const {appId} = ctx.params
			console.log(appId)
			if(!/^[a-z]\-[0-9]{4}$/.test(appId))
				return ctx.error('-2104', 'appId invalid')
			return await ctx.hd.sys.getAppInfo(appId, 'appName', 'authInfo')			
		},

		'/sys/courseTime': async ctx => {
			const {courseYear, courseTerm, courseTime, courseStartDate, courseEndDate} = await ctx.hd.sys.getCommonData('courseYear', 'courseTerm', 'courseTime', 'courseStartDate', 'courseEndDate')
			return {courseYear, courseTerm, courseTime, courseStartDate, courseEndDate}
		},

		'/sys/library/search/:mode/:content': async ctx => {
			const {mode, content} = ctx.params
			const {page} = ctx.queryData
			return await ctx.hd.library.searchBook(mode, content, page)
		},

		//查询书本详情
		'/sys/library/book/:bookId': async ctx => {
			const { bookId } = ctx.params
			let bookInfo = await ctx.hd.library.getBookCache(bookId)
			if(bookInfo)
				return bookInfo
			const bookData = await ctx.hd.library.getBookData(bookId, ctx.user)
			return bookData
		},

		//获取横幅广告
		'/sys/banner': async ctx => {
			return await ctx.hd.sys.getBanners()
		},

		//获取报修类型列表
		'/sys/repair/type': async ctx => {
			await ctx.auth.allow('student', 'teacher', 'inside')
			return await ctx.hd.repair.getRepairTypes()
		},

		//获取当前学期
		'/sys/courseTermInfo': async ctx => {
			await ctx.auth.allow('student', 'teacher', 'inside')
			return await ctx.hd.sys.getCourseTermInfo()
		},
		
		'/sys/error/:code': async ctx => {
			const {code} = ctx.params
			if(!/^\d{3}$/.test(code))
				return ctx.error('-1033', 'http code invalid')
			switch(parseInt(code)) {
				case 413:
					return ctx.error('-1032', 'data too large')
				break
				default:
					return ctx.error('-1031', 'server unknown error')
			}
		},

		'/sys/device': async ctx => {
			
		},

		'/sys/ossData': async ctx => {
			await ctx.auth.allow('nosign')
			return await ctx.hd.sys.getOSSData()
		}

	},
	post: {



	},
	put: {

		'/sys/notices/:id': async ctx => {
			const {id} = ctx.params
			return ctx.hd.sys.getNotices(id || '000000')
		},

		'/sys/grabCourse': async ctx => {
			// const {userId} = await ctx.auth.allow('inside')
			// let {majorId, startTime, endTime} = ctx.bodyData
			// if(isNaN(startTime) || isNaN(endTime))
			// 	return this.reject('-3025', 'sync grab course must be have start end end timestamp')
			// let result
			// if(!majorId) {
			// 	//无指定专业ID将使用当前登录的内部人员绑定的学生用户的专业ID
			// 	result = await ctx.hd.inside.getStuDataByInsideId(userId, 'stuId, stuPwd, majorId')
			// 	majorId = result.majorId
			// }
			// else {
			// 	//如果指定了专业ID，那么将查找属于这个专业ID并且绑定了学生用户的内部人员
			// 	result = await ctx.hd.inside.getInsideStuDataByMajorId(majorId, 'stuId, stuPwd')
			// }
			// const {stuId, stuPwd} = result
			// await ctx.hd.zf.casLogin(stuId, stuPwd, ctx.user)
			// await ctx.hd.zf.activeCasZfSession(0, ctx.user)
			// const grabCourseList = await ctx.hd.zf.getGrabCourse(ctx.user)
			// await ctx.hd.grab.updateGrabCourses(0, grabCourseList, majorId, startTime, endTime)
			// await ctx.hd.grab.createCourseGrabJob(majorId, startTime, endTime)
			// return {
			// 	grabCourseList
			// }
			return 1
		},

		'/sys/sportsGrabCourse': async ctx => {
			// const {userId} = await ctx.auth.allow('inside')
			// let {majorId, startTime, endTime} = ctx.bodyData
			// if(isNaN(startTime) || isNaN(endTime) || startTime > endTime)
			// 	return this.reject('-3025', 'sync grab course time invalid')
			// let result
			// if(!majorId) {
			// 	//无指定专业ID将使用当前登录的内部人员绑定的学生用户的专业ID
			// 	result = await ctx.hd.inside.getStuDataByInsideId(userId, 'stuId, stuPwd, majorId')
			// 	majorId = result.majorId
			// }
			// else {
			// 	//如果指定了专业ID，那么将查找属于这个专业ID并且绑定了学生用户的内部人员
			// 	result = await ctx.hd.inside.getInsideStuDataByMajorId(majorId, 'stuId, stuPwd')
			// }
			// const {stuId, stuPwd} = result
			// await ctx.hd.zf.casLogin(stuId, stuPwd, ctx.user)
			// await ctx.hd.zf.activeCasZfSession(0, ctx.user)
			// const sportGrabCourseList = await ctx.hd.zf.getSportsGrabCourse(ctx.user)
			// await ctx.hd.grab.updateGrabCourses(1, sportGrabCourseList, majorId, startTime, endTime)
			// await ctx.hd.grab.createSportsCourseGrabJob(majorId, startTime, endTime)
			// return {
			// 	sportGrabCourseList
			// }
			return 1
		}

	}

}
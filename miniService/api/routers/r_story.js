module.exports = {

    get: {

        //获取所有故事列表
        '/story/list/:start/:end': async ctx => {
            await ctx.auth.allow('normal', 'student', 'teacher', 'inside')
            const {start, end} = ctx.params
            let storyList = await ctx.hd.story.getStoryListCache(start, end)
            if(!storyList)
                storyList = await ctx.hd.story.getStoryList(start, end)
            return storyList || []
        },

        //获取故事内容
        '/story/:id/:start/:end': async ctx => {
            await ctx.auth.allow('normal', 'student', 'teacher', 'inside')
            const {id: storyId, start, end} = ctx.params
            let storyData = await ctx.hd.story.getStoryCache(storyId, start, end)
            if(!storyData)
                storyData = await ctx.hd.story.getStory(storyId, start, end)
            return storyData || {}
        }

    },

    post: {

        //新增一个故事
        '/story': async ctx => {
            const {userId: storyAuthor} = await ctx.auth.allow('normal', 'student', 'teacher', 'inside')
            const {storyTitle, storyDescription, storyCover, storyContent} = ctx.bodyData
            return await ctx.hd.story.addStory(storyTitle, storyDescription, storyCover, storyAuthor, storyContent)
        }

    },

    put: {

        //续写指定故事的跟随故事
        '/story': async ctx => {
            const {userId: storyAuthor} = await ctx.auth.allow('normal', 'student', 'teacher', 'inside')
            const {storyId, storyContent} = ctx.bodyData
            return await ctx.hd.story.followStory(storyId, storyAuthor, storyContent)
        }

    },

    delete: {

        //删除故事数据
        '/story/:id': async ctx => {
            const {userId: storyAuthor} = await ctx.auth.allow('normal', 'student', 'teacher', 'inside')
            const {id: storyId} = ctx.params
            return await ctx.hd.story.closeStory(storyId, storyAuthor)
        }

    }

}
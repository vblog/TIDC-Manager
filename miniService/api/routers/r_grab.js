module.exports = {
	get: {

		//获取抢课课程列表
		'/grab/course': async ctx => {
			const {type, userId} = await ctx.auth.allow('student', 'teacher', 'inside')
			let {majorId} = ctx.queryData
			if(!majorId)
				majorId = await ctx.hd.user.getMajorIdByUserId(type, userId)
			const grabCourseList = await ctx.hd.grab.getGrabCourseByMajorId(majorId)
			const choosedCourseList = await ctx.hd.grab.getChoosedGrabCourseByUserId(userId)
			return {
				grabCourseList,
				choosedCourseList
			}
		},

		//获取体育抢课课程列表
		'/grab/sportsCourse': async ctx => {
			const {type, userId} = await ctx.auth.allow('student', 'teacher', 'inside')
			let {majorId} = ctx.queryData
			if(!majorId)
				majorId = await ctx.hd.user.getMajorIdByUserId(type, userId)
			const sportsGrabCourseList = await ctx.hd.grab.getSportsGrabCourseByMajorId(majorId)
			const choosedSportsCourseList = await ctx.hd.grab.getChoosedSportsGrabCourseByUserId(userId)
			return {
				sportsGrabCourseList,
				choosedSportsCourseList
			}
		}

	},
	post: {

		//新建抢课课程预约
		'/grab/course/appt': async ctx => {
			const {type, userId} = await ctx.auth.allow('student', 'teacher', 'inside')
			const {courseIds} = ctx.bodyData
			let {majorId} = ctx.bodyData
			if(!majorId)
				majorId = await ctx.hd.user.getMajorIdByUserId(type, userId)
			return await ctx.hd.grab.addGrabCourseAppt(courseIds, type, userId, majorId)
		},

		//新建体育抢课课程预约
		'/grab/sportsCourse/appt': async ctx => {
			const {type, userId} = await ctx.auth.allow('student', 'teacher', 'inside')
			const {courseIds, majorId} = ctx.bodyData
			if(!majorId)
				majorId = await ctx.hd.user.getMajorIdByUserId(type, userId)
			return await ctx.hd.grab.addGrabSportsCourseAppt(courseIds, type, userId, majorId)
		}

	},
	delete: {

		//取消抢课课程预约
		'/grab/course/appt': async ctx => {
			const {type, userId} = await ctx.auth.allow('student', 'teacher', 'inside')
			const {courseIds} = ctx.bodyData
			await ctx.hd.grab.addGrabCourseAppt(courseIds, type, userId)
		},

		//取消体育抢课课程预约
		'/grab/sportsCourse/appt': async ctx => {
			const {userId} = await ctx.auth.allow('student', 'teacher', 'inside')
			const {courseIds} = ctx.bodyData
			await ctx.hd.grab.addGrabSportsCourseAppt(courseIds, type, userId)
		}

	}
}
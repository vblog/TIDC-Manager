module.exports = {
	get: {

		'/open/device/:id': async ctx => {
			const deviceId = ctx.params.id
			return await ctx.hd.open.getDeviceById(deviceId)
		},

		'/open/device/:id/ip': async ctx => {
			const deviceId = ctx.params.id
			return ctx.echo(await ctx.hd.open.getDeviceIPv4(deviceId))
		}

	},
	post: {

		'/open/device': async ctx => {
			const {deviceName, deviceNetworkInfo} = ctx.bodyData
			const deviceId = await ctx.hd.open.createDevice(deviceName, deviceNetworkInfo)
			return ctx.echo(deviceId)
		}

	},
	put: {

		'/open/device/:id': async ctx => {
			const deviceId = ctx.params.id
			const {deviceNetworkInfo} = ctx.bodyData
			return await ctx.hd.open.updateDevice(deviceId, deviceNetworkInfo)
		}

	},
	delete: {

		'/open/device/:id': async ctx => {
			const deviceId = ctx.params.id
			return await ctx.hd.open.deleteDeviceById(deviceId)
		}

	}
}
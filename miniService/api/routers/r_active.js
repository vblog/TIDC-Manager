module.exports = {

	get: {

		//获得活动数据
		'/active/:activeId': async ctx => {
			await ctx.auth.allow('nosign')
			const {activeId} = ctx.params
			return await ctx.hd.active.getActiveData(activeId)
		},

		//获得奖品数据
		'/active/:activeId/prize': async ctx => {
			await ctx.auth.allow('nosign')
			const {activeId} = ctx.params
			return await ctx.hd.active.getActivePrize(activeId)
		}

	},

	post: {

		//参与活动
		'/active/:activeId': async ctx => {
			await ctx.auth.allow('nosign')
			const {activeId, activeParams} = ctx.params
			return await ctx.hd.active.joinActive(ctx.user.userId, activeId, activeParams)
		},

		//抽奖
		'/active/:activeId/:code': async ctx => {
			await ctx.auth.allow('nosign')
			const {activeId, code} = ctx.params
			return await ctx.hd.active.lotteryDraw(activeId, code)
		}
		
	},

	put: {

	}

}
/**
 *
 * 用户路由件
 *
 * all/get/post/delete/put 是路由件目前所完全支持的方法
 * 匹配表达式直接作为方法对象的key，触发方法直接作为方法对象的value
 * 我们把每个表达式对应的方法称作为触发方法，这个方法只有两个传入参数
 * ctx 基于原koa2上扩展的功能对象，支持原koa2所有ctx方法
 * next 返回它能够跳过触发函数进入下一个匹配表达式的触发函数
 * ctx {
 * 	hd	处理件对象，通过它去调用对应处理件内方法
 * 	common 工具类
 * 	config 配置数据
 * 	fs 文件操作模块
 * 	uploader 上传文件处理模块
 * 	mysql 数据库连接池，通过它去调用对应连接的操作方法
 * 	redis Redis数据库连接池，通过它去调用对应连接的操作方法
 * 	cron 定时任务池，通过它操作对应定时任务
 * }
 * 
 * 注意事项：
 * 触发方法应该加上async关键字，否则框架可能无法正确同步的返回数据
 * 调用处理件方法时，如果处理件方法使用了async关键字请务必保证加上await关键字等到此方法完成返回
 *
 */

module.exports = {
	get: {

		//查看学生照骗
		'/user/teacher/student/:stuId/photo': async ctx => {
			const {userId} = await ctx.auth.allow('teacher')
			const {stuId, stuPwd, stuZfAuthId, stuClass, stuPhoto} = await ctx.hd.user.getStuDataByUserId(userId, 'stuId', 'stuPwd', 'stuZfAuthId', 'stuClass', 'stuPhoto')
			await ctx.hd.teacher.checkTchClass(tchId, stuClass)
			let stuUser = {}
			await ctx.hd.zf.casLogin(stuId, stuPwd, stuUser)
			await ctx.hd.zf.activeCasZfSession(0, stuZfAuthId, stuUser)
			await ctx.handels.zf.getStuZfPhoto(stuUser)
		},

		//获取教职工一卡通数据
		'/user/teacher/card': async ctx => {
			const {userId} = await ctx.auth.allow('teacher')
			let {cardPwd = '888888'} = ctx.bodyData
			let tchCardData = await ctx.hd.card.getTchCardCache(userId)
			if(tchCardData)
				return tchCardData
			const {tchId} = await ctx.hd.teacher.getTchDataByUserId(userId, 'tchId')
			const data = await ctx.hd.card.getCardDataByUserId(userId, 'cardPwd')
			if(data && data.cardPwd)
				cardPwd = data.cardPwd
			await ctx.hd.card.cardLogin(0, tchId, cardPwd, ctx.user)
			tchCardData = await ctx.hd.card.getTchCardDataFromOrigin(ctx.user)
			ctx.hd.card.updateTchCardDataByUserId(userId, cardPwd, tchCardData)
			return tchCardData
		},

		//查询教职工借阅记录
		'/user/teacher/library/loan': async ctx => {
			const {userId} = await ctx.auth.allow('teacher')
			let loanList = await ctx.hd.library.getLoanCache(userId)
			if(loanList)
				return loanList
			const {tchId, tchPwd} = await ctx.hd.teacher.getTchDataByUserId(userId, 'tchId', 'tchPwd')
			await ctx.hd.library.libraryLogin(tchId, tchPwd, ctx.user)
			loanList = await ctx.hd.library.getLoan(ctx.user)
			return loanList
		},

		//获取管理的班级列表
		'/user/teacher/classes': async ctx => {
			const {userId} = await ctx.auth.allow('teacher')
			const {tchId} = await ctx.hd.teacher.getTchDataByUserId(userId, 'tchId')
			let classes = await ctx.hd.teacher.getTchClassesCache(tchId)
			if(!classes)
				classes = await ctx.hd.teacher.getTchClasses(tchId)
			return classes
		},

		//获取管理的班级的学生列表
		'/user/teacher/class/:classId/students': async ctx => {
			const {userId} = await ctx.auth.allow('teacher')
			const {classId} = ctx.params
			const {tchId} = await ctx.hd.teacher.getTchDataByUserId(userId, 'tchId')
			await ctx.hd.teacher.checkTchClass(tchId, classId)
			return await ctx.hd.student.getClassStudentList(classId, 'stuId', 'stuSex', 'stuName', 'stuSuspend')
		},

		//获取教师当天上课课程列表
		'/user/teacher/course/today': async ctx => {
			const {userId} = await ctx.auth.allow('teacher')
			const {tchId} = await ctx.hd.teacher.getTchDataByUserId(userId, 'tchId')
			return await ctx.hd.teacher.getTchTodayCourse(tchId)
		}

	},
	post: {

		//绑定教师用户
		'/user/teacher': async ctx => {
			await ctx.auth.allow('normal')
			const {tchId, tchPwd} = ctx.bodyData
			if(await ctx.hd.teacher.checkTchBind(tchId))
				return ctx.error('-2003', 'teacher bind id only one time')
			const tchHashPwd = ctx.hd.teacher.encryptTchPwd(tchId, tchPwd)
			await ctx.hd.zf.casLogin(tchId, tchPwd, ctx.user, tchHashPwd)
			await ctx.hd.zf.updateCasPwd(tchHashPwd, ctx.user)
			await ctx.hd.zf.activeCasZfSession(1, '', ctx.user)
			let tchInfo = await ctx.hd.zf.getUserInfo(ctx.user)
			const {courseYear, courseTerm} = await ctx.hd.sys.getCommonData('courseYear', 'courseTerm')
			const {courseData, courseClasses} = await ctx.hd.syllabus.getTchSyllabusDataFromOrigin(courseYear, courseTerm, ctx.user)
			tchInfo.tchSyllabus = courseData
			tchInfo.courseClasses = courseClasses
			tchInfo.courseTime = {courseYear, courseTerm}
			return await ctx.hd.teacher.bindTchUser(tchId, tchPwd, tchInfo, ctx.user)
		},

		//提交点名单
		'/user/teacher/callName': async ctx => {
			const {userId} = await ctx.auth.allow('teacher')
			const {classId, callNameResult} = ctx.bodyData
			if(!ctx.hd.department.checkClassId(classId))
				return ctx.error('-2071', 'class id invalid')
			const {ignore, sign, late, absent, leave, earlyLeave, suspend} = callNameResult
			if(!ignore instanceof Array || !sign instanceof Array || !late instanceof Array || !absent instanceof Array || !leave instanceof Array || !earlyLeave instanceof Array || !suspend instanceof Array)
				return ctx.error('-2086', 'call name result data invalid')
			const {tchId} = await ctx.hd.teacher.getTchDataByUserId(userId, 'tchId')
			const callId = await ctx.hd.teacher.addCallNameTable(tchId, classId, callNameResult)
			return callId
		}

	},
	put: {

		//同步最新教职工一卡通数据
		'/user/teacher/card': async ctx => {
			const {userId} = await ctx.auth.allow('teacher')
			let {cardPwd = '888888'} = ctx.bodyData
			const {tchId} = await ctx.hd.teacher.getTchDataByUserId(userId, 'tchId')
			const data = await ctx.hd.card.getCardDataByUserId(userId, 'cardPwd')
			if(data && data.cardPwd)
				cardPwd = data.cardPwd
			await ctx.hd.card.cardLogin(0, tchId, cardPwd, ctx.user)
			tchCardData = await ctx.hd.card.getTchCardDataFromOrigin(ctx.user)
			ctx.hd.card.updateTchCardDataByUserId(userId, cardPwd, tchCardData)
			return tchCardData
		},

		//同步最新借阅记录
		'/user/teacher/library/loan': async ctx => {
			const {userId} = await ctx.auth.allow('teacher')
			const {tchId, tchPwd} = await ctx.hd.teacher.getTchDataByUserId(userId, 'tchId', 'tchPwd')
			await ctx.hd.library.libraryLogin(tchId, tchPwd, ctx.user)
			loanList = await ctx.hd.library.getLoan(ctx.user)
			return loanList
		},

		//续借书本
		'/user/teacher/library/reloan/:bookNum': async ctx => {
			const {userId} = await ctx.auth.allow('teacher')
			const {bookNum} = ctx.params
			const {tchId, tchPwd} = await ctx.hd.teacher.getTchDataByUserId(userId, 'tchId', 'tchPwd')
			await ctx.hd.library.libraryLogin(tchId, tchPwd, ctx.user)
			return await ctx.hd.library.reloanBook(bookNum, ctx.user)
		},

		//同步最新任教班级列表和学生列表
		'/user/teacher/classes': async ctx => {
			const {userId} = await ctx.auth.allow('teacher')
			const {tchId, tchPwd, tchZfAuthId} = await ctx.hd.teacher.getTchDataByUserId(userId, 'tchId', 'tchPwd', 'tchZfAuthId')
			await ctx.hd.zf.casLogin(tchId, tchPwd, ctx.user)
			await ctx.hd.zf.activeCasZfSession(1, tchZfAuthId, ctx.user)
			const {courseYear, courseTerm} = await ctx.hd.sys.getCommonData('courseYear', 'courseTerm')
			const {courseData, courseClasses} = await ctx.hd.syllabus.getTchSyllabusDataFromOrigin(courseYear, courseTerm, ctx.user)
			await ctx.hd.syllabus.addTchCourseData(tchId, courseClasses, courseData, {courseYear, courseTerm})
			return await ctx.hd.teacher.getTchClasses(tchId)
		},

	},
	delete: {

		//解绑教职工用户
		'/user/teacher': async ctx => {
			await ctx.auth.allow('teacher')
			await ctx.hd.teacher.untieTchUser(ctx.user)
			return 1
		}

	}
}
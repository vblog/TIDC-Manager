//获得项目根目录
const basePath = process.cwd().replace(/\\/g, '/')
module.exports = {

	/*

	 框架配置
	 框架运行时的参数配置

	 [string] version 框架版本号
	 [string] uriPrefix 路由URI前缀，默认/api，尾部无需再加斜杠
	 [array] routers 路由件列表数组，r_xxx放于routers文件夹
	 [array] handles 处理件列表数组，h_xxx放于handles文件夹
	 [object] exports 开放的接口列表
	 [object] notSign 无需签名处理的接口
	 [string] logFile 日志文件地址
	 [boolean] apiStrict 接口是否开启严格模式，严格模式一个API周期内只能调用一次
	 [object] apiKeys API密钥
	 [object] apiExpires 指定所有API调用有效期（秒）
	 [object] sessionExpires 指定不同配置的session生命周期（秒）
	 [string] pwdSalt 加密密码用的盐字符串
	 [string] alertUrl 服务器警报接收URL

	*/

	frame: {
		version: '1.1.0',
		uriPrefix: '',
		routers: ['user', 'student', 'teacher', 'studio', 'inside', 'grab', 'sys', 'story', 'active', 'open'],
		handles: ['user', 'zf', 'card', 'syllabus', 'score', 'evaluation', 'repair', 'grab', 'sys', 'story', 'active', 'library', 'student', 'teacher', 'studio', 'inside', 'department', 'open', 'lost'],
		logFile: `${process.cwd()}/logs/production.log`,
		apiStrict: true,
		jpegCompressibility: 50,
		apiKeys: {
			'cxxy': 'peiYlWBpBoNGT2I3ZK6rOBBBjgyOva62'
		},
		apiExpires: {
			all: 3600
		},
		sessionExpires: {
			all: 86400,
			student: 86400,
			teacher: 86400,
			evaluation: 86400,
			classStudentList: 604800,
			tchClasses: 604800,
			repair: 28800,
			library: 7200,
			syllabus: 86400,
			score: 604800,
			card: 1800,
			cas: 7200,
			zf: 7200,
			lostList: 7200,
			lostPublish: 7200,
			lostFinish: 7200,
			studioUser: 604800,
			studioCommonData: 86400,
			studioSignRecord: 7200
		},
		msgType: [
			'correlSyllabus',
			'resolveCorrelSyllabus',
			'rejectCorrelSyllabus',
			'untieCorrelSyllabus'
		],
		pwdSalt: 'RbRn1FtKlduyvPyPk7JBN765ipZV1OiY',
		dataKey: 'Egnbb0B3vR69mHI7s5zxESner4AZBva3/OmcpQLP540aDVNMV2USm+2S71aq6Z8DX0Eo0rgHHMpDe042qylhk4zo66h/lq9BS2wUVlreNp2/gUp3xqV284gydzreGxSjuNErl0ZxlJUctyzNeDi+itnPwVJQ1L2q0TllkCm95nY=',
		dataIv: 'v0ySU4tm+gZRLLuVUAefTG3y4tkhJ66V+Hnzp3vcHmaVMkqP6fCapUmmdr5J1tnlXlz/On0XHcfEJob5WtY7KwfaDqgmr0mS02H4J1ImEgTPg+iKwAchho6Cr5J4TYxiME0d/mkQ4eDjPWu61KngrJ65cfV538pX7L2Hv+DsfNo=',
		alertUrl: 'https://sc.ftqq.com/SCU12695Tb9f582edc16d3b52cc7657b34d522fa359c9e1a74b452.send',
		fileFilter: {
			names: ['image'],
			mimes: ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'],
			maxSize: 10485760
		}
	},
	
	/**
	 *
	 * 服务器配置
	 * 建议使用nginx进行ssl代理、反向代理、负载均衡
	 *
	 * [number] port 服务器默认监听的端口，通过pm2管理器可以指定端口启动pm2 start index.js --name "服务名" -- p 端口号
	 * [string] staticPath 服务器静态资源所在路径
	 * [string] tmpPath 服务器上传临时文件所在路径
	 *
	 */

	server: {
		webUrl: 'https://www.cx-tidc.com/',
		apiUrl: 'https://pub.cx-tidc.com/',
		staticUrl: 'https://pub.cx-tidc.com/',
		port: 3000,
		basePath,
		staticPath: '../../publics/cxxy',
		tmpPath: '../../publics/cxxy/tmp',
		zfUserHeadPath: '../../publics/cxxy/zf/head'
	},
	
	/**
	 *
	 * Redis数据库配置
	 * 数据库配置中默认绑定到127.0.0.1，可以不采取认证
	 *
	 * [array] dbs 需要连接的数据库名称与index映射数组，dbs[0]代表redis的0号数据库别称为default
	 * [string] host 数据库地址，默认127.0.0.1
	 * [number] port 数据库端口，默认6379
	 * [string] pwd 认证的密码，如果不为空字符串将开启认证模式
	 *
	 */

	redis: {
		session: {
			host: '127.0.0.1',
			port: 7878,
			pwd: 'cxxyRedis456289788',
			db: 0
		},
		system: {
			host: '127.0.0.1',
			port: 7878,
			pwd: 'cxxyRedis456289788',
			db: 1
		},
		grab: {
			host: '127.0.0.1',
			port: 7878,
			pwd: 'cxxyRedis456289788',
			db: 2
			// nodes: [
			// 	{
			// 		host: '127.0.0.1',
			// 		port: 7001
			// 	},
			// 	{
			// 		host: '127.0.0.1',
			// 		port: 7002
			// 	},
			// 	{
			// 		host: '127.0.0.1',
			// 		port: 7003
			// 	},
			// 	{
			// 		host: '127.0.0.1',
			// 		port: 7004
			// 	},
			// 	{
			// 		host: '127.0.0.1',
			// 		port: 7005
			// 	},
			// 	{
			// 		host: '127.0.0.1',
			// 		port: 7006
			// 	}
			// ],
			// pwd: 'cwb456289788',
		},
		user: {
			host: '127.0.0.1',
			port: 7878,
			pwd: 'cxxyRedis456289788',
			db: 3
		},
		active: {
			host: '127.0.0.1',
			port: 7878,
			pwd: 'cxxyRedis456289788',
			db: 4
		},
		cache: {
			host: '127.0.0.1',
			port: 7878,
			pwd: 'cxxyRedis456289788',
			db: 5
		}
	},

	/*
	 
	 MySQL数据库配置
	 数据库配置中默认绑定到127.0.0.1
	
	 [string] dbPrefix 数据库名前缀
	 [array] dbs 需要连接的数据库名称数组
	 [string] host 数据库地址，默认127.0.0.1
	 [number] port 数据库端口，默认3306
	 [string] user 数据库用户，勿用root用户
	 [string] pwd 数据库密码
	 [string] charset 数据库默认编码
	
	*/

	mysql: {
		dbPrefix: '',
		dbs: ['cxxy', 'cxxyMsg', 'cxxyStudio', 'cxxyOpen'],
	  	host: '127.0.0.1',
	  	port: 3306,
		user: 'cxxy',
		pwd: 'cxxyMySQL456289788',
		charset: 'utf8mb4_bin',
		connExpires: 3,
		encryptTableFields: {
			cxxy: {
				active: [
					'activeName',
					'activeData'
				],
				call_name: [
					'callData'
				],
				card: [
					'cardPwd',
					'cardBalance',
					'cardTranBalance',
					'cardTradeList',
					'cardExpend'
				],
				class: [
					'className'
				],
				classroom: [
					'roomName'
				],
				common: [
					'infoValue',
					'infoDescription'
				],
				course: [
					'courseName'
				],
				faculty: [
					'facultyName'
				],
				feedback: [
					'feedbackContent',
					'feedbackContact'
				],
				grab_course: [
					'courseTid',
					'courseName',
					'courseTch',
					'courseData'
				],
				inside: [
					'insideName',
					'insideNickName'
				],
				library: [
					'readerPwd'
				],
				major: [
					'majorName',
					'majorShortName'
				],
				repair: [
					'repairContent',
					'repairAddress',
					'repairContact',
					'repairEvaluateContent'
				],
				repair_department: [
					'departmentName'
				],
				score: [
					'scoreList',
					'certScoreList'
				],
				staff_room: [
					'roomName'
				],
				student: [
					'stuPwd',
					'stuZfAuthId',
					'stuName',
					'stuBirth',
					'stuPhoto',
					'stuPhone',
					'stuCardId',
					'stuIdCard',
					'stuExamId',
					'stuEdu',
					'stuNation',
					'stuPolicital'
				],
				student_syllabus: [
					'syllabusData'
				],
				teacher: [
					'tchPwd',
					'tchZfAuthId',
					'tchName',
					'tchPhoto',
					'tchPhone',
					'tchBirth',
					'tchNation',
					'tchPolicital',
					'tchEmail',
					'tchUniversity',
					'tchMajor',
					'tchEdu',
					'tchDegree',
					'tchTitle',
					'tchJob',
					'tchSubject',
					'tchCertNum',
					'tchLectureNum',
					'tchHumanId',
					'tchEvaluation',
					'tchSummary'
				],
				teacher_syllabus: [
					'syllabusData'
				],
				user: [
					'userName',
					'userHead'
				],
				lost_found: [
					'lostTitle',
					'lostContent',
					'lostContact',
					'lostAddress'
				],
				app_info: [
					'appName',
					'authInfo',
					'authNeed'
				]
			},
			cxxyStudio: {
				common: [
					'infoValue',
					'infoDescription'
				],
				feedback: [
					'feedbackContent',
					'feedbackContact'
				],
				sign_device: [
					'deviceDescription'
				],
				sign_in_record: [
					'signUserName'
				]
			}
		}
	},

	/*

	 RabbitMQ消息服务器配置
	 消息服务器中默认绑定到127.0.0.1

	 [string] host 服务器地址，默认127.0.0.1
	 [number] port 服务器端口，默认4369
	 [string] user 服务器用户
	 [string] pwd 服务器密码

	*/

	amqp: {
		message: {
			host: 'localhost',
			port: 5672,
			vhost: 'cxxy',
			user: 'cxxy',
			pwd: 'cxxyVhost456289788'
		}
	},
	
	cxxy: {
		teacherDefaultPwd: 'cxxy123456'
	},

	/*
	
	 正方教务管理系统配置
	
	 [string] url 系统的站点地址
	 [object] viewState _VIEWSTATE字段对象
	
	*/

	zf: {
		url: 'http://jwc.gdcxxy.net/',
		captchaRecognitionUrl: 'http://localhost:4231',
		viewState: {
			login: 'dDw3OTkxMjIwNTU7Oz7x4N9Rx4HuooqW5X5py3jGiJK0yg==',
			stuSyllabus: 'dDwtODAxODI2NDQzO3Q8O2w8aTwwPjtpPDE+O2k8Mj47aTwzPjtpPDQ+O2k8NT47PjtsPHQ8O2w8aTwxPjtpPDM+O2k8NT47aTw5Pjs+O2w8dDw7bDxpPDA+Oz47bDx0PDtsPGk8MD47aTwxPjtpPDM+O2k8Nj47PjtsPHQ8cDxwPGw8VGV4dDs+O2w8XGU7Pj47Pjs7Pjt0PHQ8cDxwPGw8RGF0YVRleHRGaWVsZDtEYXRhVmFsdWVGaWVsZDs+O2w8eG47eG47Pj47Pjt0PGk8ND47QDwyMDE4LTIwMTk7MjAxNy0yMDE4OzIwMTYtMjAxNztcZTs+O0A8MjAxOC0yMDE5OzIwMTctMjAxODsyMDE2LTIwMTc7XGU7Pj47bDxpPDA+Oz4+Ozs+O3Q8dDw7O2w8aTwxPjs+Pjs7Pjt0PDtsPGk8MD47PjtsPHQ8dDxwPHA8bDxWaXNpYmxlOz47bDxvPGY+Oz4+Oz47O2w8aTwwPjs+Pjs7Pjs+Pjs+Pjs+Pjt0PDtsPGk8MD47PjtsPHQ8O2w8aTwxPjtpPDM+O2k8NT47aTw3PjtpPDk+Oz47bDx0PHQ8cDxwPGw8RGF0YVRleHRGaWVsZDtEYXRhVmFsdWVGaWVsZDs+O2w8bmo7bmo7Pj47Pjt0PGk8MTQ+O0A8MjAxNzsyMDE2OzIwMTU7MjAxNDsyMDEzOzIwMTI7MjAxMTsyMDA5OzIwMDg7MjAwNzsyMDA2OzIwMDU7MjAwNDtcZTs+O0A8MjAxNzsyMDE2OzIwMTU7MjAxNDsyMDEzOzIwMTI7MjAxMTsyMDA5OzIwMDg7MjAwNzsyMDA2OzIwMDU7MjAwNDtcZTs+PjtsPGk8MD47Pj47Oz47dDx0PHA8cDxsPERhdGFUZXh0RmllbGQ7RGF0YVZhbHVlRmllbGQ7PjtsPHh5bWM7eHlkbTs+Pjs+O3Q8aTw5PjtAPOS/oeaBr+W3peeoi+WtpumZojvnrqHnkIbns7s75aSW6K+t57O7O+i0oue7j+WtpumZojvnu6fnu63mlZnogrLlrabpmaI76Im65pyv6K6+6K6h57O7O+W7uuetkeW3peeoi+ezuzvmnLrnlLXlt6XnqIvns7s7XGU7PjtAPDAxOzAzOzA0OzA1OzA3OzA4OzA5OzEwO1xlOz4+O2w8aTwwPjs+Pjs7Pjt0PHQ8cDxwPGw8RGF0YVRleHRGaWVsZDtEYXRhVmFsdWVGaWVsZDs+O2w8enltYzt6eWRtOz4+Oz47dDxpPDIyPjtAPOeJqeiBlOe9keW6lOeUqOaKgOacrzvliqjmvKvliLbkvZzmioDmnK8o5riv5r6z5Y+w5ZCI5L2cKTvorqHnrpfmnLrlupTnlKjmioDmnK876K6h566X5py65bqU55So5oqA5pyvKOWKqOa8q+S4juaVsOWtl+WqkuS9k+aKgOacryk76K6h566X5py65bqU55So5oqA5pyvKOe9keermeW7uuiuvuS4jui/kOiQpSk76YCa5L+h5oqA5pyvO+mAmuS/oeaKgOacryjpgJrkv6Hlt6XnqIvkuI7nm5HnkIYpO+mAmuS/oeaKgOacrygzR+enu+WKqOmAmuS/oSk76YCa5L+h5oqA5pyvKOenu+WKqOS6kuiBlOe9keaKgOacryk75ZG85Y+r5Lit5b+D5pyN5Yqh5LiO566h55CGO+WKqOa8q+WItuS9nOaKgOacrzvorqHnrpfmnLrlupTnlKjmioDmnK8o5aSa5aqS5L2T5Yi25L2cKTvorqHnrpfmnLrlupTnlKjmioDmnK8o6YCa5L+h5oqA5pyvKTvorqHnrpfmnLrlupTnlKjmioDmnK8o5pWw5o2u5bqT5byA5Y+R5LiO566h55CGKTvorqHnrpfmnLrlupTnlKjmioDmnK8o572R57uc5bel56iL5LiO566h55CGKTvnlLXlrZDkv6Hmga/lt6XnqIvmioDmnK8o5riv5r6z5Y+w5ZCI5L2cKTvnlLXlrZDkv6Hmga/lt6XnqIvmioDmnK875Yqo5ryr6K6+6K6h5LiO5Yi25L2cO+eUteS/oeacjeWKoeS4jueuoeeQhjvorqHnrpfmnLrnvZHnu5zmioDmnK876L2v5Lu25oqA5pyvO1xlOz47QDwwMTIyOzAxMjE7MDEwNTswMTA2OzAxMDc7MDEwODswMTA5OzAxMTA7MDExMTswMTEyOzAxMjA7MDEwMTswMTAyOzAxMDM7MDEwNDswMTE4OzAxMTM7MDExNDswMTE5OzAxMTY7MDExNztcZTs+PjtsPGk8MD47Pj47Oz47dDx0PHA8cDxsPERhdGFUZXh0RmllbGQ7RGF0YVZhbHVlRmllbGQ7PjtsPGJqbWM7YmpkbTs+Pjs+O3Q8aTwxPjtAPFxlOz47QDxcZTs+PjtsPGk8MD47Pj47Oz47dDx0PHA8cDxsPERhdGFUZXh0RmllbGQ7RGF0YVZhbHVlRmllbGQ7PjtsPHhtO3hoOz4+Oz47dDxpPDE+O0A8XGU7PjtAPFxlOz4+Oz47Oz47Pj47Pj47dDw7bDxpPDA+Oz47bDx0PDtsPGk8MD47aTwyPjtpPDQ+O2k8Nj47aTw4PjtpPDExPjs+O2w8dDxwPHA8bDxUZXh0Oz47bDzlrablj7fvvJoxNjAxMTcwMTMwOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzlp5PlkI3vvJrpmYjngpzlrr47Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOWtpumZou+8muS/oeaBr+W3peeoi+WtpumZojs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85LiT5Lia77ya6L2v5Lu25oqA5pyvOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzooYzmlL/nj63vvJoxNui9r+S7tjHnj607Pj47Pjs7Pjt0PHQ8OztsPGk8MD47Pj47Oz47Pj47Pj47dDxAMDxwPHA8bDxQYWdlQ291bnQ7XyFJdGVtQ291bnQ7XyFEYXRhU291cmNlSXRlbUNvdW50O0RhdGFLZXlzOz47bDxpPDE+O2k8MD47aTwwPjtsPD47Pj47Pjs7Ozs7Ozs7Ozs+Ozs+Oz4+O3Q8QDA8cDxwPGw8UGFnZUNvdW50O18hSXRlbUNvdW50O18hRGF0YVNvdXJjZUl0ZW1Db3VudDtEYXRhS2V5czs+O2w8aTwxPjtpPDA+O2k8MD47bDw+Oz4+Oz47Ozs7Ozs7Ozs7Pjs7Pjt0PHA8bDxWaXNpYmxlOz47bDxvPGY+Oz4+Ozs+O3Q8cDxsPFZpc2libGU7PjtsPG88Zj47Pj47bDxpPDA+Oz47bDx0PDtsPGk8MD47PjtsPHQ8QDA8cDxwPGw8UGFnZUNvdW50O18hSXRlbUNvdW50O18hRGF0YVNvdXJjZUl0ZW1Db3VudDtEYXRhS2V5czs+O2w8aTwxPjtpPDA+O2k8MD47bDw+Oz4+Oz47Ozs7Ozs7Ozs7Pjs7Pjs+Pjs+Pjt0PEAwPHA8cDxsPFBhZ2VDb3VudDtfIUl0ZW1Db3VudDtfIURhdGFTb3VyY2VJdGVtQ291bnQ7RGF0YUtleXM7PjtsPGk8MT47aTwwPjtpPDA+O2w8Pjs+Pjs+Ozs7Ozs7Ozs7Oz47Oz47dDxAMDxwPHA8bDxQYWdlQ291bnQ7XyFJdGVtQ291bnQ7XyFEYXRhU291cmNlSXRlbUNvdW50O0RhdGFLZXlzOz47bDxpPDE+O2k8MD47aTwwPjtsPD47Pj47Pjs7Ozs7Ozs7Ozs+Ozs+Oz4+Oz6w01SWt+hMv8cfjPTMmCRdrwEKBQ==',
			tchSyllabus: 'dDwtNzc0ODE2Mjk1O3Q8O2w8aTwwPjs+O2w8dDw7bDxpPDE+O2k8Mz47aTw1PjtpPDc+O2k8MTA+O2k8MTU+O2k8MjA+O2k8MzA+O2k8MzI+O2k8MzQ+O2k8MzY+Oz47bDx0PHQ8cDxwPGw8RGF0YVRleHRGaWVsZDtEYXRhVmFsdWVGaWVsZDs+O2w8eG47eG47Pj47Pjt0PGk8MTE+O0A8MjAxOC0yMDE5OzIwMTctMjAxODsyMDE2LTIwMTc7MjAxNS0yMDE2OzIwMTQtMjAxNTsyMDEzLTIwMTQ7MjAxMi0yMDEzOzIwMTEtMjAxMjsyMDEwLTIwMTE7XGU7XGU7PjtAPDIwMTgtMjAxOTsyMDE3LTIwMTg7MjAxNi0yMDE3OzIwMTUtMjAxNjsyMDE0LTIwMTU7MjAxMy0yMDE0OzIwMTItMjAxMzsyMDExLTIwMTI7MjAxMC0yMDExO1xlO1xlOz4+O2w8aTwxPjs+Pjs7Pjt0PHQ8cDxwPGw8RGF0YVRleHRGaWVsZDtEYXRhVmFsdWVGaWVsZDs+O2w8eHE7eHE7Pj47Pjt0PGk8ND47QDwyOzE7XGU7XGU7PjtAPDI7MTtcZTtcZTs+PjtsPGk8MD47Pj47Oz47dDx0PHA8cDxsPFZpc2libGU7PjtsPG88Zj47Pj47Pjs7Pjs7Pjt0PHQ8cDxwPGw8VmlzaWJsZTs+O2w8bzxmPjs+Pjs+OztsPGk8MD47Pj47Oz47dDx0PHA8cDxsPERhdGFUZXh0RmllbGQ7RGF0YVZhbHVlRmllbGQ7PjtsPGJtO2JtOz4+Oz47dDxpPDMxPjtAPOS/oeaBr+W3peeoi+WtpumZojvorr7orqHkuI7liLbpgKDns7s7566h55CG57O7O+WkluivreezuzvotKLnu4/lrabpmaI75oCd5oOz5pS/5rK755CG6K666K++5pWZ5a2m6YOoO+e7p+e7reaVmeiCsuWtpumZojvoibrmnK/orr7orqHns7s75bu6562R5bel56iL57O7O+acuueUteW3peeoi+ezuzvlrabmoKHpooblr7w75pWZ5Yqh5aSEO+WtpueUn+WkhDvotKjph4/nrqHnkIblip7lhazlrqQ75oub55Sf5bCx5Lia5YqeO+aAu+WKoeWkhDvlrabmoKHlip7lhazlrqQ75Zu+5Lmm6aaGO+aVmeiCsuaKgOacr+S4reW/gzvkurrkuovlpIQ75Zu96ZmF5Lqk5rWB6YOoO+WFmuWnlOWKnuWFrOWupDvotKLliqHlrqQ75L+d5Y2r5aSEO+WunuiureS4reW/gzvnp5HmioDlpIQ76K6+5aSH6LWE5Lqn6YOoO+WIm+S4muWtpumZojvoibrmnK/mlZnogrLkuK3lv4M7XGU7XGU7PjtAPOS/oeaBr+W3peeoi+WtpumZojvorr7orqHkuI7liLbpgKDns7s7566h55CG57O7O+WkluivreezuzvotKLnu4/lrabpmaI75oCd5oOz5pS/5rK755CG6K666K++5pWZ5a2m6YOoO+e7p+e7reaVmeiCsuWtpumZojvoibrmnK/orr7orqHns7s75bu6562R5bel56iL57O7O+acuueUteW3peeoi+ezuzvlrabmoKHpooblr7w75pWZ5Yqh5aSEO+WtpueUn+WkhDvotKjph4/nrqHnkIblip7lhazlrqQ75oub55Sf5bCx5Lia5YqeO+aAu+WKoeWkhDvlrabmoKHlip7lhazlrqQ75Zu+5Lmm6aaGO+aVmeiCsuaKgOacr+S4reW/gzvkurrkuovlpIQ75Zu96ZmF5Lqk5rWB6YOoO+WFmuWnlOWKnuWFrOWupDvotKLliqHlrqQ75L+d5Y2r5aSEO+WunuiureS4reW/gzvnp5HmioDlpIQ76K6+5aSH6LWE5Lqn6YOoO+WIm+S4muWtpumZojvoibrmnK/mlZnogrLkuK3lv4M7XGU7XGU7Pj47bDxpPDA+Oz4+Ozs+O3Q8dDxwPHA8bDxEYXRhVGV4dEZpZWxkO0RhdGFWYWx1ZUZpZWxkOz47bDxKU3htO2pzemdoOz4+Oz47dDxpPDkyPjtAPOiUoee/oOe/lDvolKHpuY876L2m6am+6ZuEO+mZiOW9qeaciDvpmYjovpHmupA76ZmI6I2j5a6dO+mZiOaZk+S4uTvpmYjpm6rmlY876YKT5bqG5rW3O+mCk+S4gOi+iTvmlrnlrrbovok75Yav5a6d56WlO+WGr+awuOWNjjvlhbPnu6flpKs76YOt6ZuFO+mDnemUi+mSojvkvZXnhYw75L2V6ZO25bedO+S9leW9sDvog6HpnZM76buE5Lyf5YibO+mbt+WwkeeOsjvmnY7otoU75p2O5bOwO+adjumUizvmnY7mnb7mtps75p2O5p2P5riFO+adjuazl+WFsDvmooHmtbfkvKY75buW5oWO5YukO+WImOWAjembhDvliJjpl6875YiY5Li95LuqO+WImOiCg+W5szvliJjkvJ/mt7E75YiY5p2P5Y2XO+WNouaZr+WzsDvljaLojaM76aqG6YeR57u0O+mprOiOieiOiTvogYLlhps76IGC5b2x5b2xO+asp+eBv+iNozvlup7lj4zpvpk75LiY5paH5bOwO+mCsemqj+mpuTvpgrHmnpfmtqY75LiK5a6Y5YWJ6IqSO+mCtee/oDvpgrXluIU75a2Z5rC45p6XO+WtmeWuhzvosK3lv5flubM75rGk5oCAO+WUkOWLhzvllJDnjonoirM76Zm25bOwO+eUsOeri+S8nzvnlLDoi5fpnZI7546L5Zu95by6O+eOi+WLhzvlt6vmgJ3mlY875ZC05qSN5qCLO+iClue+juWptzvogpblt6fnjrI76LCi5ZOB56ugO+iwoue7tOePoDvosKLljZM76K645Y2T5by6O+adqOWFtumSpjvmnajlkK/oirM75p2o5rO96L6JO+adqOaYijvlj7bmhafoirM75Y+26JOd5qKmO+aYk+W4hTvkvZnkuq476KKB5qKT5bOwO+abvuW+t+eUnzvmm77mmK3msZ876Km55a6d5a65O+W8oOmHkeaXujvlvKDlirLms6I75byg5piO6L6JO+W8oOS7geW/oDvlvKDpopbkv4o76LW15bOwO+WRqOaYpeWtpjvlkajmgKE75pyx5LquO+WNk+iNo+a1tzvlrpfnjrI7PjtAPDA0MTc7dzIyMTsyNjUxOzExNDE7MDMyMzswODE4OzAwMTA7MDI3NDswOTE4OzA0MTg7dzE5Nzt3MjA5OzAzNzg7dzAwMzswMjQxOzAwMzg7MDgwMzt3MjEyOzA4ODY7MjczMjt3MjI1OzExMDU7dzE0MDswMDM2OzAwMzk7dzU4MjswMzc5OzAyNDc7MTAxMTswOTE1OzAzNDQ7MTE0NTsxMDM5O3cyMDc7MDMxOTswMDQ0OzEwOTQ7dzIwMTtMUzA0OzA5MjA7d3A1OTsxMTM0O3cyMTE7MDkxNzt3NTgwO3cxMzM7dzEzNzt3MjIyOzEwMTI7czEwODsxMDA4OzA5MTk7dzEzMTswMjU2OzExMzM7MDEyMjt3NjIwO3dwNTc7MDc0NDswMzgxOzA5NzA7MDk5MjsxMDczOzEwOTU7MDM3MDswNDg2OzA4Mzg7dzIyMDsxMDUzOzAyNjE7MTEwODsyNzI2O3cxNDM7MDg5NTt3MjIzO2xzNDA7dzEzMjsxMDEwO2xzMjI7MDI1NTtsczM4OzA5NjA7MDQ3OTswNzUxOzA3MDM7MTEyMDswOTIyOzA5MTQ7MjY1MjsxMDkxOzAyNTM7MDc0OTs+PjtsPGk8MD47Pj47Oz47dDx0PDs7bDxpPDA+Oz4+Ozs+O3Q8O2w8aTwxPjs+O2w8dDxAMDw7Ozs7Ozs7Ozs7Pjs7Pjs+Pjt0PEAwPHA8cDxsPFBhZ2VDb3VudDtfIUl0ZW1Db3VudDtfIURhdGFTb3VyY2VJdGVtQ291bnQ7RGF0YUtleXM7PjtsPGk8MT47aTwwPjtpPDA+O2w8Pjs+Pjs+Ozs7Ozs7Ozs7Oz47Oz47dDxAMDxwPHA8bDxQYWdlQ291bnQ7XyFJdGVtQ291bnQ7XyFEYXRhU291cmNlSXRlbUNvdW50O0RhdGFLZXlzOz47bDxpPDE+O2k8MD47aTwwPjtsPD47Pj47Pjs7Ozs7Ozs7Ozs+Ozs+O3Q8O2w8aTwxPjs+O2w8dDxAMDw7Ozs7Ozs7Ozs7Pjs7Pjs+Pjs+Pjs+Pjs+lcwyLeh38zUKj1blRovX8DkjD2o=',
			casVerify: 'dDwtNjU0MzcyMTk1Ozs+FJ69NbU1bhqXdtghQQqSqVqAohQ=',
			score: 'dDwtMTg3MzYzMTY5MDt0PHA8bDx4aDtzZmRjYms7ZHlieXNjajt6eGNqY3h4czs+O2w8MTcwNTA3MDczMjtcZTtcZTswOz4+O2w8aTwxPjs+O2w8dDw7bDxpPDE+O2k8Mz47aTw1PjtpPDc+O2k8OT47aTwxMT47aTwxMz47aTwxNT47aTwyND47aTwyNT47aTwyNj47aTwzOT47aTw0Mz47aTw0NT47PjtsPHQ8cDxwPGw8VGV4dDs+O2w85a2m5Y+377yaMTcwNTA3MDczMjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85aeT5ZCN77ya6auY5YCp6Iy1Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzlrabpmaLvvJrotKLnu4/lrabpmaI7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOS4k+S4mu+8mjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w855S15a2Q5ZWG5YqhOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzooYzmlL/nj63vvJoxN+eUteWVhjfnj607Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDIwMTcwNTA3Oz4+Oz47Oz47dDx0PDt0PGk8MTk+O0A8XGU7MjAwMS0yMDAyOzIwMDItMjAwMzsyMDAzLTIwMDQ7MjAwNC0yMDA1OzIwMDUtMjAwNjsyMDA2LTIwMDc7MjAwNy0yMDA4OzIwMDgtMjAwOTsyMDA5LTIwMTA7MjAxMC0yMDExOzIwMTEtMjAxMjsyMDEyLTIwMTM7MjAxMy0yMDE0OzIwMTQtMjAxNTsyMDE1LTIwMTY7MjAxNi0yMDE3OzIwMTctMjAxODsyMDE4LTIwMTk7PjtAPFxlOzIwMDEtMjAwMjsyMDAyLTIwMDM7MjAwMy0yMDA0OzIwMDQtMjAwNTsyMDA1LTIwMDY7MjAwNi0yMDA3OzIwMDctMjAwODsyMDA4LTIwMDk7MjAwOS0yMDEwOzIwMTAtMjAxMTsyMDExLTIwMTI7MjAxMi0yMDEzOzIwMTMtMjAxNDsyMDE0LTIwMTU7MjAxNS0yMDE2OzIwMTYtMjAxNzsyMDE3LTIwMTg7MjAxOC0yMDE5Oz4+Oz47Oz47dDxwPDtwPGw8b25jbGljazs+O2w8d2luZG93LnByaW50KClcOzs+Pj47Oz47dDxwPDtwPGw8b25jbGljazs+O2w8d2luZG93LmNsb3NlKClcOzs+Pj47Oz47dDxwPHA8bDxWaXNpYmxlOz47bDxvPHQ+Oz4+Oz47Oz47dDw7bDxpPDA+O2k8MT47aTwzPjtpPDU+O2k8Nz47aTw5PjtpPDExPjtpPDIxPjs+O2w8dDxAMDw7Ozs7Ozs7Ozs7Pjs7Pjt0PEAwPDs7Ozs7Ozs7Ozs+Ozs+O3Q8QDA8Ozs7Ozs7Ozs7Oz47Oz47dDxAMDw7Ozs7Ozs7Ozs7Pjs7Pjt0PEAwPDs7Ozs7Ozs7Ozs+Ozs+O3Q8QDA8cDxwPGw8VmlzaWJsZTs+O2w8bzxmPjs+Pjs+Ozs7Ozs7Ozs7Oz47Oz47dDxAMDxwPHA8bDxWaXNpYmxlOz47bDxvPGY+Oz4+Oz47Ozs7Ozs7Ozs7Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPHpmOz4+Oz47Oz47Pj47dDxAMDw7Ozs7Ozs7Ozs7Pjs7Pjt0PEAwPDs7Ozs7Ozs7Ozs+Ozs+Oz4+Oz4+Oz4tE7hB/qrw/KyQuX4rGmX+pSBGwA==',
			grabCourse: 'dDwtNzY1ODgyNzY3O3Q8cDxsPGRxc3pqO3p5ZG07WEtYTjtYS1hROz47bDwyMDE2OzAxMTc7MjAxNy0yMDE4OzI7Pj47bDxpPDE+Oz47bDx0PDtsPGk8MT47aTwzPjtpPDU+O2k8Nz47aTw5PjtpPDExPjtpPDE5PjtpPDIxPjtpPDIzPjtpPDI3PjtpPDI5Pjs+O2w8dDxwPHA8bDxUZXh0Oz47bDzlp5PlkI3vvJrpmYjngpzlrr4mbmJzcFw7Jm5ic3BcOyZuYnNwXDsmbmJzcFw75a2m6Zmi77ya5L+h5oGv5bel56iL5a2m6ZmiJm5ic3BcOyZuYnNwXDsmbmJzcFw7Jm5ic3BcO+S4k+S4mu+8mui9r+S7tuaKgOacrzs+Pjs+Ozs+O3Q8dDxwPHA8bDxEYXRhVGV4dEZpZWxkO0RhdGFWYWx1ZUZpZWxkOz47bDxrY3h6O2tjeHo7Pj47Pjt0PGk8Mj47QDzlhazpgInor747XGU7PjtAPOWFrOmAieivvjtcZTs+Pjs+Ozs+O3Q8dDw7cDxsPGk8MD47aTwxPjtpPDI+Oz47bDxwPOaciTvmnIk+O3A85pegO+aXoD47cDxcZTtcZT47Pj47bDxpPDA+Oz4+Ozs+O3Q8dDxwPHA8bDxEYXRhVGV4dEZpZWxkO0RhdGFWYWx1ZUZpZWxkOz47bDxrY2dzO2tjZ3M7Pj47Pjt0PGk8Mj47QDxCO1xlOz47QDxCO1xlOz4+O2w8aTwxPjs+Pjs7Pjt0PHQ8cDxwPGw8RGF0YVRleHRGaWVsZDtEYXRhVmFsdWVGaWVsZDs+O2w8c2tzajtza3NqOz4+Oz47dDxpPDg+O0A85ZGo5LqM56ysNSw26IqCe+esrDItMTblkah85Y+M5ZGofVw75ZGo5LqM56ysNyw46IqCe+esrDItMTblkah85Y+M5ZGofTvlkajkuoznrKw1LDboioJ756ysMy0xN+WRqHzljZXlkah9XDvlkajkuoznrKw3LDjoioJ756ysMy0xN+WRqHzljZXlkah9O+WRqOS6jOesrDUsNuiKgnvnrKw0LTE35ZGofOWNleWRqH1cO+WRqOS6jOesrDcsOOiKgnvnrKw0LTTlkah85Y+M5ZGofVw75ZGo5LqM56ysNyw46IqCe+esrDQtMTflkah85Y2V5ZGofTvlkajkuoznrKw5LDEw6IqCe+esrDItMTflkah9O+WRqOS4ieesrDksMTDoioJ756ysMi0xN+WRqH075ZGo5Zub56ysOSwxMOiKgnvnrKwyLTE35ZGofTvlkajkuIDnrKw5LDEw6IqCe+esrDItMTflkah9O1xlOz47QDzlkajkuoznrKw1LDboioJ756ysMi0xNuWRqHzlj4zlkah9XDvlkajkuoznrKw3LDjoioJ756ysMi0xNuWRqHzlj4zlkah9O+WRqOS6jOesrDUsNuiKgnvnrKwzLTE35ZGofOWNleWRqH1cO+WRqOS6jOesrDcsOOiKgnvnrKwzLTE35ZGofOWNleWRqH075ZGo5LqM56ysNSw26IqCe+esrDQtMTflkah85Y2V5ZGofVw75ZGo5LqM56ysNyw46IqCe+esrDQtNOWRqHzlj4zlkah9XDvlkajkuoznrKw3LDjoioJ756ysNC0xN+WRqHzljZXlkah9O+WRqOS6jOesrDksMTDoioJ756ysMi0xN+WRqH075ZGo5LiJ56ysOSwxMOiKgnvnrKwyLTE35ZGofTvlkajlm5vnrKw5LDEw6IqCe+esrDItMTflkah9O+WRqOS4gOesrDksMTDoioJ756ysMi0xN+WRqH07XGU7Pj47bDxpPDc+Oz4+Ozs+O3Q8dDxwPHA8bDxEYXRhVGV4dEZpZWxkO0RhdGFWYWx1ZUZpZWxkOz47bDx4cW1jO3hxZG07Pj47Pjt0PGk8MT47QDzmnKzpg6g7PjtAPDE7Pj47bDxpPDA+Oz4+Ozs+O3Q8O2w8aTwxPjs+O2w8dDxAMDw7Ozs7Ozs7Ozs7Pjs7Pjs+Pjt0PEAwPHA8cDxsPFBhZ2VDb3VudDtfIUl0ZW1Db3VudDtfIURhdGFTb3VyY2VJdGVtQ291bnQ7RGF0YUtleXM7PjtsPGk8MT47aTwxNT47aTwxNT47bDw+Oz4+Oz47Ozs7Ozs7Ozs7PjtsPGk8MD47PjtsPHQ8O2w8aTwxPjtpPDI+O2k8Mz47aTw0PjtpPDU+O2k8Nj47aTw3PjtpPDg+O2k8OT47aTwxMD47aTwxMT47aTwxMj47aTwxMz47aTwxND47aTwxNT47PjtsPHQ8O2w8aTwyPjtpPDM+O2k8ND47aTw1PjtpPDY+O2k8Nz47aTw4PjtpPDk+O2k8MTA+O2k8MTE+O2k8MTI+O2k8MTM+O2k8MTQ+O2k8MTU+O2k8MTY+O2k8MTc+O2k8MTg+O2k8MTk+O2k8MjA+O2k8MjE+O2k8MjI+O2k8MjM+O2k8MjU+Oz47bDx0PHA8cDxsPFRleHQ7PjtsPFw8YSBocmVmPScjJyBvbmNsaWNrPSJ3aW5kb3cub3Blbigna2N4eC5hc3B4P3hoPTE2MDExNzAxMzAma2NkbT1QQTUxMDAwMyZ4a2toPSgyMDE3LTIwMTgtMiktUEE1MTAwMDMtMDMzNy0xJywna2N4eCcsJ3Rvb2xiYXI9MCxsb2NhdGlvbj0wLGRpcmVjdG9yaWVzPTAsc3RhdHVzPTAsbWVudWJhcj0wLHNjcm9sbGJhcnM9MSxyZXNpemFibGU9MCx3aWR0aD01NDAsaGVpZ2h0PTQ1MCxsZWZ0PTEyMCx0b3A9NjAnKSJcPuWPkeWxleW/g+eQhuWtplw8L2FcPjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8UEE1MTAwMDM7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPFw8YSBocmVmPScjJyBvbmNsaWNrPSJ3aW5kb3cub3BlbignanN4eC5hc3B4P3hoPTE2MDExNzAxMzAmanN6Z2g9MDMzNyZ4a2toPSgyMDE3LTIwMTgtMiktUEE1MTAwMDMtMDMzNy0xJywnanN4eCcsJ3Rvb2xiYXI9MCxsb2NhdGlvbj0wLGRpcmVjdG9yaWVzPTAsc3RhdHVzPTAsbWVudWJhcj0wLHNjcm9sbGJhcnM9MSxyZXNpemFibGU9MCx3aWR0aD01NDAsaGVpZ2h0PTQ1MCxsZWZ0PTEyMCx0b3A9NjAnKSJcPuWQtOa1t+iQjVw8L2FcPjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85ZGo5LiA56ysOSwxMOiKgnvnrKwyLTE35ZGofTs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85pWZMy0yMDc7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDI7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDIuMC0wLjA7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDMyOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwwMi0xNzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MTEwOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwxMDk7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDE7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCgyMDE3LTIwMTgtMiktUEE1MTAwMDMtMDMzNy0xOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwmbmJzcFw7Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDxQQTUxMDAwMzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MDMzNzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Qjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85YWs6YCJ6K++Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzmnKzpg6g7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCZuYnNwXDs7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOaVmeWKoeWkhDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Jm5ic3BcOzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Jm5ic3BcOzs+Pjs+Ozs+Oz4+O3Q8O2w8aTwyPjtpPDM+O2k8ND47aTw1PjtpPDY+O2k8Nz47aTw4PjtpPDk+O2k8MTA+O2k8MTE+O2k8MTI+O2k8MTM+O2k8MTQ+O2k8MTU+O2k8MTY+O2k8MTc+O2k8MTg+O2k8MTk+O2k8MjA+O2k8MjE+O2k8MjI+O2k8MjM+O2k8MjU+Oz47bDx0PHA8cDxsPFRleHQ7PjtsPFw8YSBocmVmPScjJyBvbmNsaWNrPSJ3aW5kb3cub3Blbigna2N4eC5hc3B4P3hoPTE2MDExNzAxMzAma2NkbT1QQTUxMDAwOCZ4a2toPSgyMDE3LTIwMTgtMiktUEE1MTAwMDgtMDQ5MC0xJywna2N4eCcsJ3Rvb2xiYXI9MCxsb2NhdGlvbj0wLGRpcmVjdG9yaWVzPTAsc3RhdHVzPTAsbWVudWJhcj0wLHNjcm9sbGJhcnM9MSxyZXNpemFibGU9MCx3aWR0aD01NDAsaGVpZ2h0PTQ1MCxsZWZ0PTEyMCx0b3A9NjAnKSJcPuayn+mAmuaKgOW3p1w8L2FcPjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8UEE1MTAwMDg7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPFw8YSBocmVmPScjJyBvbmNsaWNrPSJ3aW5kb3cub3BlbignanN4eC5hc3B4P3hoPTE2MDExNzAxMzAmanN6Z2g9MDQ5MCZ4a2toPSgyMDE3LTIwMTgtMiktUEE1MTAwMDgtMDQ5MC0xJywnanN4eCcsJ3Rvb2xiYXI9MCxsb2NhdGlvbj0wLGRpcmVjdG9yaWVzPTAsc3RhdHVzPTAsbWVudWJhcj0wLHNjcm9sbGJhcnM9MSxyZXNpemFibGU9MCx3aWR0aD01NDAsaGVpZ2h0PTQ1MCxsZWZ0PTEyMCx0b3A9NjAnKSJcPuWQleWNsOaWh1w8L2FcPjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85ZGo5LiA56ysOSwxMOiKgnvnrKwyLTE35ZGofTs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85pWZMy0zMDU7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDI7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDIuMC0wLjA7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDMyOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwwMi0xNzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MTEwOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwxMDg7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDI7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCgyMDE3LTIwMTgtMiktUEE1MTAwMDgtMDQ5MC0xOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwmbmJzcFw7Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDxQQTUxMDAwODs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MDQ5MDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Qjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85YWs6YCJ6K++Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzmnKzpg6g7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCZuYnNwXDs7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOaVmeWKoeWkhDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Jm5ic3BcOzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Jm5ic3BcOzs+Pjs+Ozs+Oz4+O3Q8O2w8aTwyPjtpPDM+O2k8ND47aTw1PjtpPDY+O2k8Nz47aTw4PjtpPDk+O2k8MTA+O2k8MTE+O2k8MTI+O2k8MTM+O2k8MTQ+O2k8MTU+O2k8MTY+O2k8MTc+O2k8MTg+O2k8MTk+O2k8MjA+O2k8MjE+O2k8MjI+O2k8MjM+O2k8MjU+Oz47bDx0PHA8cDxsPFRleHQ7PjtsPFw8YSBocmVmPScjJyBvbmNsaWNrPSJ3aW5kb3cub3Blbigna2N4eC5hc3B4P3hoPTE2MDExNzAxMzAma2NkbT1QQTUxMDAxMyZ4a2toPSgyMDE3LTIwMTgtMiktUEE1MTAwMTMtMDQzMi0xJywna2N4eCcsJ3Rvb2xiYXI9MCxsb2NhdGlvbj0wLGRpcmVjdG9yaWVzPTAsc3RhdHVzPTAsbWVudWJhcj0wLHNjcm9sbGJhcnM9MSxyZXNpemFibGU9MCx3aWR0aD01NDAsaGVpZ2h0PTQ1MCxsZWZ0PTEyMCx0b3A9NjAnKSJcPuWutueUteS4jueUn+a0u1w8L2FcPjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8UEE1MTAwMTM7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPFw8YSBocmVmPScjJyBvbmNsaWNrPSJ3aW5kb3cub3BlbignanN4eC5hc3B4P3hoPTE2MDExNzAxMzAmanN6Z2g9MDQzMiZ4a2toPSgyMDE3LTIwMTgtMiktUEE1MTAwMTMtMDQzMi0xJywnanN4eCcsJ3Rvb2xiYXI9MCxsb2NhdGlvbj0wLGRpcmVjdG9yaWVzPTAsc3RhdHVzPTAsbWVudWJhcj0wLHNjcm9sbGJhcnM9MSxyZXNpemFibGU9MCx3aWR0aD01NDAsaGVpZ2h0PTQ1MCxsZWZ0PTEyMCx0b3A9NjAnKSJcPuWPpOa4heWBpVw8L2FcPjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85ZGo5LiA56ysOSwxMOiKgnvnrKwyLTE35ZGofTs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85pWZMy0zMDY7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDI7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDIuMC0wLjA7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDMyOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwwMi0xNzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MTEwOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwxMDY7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDQ7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCgyMDE3LTIwMTgtMiktUEE1MTAwMTMtMDQzMi0xOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwmbmJzcFw7Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDxQQTUxMDAxMzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MDQzMjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Qjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85YWs6YCJ6K++Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzmnKzpg6g7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCZuYnNwXDs7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOaVmeWKoeWkhDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Jm5ic3BcOzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Jm5ic3BcOzs+Pjs+Ozs+Oz4+O3Q8O2w8aTwyPjtpPDM+O2k8ND47aTw1PjtpPDY+O2k8Nz47aTw4PjtpPDk+O2k8MTA+O2k8MTE+O2k8MTI+O2k8MTM+O2k8MTQ+O2k8MTU+O2k8MTY+O2k8MTc+O2k8MTg+O2k8MTk+O2k8MjA+O2k8MjE+O2k8MjI+O2k8MjM+O2k8MjU+Oz47bDx0PHA8cDxsPFRleHQ7PjtsPFw8YSBocmVmPScjJyBvbmNsaWNrPSJ3aW5kb3cub3Blbigna2N4eC5hc3B4P3hoPTE2MDExNzAxMzAma2NkbT1QQTUxMDAyMyZ4a2toPSgyMDE3LTIwMTgtMiktUEE1MTAwMjMtMDE5My0xJywna2N4eCcsJ3Rvb2xiYXI9MCxsb2NhdGlvbj0wLGRpcmVjdG9yaWVzPTAsc3RhdHVzPTAsbWVudWJhcj0wLHNjcm9sbGJhcnM9MSxyZXNpemFibGU9MCx3aWR0aD01NDAsaGVpZ2h0PTQ1MCxsZWZ0PTEyMCx0b3A9NjAnKSJcPueUn+a0u+S4reeahOe7j+a1juWtplw8L2FcPjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8UEE1MTAwMjM7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPFw8YSBocmVmPScjJyBvbmNsaWNrPSJ3aW5kb3cub3BlbignanN4eC5hc3B4P3hoPTE2MDExNzAxMzAmanN6Z2g9MDE5MyZ4a2toPSgyMDE3LTIwMTgtMiktUEE1MTAwMjMtMDE5My0xJywnanN4eCcsJ3Rvb2xiYXI9MCxsb2NhdGlvbj0wLGRpcmVjdG9yaWVzPTAsc3RhdHVzPTAsbWVudWJhcj0wLHNjcm9sbGJhcnM9MSxyZXNpemFibGU9MCx3aWR0aD01NDAsaGVpZ2h0PTQ1MCxsZWZ0PTEyMCx0b3A9NjAnKSJcPumZiOWwj+WQm1w8L2FcPjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85ZGo5LiJ56ysOSwxMOiKgnvnrKwyLTE35ZGofTs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85pWZMy0yMDI7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDI7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDIuMC0wLjA7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDMyOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwwMi0xNzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8OTA7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDg5Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwxOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwoMjAxNy0yMDE4LTIpLVBBNTEwMDIzLTAxOTMtMTs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Jm5ic3BcOzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8UEE1MTAwMjM7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDAxOTM7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPEI7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOWFrOmAieivvjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85pys6YOoOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwmbmJzcFw7Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzmlZnliqHlpIQ7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCZuYnNwXDs7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCZuYnNwXDs7Pj47Pjs7Pjs+Pjt0PDtsPGk8Mj47aTwzPjtpPDQ+O2k8NT47aTw2PjtpPDc+O2k8OD47aTw5PjtpPDEwPjtpPDExPjtpPDEyPjtpPDEzPjtpPDE0PjtpPDE1PjtpPDE2PjtpPDE3PjtpPDE4PjtpPDE5PjtpPDIwPjtpPDIxPjtpPDIyPjtpPDIzPjtpPDI1Pjs+O2w8dDxwPHA8bDxUZXh0Oz47bDxcPGEgaHJlZj0nIycgb25jbGljaz0id2luZG93Lm9wZW4oJ2tjeHguYXNweD94aD0xNjAxMTcwMTMwJmtjZG09UEE1MTAwNTEmeGtraD0oMjAxNy0yMDE4LTIpLVBBNTEwMDUxLTA5MzMtMScsJ2tjeHgnLCd0b29sYmFyPTAsbG9jYXRpb249MCxkaXJlY3Rvcmllcz0wLHN0YXR1cz0wLG1lbnViYXI9MCxzY3JvbGxiYXJzPTEscmVzaXphYmxlPTAsd2lkdGg9NTQwLGhlaWdodD00NTAsbGVmdD0xMjAsdG9wPTYwJykiXD7ms6XloZFcPC9hXD47Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPFBBNTEwMDUxOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDxcPGEgaHJlZj0nIycgb25jbGljaz0id2luZG93Lm9wZW4oJ2pzeHguYXNweD94aD0xNjAxMTcwMTMwJmpzemdoPTA5MzMmeGtraD0oMjAxNy0yMDE4LTIpLVBBNTEwMDUxLTA5MzMtMScsJ2pzeHgnLCd0b29sYmFyPTAsbG9jYXRpb249MCxkaXJlY3Rvcmllcz0wLHN0YXR1cz0wLG1lbnViYXI9MCxzY3JvbGxiYXJzPTEscmVzaXphYmxlPTAsd2lkdGg9NTQwLGhlaWdodD00NTAsbGVmdD0xMjAsdG9wPTYwJykiXD7pvprokI1cPC9hXD47Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOWRqOS4gOesrDksMTDoioJ756ysMi0xN+WRqH07Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOWunuiurTMtMjAyOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwyLjA7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDIuMC0wLjA7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDMyOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwwMi0xNzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MjU7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDI0Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwxOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwoMjAxNy0yMDE4LTIpLVBBNTEwMDUxLTA5MzMtMTs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Jm5ic3BcOzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8UEE1MTAwNTE7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDA5MzM7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPEI7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOWFrOmAieivvjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85pys6YOoOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwmbmJzcFw7Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzmlZnliqHlpIQ7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCZuYnNwXDs7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCZuYnNwXDs7Pj47Pjs7Pjs+Pjt0PDtsPGk8Mj47aTwzPjtpPDQ+O2k8NT47aTw2PjtpPDc+O2k8OD47aTw5PjtpPDEwPjtpPDExPjtpPDEyPjtpPDEzPjtpPDE0PjtpPDE1PjtpPDE2PjtpPDE3PjtpPDE4PjtpPDE5PjtpPDIwPjtpPDIxPjtpPDIyPjtpPDIzPjtpPDI1Pjs+O2w8dDxwPHA8bDxUZXh0Oz47bDxcPGEgaHJlZj0nIycgb25jbGljaz0id2luZG93Lm9wZW4oJ2tjeHguYXNweD94aD0xNjAxMTcwMTMwJmtjZG09UEE1MTAwNTQmeGtraD0oMjAxNy0yMDE4LTIpLVBBNTEwMDU0LTA5MzMtMScsJ2tjeHgnLCd0b29sYmFyPTAsbG9jYXRpb249MCxkaXJlY3Rvcmllcz0wLHN0YXR1cz0wLG1lbnViYXI9MCxzY3JvbGxiYXJzPTEscmVzaXphYmxlPTAsd2lkdGg9NTQwLGhlaWdodD00NTAsbGVmdD0xMjAsdG9wPTYwJykiXD7pmbbnk7fppbDlk4Horr7orqHkuI7liLbkvZxcPC9hXD47Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPFBBNTEwMDU0Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDxcPGEgaHJlZj0nIycgb25jbGljaz0id2luZG93Lm9wZW4oJ2pzeHguYXNweD94aD0xNjAxMTcwMTMwJmpzemdoPTA5MzMmeGtraD0oMjAxNy0yMDE4LTIpLVBBNTEwMDU0LTA5MzMtMScsJ2pzeHgnLCd0b29sYmFyPTAsbG9jYXRpb249MCxkaXJlY3Rvcmllcz0wLHN0YXR1cz0wLG1lbnViYXI9MCxzY3JvbGxiYXJzPTEscmVzaXphYmxlPTAsd2lkdGg9NTQwLGhlaWdodD00NTAsbGVmdD0xMjAsdG9wPTYwJykiXD7pvprokI1cPC9hXD47Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOWRqOS4ieesrDksMTDoioJ756ysMi0xN+WRqH07Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOWunuiurTMtMjAyOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwyLjA7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDIuMC0wLjA7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDMyOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwwMi0xNzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MzA7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDI5Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwxOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwoMjAxNy0yMDE4LTIpLVBBNTEwMDU0LTA5MzMtMTs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Jm5ic3BcOzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8UEE1MTAwNTQ7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDA5MzM7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPEI7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOWFrOmAieivvjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85pys6YOoOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwmbmJzcFw7Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzmlZnliqHlpIQ7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCZuYnNwXDs7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCZuYnNwXDs7Pj47Pjs7Pjs+Pjt0PDtsPGk8Mj47aTwzPjtpPDQ+O2k8NT47aTw2PjtpPDc+O2k8OD47aTw5PjtpPDEwPjtpPDExPjtpPDEyPjtpPDEzPjtpPDE0PjtpPDE1PjtpPDE2PjtpPDE3PjtpPDE4PjtpPDE5PjtpPDIwPjtpPDIxPjtpPDIyPjtpPDIzPjtpPDI1Pjs+O2w8dDxwPHA8bDxUZXh0Oz47bDxcPGEgaHJlZj0nIycgb25jbGljaz0id2luZG93Lm9wZW4oJ2tjeHguYXNweD94aD0xNjAxMTcwMTMwJmtjZG09UEE1MTAwNTUmeGtraD0oMjAxNy0yMDE4LTIpLVBBNTEwMDU1LXMxMjYtMScsJ2tjeHgnLCd0b29sYmFyPTAsbG9jYXRpb249MCxkaXJlY3Rvcmllcz0wLHN0YXR1cz0wLG1lbnViYXI9MCxzY3JvbGxiYXJzPTEscmVzaXphYmxlPTAsd2lkdGg9NTQwLGhlaWdodD00NTAsbGVmdD0xMjAsdG9wPTYwJykiXD7nr4bliLtcPC9hXD47Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPFBBNTEwMDU1Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDxcPGEgaHJlZj0nIycgb25jbGljaz0id2luZG93Lm9wZW4oJ2pzeHguYXNweD94aD0xNjAxMTcwMTMwJmpzemdoPXMxMjYmeGtraD0oMjAxNy0yMDE4LTIpLVBBNTEwMDU1LXMxMjYtMScsJ2pzeHgnLCd0b29sYmFyPTAsbG9jYXRpb249MCxkaXJlY3Rvcmllcz0wLHN0YXR1cz0wLG1lbnViYXI9MCxzY3JvbGxiYXJzPTEscmVzaXphYmxlPTAsd2lkdGg9NTQwLGhlaWdodD00NTAsbGVmdD0xMjAsdG9wPTYwJykiXD7lva3npo/ljp9cPC9hXD47Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOWRqOS4gOesrDksMTDoioJ756ysMi0xN+WRqH07Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOWunuiurTMtMzA3Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwyLjA7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDIuMC0wLjA7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDMyOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwwMi0xNzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MjU7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDI0Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwxOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwoMjAxNy0yMDE4LTIpLVBBNTEwMDU1LXMxMjYtMTs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Jm5ic3BcOzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8UEE1MTAwNTU7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPHMxMjY7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPEI7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOWFrOmAieivvjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85pys6YOoOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwmbmJzcFw7Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzmlZnliqHlpIQ7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCZuYnNwXDs7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCZuYnNwXDs7Pj47Pjs7Pjs+Pjt0PDtsPGk8Mj47aTwzPjtpPDQ+O2k8NT47aTw2PjtpPDc+O2k8OD47aTw5PjtpPDEwPjtpPDExPjtpPDEyPjtpPDEzPjtpPDE0PjtpPDE1PjtpPDE2PjtpPDE3PjtpPDE4PjtpPDE5PjtpPDIwPjtpPDIxPjtpPDIyPjtpPDIzPjtpPDI1Pjs+O2w8dDxwPHA8bDxUZXh0Oz47bDxcPGEgaHJlZj0nIycgb25jbGljaz0id2luZG93Lm9wZW4oJ2tjeHguYXNweD94aD0xNjAxMTcwMTMwJmtjZG09UEE1MTAwNzcmeGtraD0oMjAxNy0yMDE4LTIpLVBBNTEwMDc3LTA5NTUtMScsJ2tjeHgnLCd0b29sYmFyPTAsbG9jYXRpb249MCxkaXJlY3Rvcmllcz0wLHN0YXR1cz0wLG1lbnViYXI9MCxzY3JvbGxiYXJzPTEscmVzaXphYmxlPTAsd2lkdGg9NTQwLGhlaWdodD00NTAsbGVmdD0xMjAsdG9wPTYwJykiXD7llYbliqHosIjliKTlrp7liqFcPC9hXD47Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPFBBNTEwMDc3Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDxcPGEgaHJlZj0nIycgb25jbGljaz0id2luZG93Lm9wZW4oJ2pzeHguYXNweD94aD0xNjAxMTcwMTMwJmpzemdoPTA5NTUmeGtraD0oMjAxNy0yMDE4LTIpLVBBNTEwMDc3LTA5NTUtMScsJ2pzeHgnLCd0b29sYmFyPTAsbG9jYXRpb249MCxkaXJlY3Rvcmllcz0wLHN0YXR1cz0wLG1lbnViYXI9MCxzY3JvbGxiYXJzPTEscmVzaXphYmxlPTAsd2lkdGg9NTQwLGhlaWdodD00NTAsbGVmdD0xMjAsdG9wPTYwJykiXD7lp5zmsrvlvLpcPC9hXD47Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOWRqOS4ieesrDksMTDoioJ756ysMi0xN+WRqH07Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOaVmTMtMzAxOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwyLjA7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDIuMC0wLjA7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDMyOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwwMi0xNzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MTEwOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwxMDk7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDE7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCgyMDE3LTIwMTgtMiktUEE1MTAwNzctMDk1NS0xOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwmbmJzcFw7Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDxQQTUxMDA3Nzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MDk1NTs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Qjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85YWs6YCJ6K++Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzmnKzpg6g7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCZuYnNwXDs7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOaVmeWKoeWkhDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Jm5ic3BcOzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Jm5ic3BcOzs+Pjs+Ozs+Oz4+O3Q8O2w8aTwyPjtpPDM+O2k8ND47aTw1PjtpPDY+O2k8Nz47aTw4PjtpPDk+O2k8MTA+O2k8MTE+O2k8MTI+O2k8MTM+O2k8MTQ+O2k8MTU+O2k8MTY+O2k8MTc+O2k8MTg+O2k8MTk+O2k8MjA+O2k8MjE+O2k8MjI+O2k8MjM+O2k8MjU+Oz47bDx0PHA8cDxsPFRleHQ7PjtsPFw8YSBocmVmPScjJyBvbmNsaWNrPSJ3aW5kb3cub3Blbigna2N4eC5hc3B4P3hoPTE2MDExNzAxMzAma2NkbT1QQTUxMDA3OCZ4a2toPSgyMDE3LTIwMTgtMiktUEE1MTAwNzgtMDc0Mi0xJywna2N4eCcsJ3Rvb2xiYXI9MCxsb2NhdGlvbj0wLGRpcmVjdG9yaWVzPTAsc3RhdHVzPTAsbWVudWJhcj0wLHNjcm9sbGJhcnM9MSxyZXNpemFibGU9MCx3aWR0aD01NDAsaGVpZ2h0PTQ1MCxsZWZ0PTEyMCx0b3A9NjAnKSJcPuaXpeivreWPo+ivreWFpemXqFw8L2FcPjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8UEE1MTAwNzg7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPFw8YSBocmVmPScjJyBvbmNsaWNrPSJ3aW5kb3cub3BlbignanN4eC5hc3B4P3hoPTE2MDExNzAxMzAmanN6Z2g9MDc0MiZ4a2toPSgyMDE3LTIwMTgtMiktUEE1MTAwNzgtMDc0Mi0xJywnanN4eCcsJ3Rvb2xiYXI9MCxsb2NhdGlvbj0wLGRpcmVjdG9yaWVzPTAsc3RhdHVzPTAsbWVudWJhcj0wLHNjcm9sbGJhcnM9MSxyZXNpemFibGU9MCx3aWR0aD01NDAsaGVpZ2h0PTQ1MCxsZWZ0PTEyMCx0b3A9NjAnKSJcPumZiOiCsuWLpFw8L2FcPjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85ZGo5LiJ56ysOSwxMOiKgnvnrKwyLTE35ZGofTs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85pWZMy0yMDY7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDIuMDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Mi4wLTAuMDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MzI7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDAyLTE3Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwxMTE7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDEwOTs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Mjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8KDIwMTctMjAxOC0yKS1QQTUxMDA3OC0wNzQyLTE7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCZuYnNwXDs7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPFBBNTEwMDc4Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwwNzQyOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDxCOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzlhazpgInor747Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOacrOmDqDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Jm5ic3BcOzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85pWZ5Yqh5aSEOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwmbmJzcFw7Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwmbmJzcFw7Oz4+Oz47Oz47Pj47dDw7bDxpPDI+O2k8Mz47aTw0PjtpPDU+O2k8Nj47aTw3PjtpPDg+O2k8OT47aTwxMD47aTwxMT47aTwxMj47aTwxMz47aTwxND47aTwxNT47aTwxNj47aTwxNz47aTwxOD47aTwxOT47aTwyMD47aTwyMT47aTwyMj47aTwyMz47aTwyNT47PjtsPHQ8cDxwPGw8VGV4dDs+O2w8XDxhIGhyZWY9JyMnIG9uY2xpY2s9IndpbmRvdy5vcGVuKCdrY3h4LmFzcHg/eGg9MTYwMTE3MDEzMCZrY2RtPVBBNTEwMDg1Jnhra2g9KDIwMTctMjAxOC0yKS1QQTUxMDA4NS0wOTk3LTEnLCdrY3h4JywndG9vbGJhcj0wLGxvY2F0aW9uPTAsZGlyZWN0b3JpZXM9MCxzdGF0dXM9MCxtZW51YmFyPTAsc2Nyb2xsYmFycz0xLHJlc2l6YWJsZT0wLHdpZHRoPTU0MCxoZWlnaHQ9NDUwLGxlZnQ9MTIwLHRvcD02MCcpIlw+5Lit5Zu9546w5b2T5Luj6YCa5L+X5paH5a2m5a+86K+7XDwvYVw+Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDxQQTUxMDA4NTs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8XDxhIGhyZWY9JyMnIG9uY2xpY2s9IndpbmRvdy5vcGVuKCdqc3h4LmFzcHg/eGg9MTYwMTE3MDEzMCZqc3pnaD0wOTk3Jnhra2g9KDIwMTctMjAxOC0yKS1QQTUxMDA4NS0wOTk3LTEnLCdqc3h4JywndG9vbGJhcj0wLGxvY2F0aW9uPTAsZGlyZWN0b3JpZXM9MCxzdGF0dXM9MCxtZW51YmFyPTAsc2Nyb2xsYmFycz0xLHJlc2l6YWJsZT0wLHdpZHRoPTU0MCxoZWlnaHQ9NDUwLGxlZnQ9MTIwLHRvcD02MCcpIlw+5pu+5a6X5oChXDwvYVw+Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzlkajkuIDnrKw5LDEw6IqCe+esrDItMTflkah9Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzmlZkzLTIwMTs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Mi4wOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwyLjAtMC4wOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwzMjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MDItMTc7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDkwOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDw4Nzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Mzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8KDIwMTctMjAxOC0yKS1QQTUxMDA4NS0wOTk3LTE7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCZuYnNwXDs7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPFBBNTEwMDg1Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwwOTk3Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDxCOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzlhazpgInor747Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOacrOmDqDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Jm5ic3BcOzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85pWZ5Yqh5aSEOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwmbmJzcFw7Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwmbmJzcFw7Oz4+Oz47Oz47Pj47dDw7bDxpPDI+O2k8Mz47aTw0PjtpPDU+O2k8Nj47aTw3PjtpPDg+O2k8OT47aTwxMD47aTwxMT47aTwxMj47aTwxMz47aTwxND47aTwxNT47aTwxNj47aTwxNz47aTwxOD47aTwxOT47aTwyMD47aTwyMT47aTwyMj47aTwyMz47aTwyNT47PjtsPHQ8cDxwPGw8VGV4dDs+O2w8XDxhIGhyZWY9JyMnIG9uY2xpY2s9IndpbmRvdy5vcGVuKCdrY3h4LmFzcHg/eGg9MTYwMTE3MDEzMCZrY2RtPVBBNTEwMDk4Jnhra2g9KDIwMTctMjAxOC0yKS1QQTUxMDA5OC0wODU0LTEnLCdrY3h4JywndG9vbGJhcj0wLGxvY2F0aW9uPTAsZGlyZWN0b3JpZXM9MCxzdGF0dXM9MCxtZW51YmFyPTAsc2Nyb2xsYmFycz0xLHJlc2l6YWJsZT0wLHdpZHRoPTU0MCxoZWlnaHQ9NDUwLGxlZnQ9MTIwLHRvcD02MCcpIlw+5YWs5YWx5YWz57O75a2mXDwvYVw+Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDxQQTUxMDA5ODs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8XDxhIGhyZWY9JyMnIG9uY2xpY2s9IndpbmRvdy5vcGVuKCdqc3h4LmFzcHg/eGg9MTYwMTE3MDEzMCZqc3pnaD0wODU0Jnhra2g9KDIwMTctMjAxOC0yKS1QQTUxMDA5OC0wODU0LTEnLCdqc3h4JywndG9vbGJhcj0wLGxvY2F0aW9uPTAsZGlyZWN0b3JpZXM9MCxzdGF0dXM9MCxtZW51YmFyPTAsc2Nyb2xsYmFycz0xLHJlc2l6YWJsZT0wLHdpZHRoPTU0MCxoZWlnaHQ9NDUwLGxlZnQ9MTIwLHRvcD02MCcpIlw+5p2O6ImzXDwvYVw+Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzlkajkuInnrKw5LDEw6IqCe+esrDItMTflkah9Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzmlZkzLTMwMjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Mi4wOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwyLjAtMC4wOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwzMjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MDItMTc7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDExMDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MTA1Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDw1Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwoMjAxNy0yMDE4LTIpLVBBNTEwMDk4LTA4NTQtMTs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Jm5ic3BcOzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8UEE1MTAwOTg7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDA4NTQ7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPEI7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOWFrOmAieivvjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85pys6YOoOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwmbmJzcFw7Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzmlZnliqHlpIQ7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCZuYnNwXDs7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCZuYnNwXDs7Pj47Pjs7Pjs+Pjt0PDtsPGk8Mj47aTwzPjtpPDQ+O2k8NT47aTw2PjtpPDc+O2k8OD47aTw5PjtpPDEwPjtpPDExPjtpPDEyPjtpPDEzPjtpPDE0PjtpPDE1PjtpPDE2PjtpPDE3PjtpPDE4PjtpPDE5PjtpPDIwPjtpPDIxPjtpPDIyPjtpPDIzPjtpPDI1Pjs+O2w8dDxwPHA8bDxUZXh0Oz47bDxcPGEgaHJlZj0nIycgb25jbGljaz0id2luZG93Lm9wZW4oJ2tjeHguYXNweD94aD0xNjAxMTcwMTMwJmtjZG09UEI1MTAwMDYmeGtraD0oMjAxNy0yMDE4LTIpLVBCNTEwMDA2LXdwNzAtMScsJ2tjeHgnLCd0b29sYmFyPTAsbG9jYXRpb249MCxkaXJlY3Rvcmllcz0wLHN0YXR1cz0wLG1lbnViYXI9MCxzY3JvbGxiYXJzPTEscmVzaXphYmxlPTAsd2lkdGg9NTQwLGhlaWdodD00NTAsbGVmdD0xMjAsdG9wPTYwJykiXD7lub/nu6PmioDms5VcPC9hXD47Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPFBCNTEwMDA2Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDxcPGEgaHJlZj0nIycgb25jbGljaz0id2luZG93Lm9wZW4oJ2pzeHguYXNweD94aD0xNjAxMTcwMTMwJmpzemdoPXdwNzAmeGtraD0oMjAxNy0yMDE4LTIpLVBCNTEwMDA2LXdwNzAtMScsJ2pzeHgnLCd0b29sYmFyPTAsbG9jYXRpb249MCxkaXJlY3Rvcmllcz0wLHN0YXR1cz0wLG1lbnViYXI9MCxzY3JvbGxiYXJzPTEscmVzaXphYmxlPTAsd2lkdGg9NTQwLGhlaWdodD00NTAsbGVmdD0xMjAsdG9wPTYwJykiXD7olKHnjonnnJ9cPC9hXD47Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOWRqOS6jOesrDUsNuiKgnvnrKw0LTE35ZGofOWNleWRqH1cO+WRqOS6jOesrDcsOOiKgnvnrKw0LTTlkah85Y+M5ZGofVw75ZGo5LqM56ysNyw46IqCe+esrDQtMTflkah85Y2V5ZGofTs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85a6e6K6tMy0yMDNcO+WunuiurTMtMjAzXDvlrp7orq0zLTIwMzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Mjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8NC4wLTAuMDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MzI7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDAzLTE3Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwyNTs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MjQ7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDE7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCgyMDE3LTIwMTgtMiktUEI1MTAwMDYtd3A3MC0xOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwmbmJzcFw7Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDxQQjUxMDAwNjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8d3A3MDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Qjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85YWs6YCJ6K++Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzmnKzpg6g7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCZuYnNwXDs7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOaVmeWKoeWkhDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Jm5ic3BcOzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Jm5ic3BcOzs+Pjs+Ozs+Oz4+O3Q8O2w8aTwyPjtpPDM+O2k8ND47aTw1PjtpPDY+O2k8Nz47aTw4PjtpPDk+O2k8MTA+O2k8MTE+O2k8MTI+O2k8MTM+O2k8MTQ+O2k8MTU+O2k8MTY+O2k8MTc+O2k8MTg+O2k8MTk+O2k8MjA+O2k8MjE+O2k8MjI+O2k8MjM+O2k8MjU+Oz47bDx0PHA8cDxsPFRleHQ7PjtsPFw8YSBocmVmPScjJyBvbmNsaWNrPSJ3aW5kb3cub3Blbigna2N4eC5hc3B4P3hoPTE2MDExNzAxMzAma2NkbT1QQjUxMDAxMiZ4a2toPSgyMDE3LTIwMTgtMiktUEI1MTAwMTItMDk0Ni0xJywna2N4eCcsJ3Rvb2xiYXI9MCxsb2NhdGlvbj0wLGRpcmVjdG9yaWVzPTAsc3RhdHVzPTAsbWVudWJhcj0wLHNjcm9sbGJhcnM9MSxyZXNpemFibGU9MCx3aWR0aD01NDAsaGVpZ2h0PTQ1MCxsZWZ0PTEyMCx0b3A9NjAnKSJcPueDuemlquS4jueDmOeEmVw8L2FcPjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8UEI1MTAwMTI7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPFw8YSBocmVmPScjJyBvbmNsaWNrPSJ3aW5kb3cub3BlbignanN4eC5hc3B4P3hoPTE2MDExNzAxMzAmanN6Z2g9MDk0NiZ4a2toPSgyMDE3LTIwMTgtMiktUEI1MTAwMTItMDk0Ni0xJywnanN4eCcsJ3Rvb2xiYXI9MCxsb2NhdGlvbj0wLGRpcmVjdG9yaWVzPTAsc3RhdHVzPTAsbWVudWJhcj0wLHNjcm9sbGJhcnM9MSxyZXNpemFibGU9MCx3aWR0aD01NDAsaGVpZ2h0PTQ1MCxsZWZ0PTEyMCx0b3A9NjAnKSJcPue9l+a2m1w8L2FcPjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85ZGo5LiA56ysOSwxMOiKgnvnrKwyLTE35ZGofTs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85pWZMS01MDg7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDIuMDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Mi4wLTAuMDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MzI7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDAyLTE3Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDw1MDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8NDk7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDE7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCgyMDE3LTIwMTgtMiktUEI1MTAwMTItMDk0Ni0xOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwmbmJzcFw7Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDxQQjUxMDAxMjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MDk0Njs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Qjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85YWs6YCJ6K++Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzmnKzpg6g7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCZuYnNwXDs7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOaVmeWKoeWkhDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Jm5ic3BcOzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Jm5ic3BcOzs+Pjs+Ozs+Oz4+O3Q8O2w8aTwyPjtpPDM+O2k8ND47aTw1PjtpPDY+O2k8Nz47aTw4PjtpPDk+O2k8MTA+O2k8MTE+O2k8MTI+O2k8MTM+O2k8MTQ+O2k8MTU+O2k8MTY+O2k8MTc+O2k8MTg+O2k8MTk+O2k8MjA+O2k8MjE+O2k8MjI+O2k8MjM+O2k8MjU+Oz47bDx0PHA8cDxsPFRleHQ7PjtsPFw8YSBocmVmPScjJyBvbmNsaWNrPSJ3aW5kb3cub3Blbigna2N4eC5hc3B4P3hoPTE2MDExNzAxMzAma2NkbT1QQjUxMDAxNSZ4a2toPSgyMDE3LTIwMTgtMiktUEI1MTAwMTUtMDkwNi0xJywna2N4eCcsJ3Rvb2xiYXI9MCxsb2NhdGlvbj0wLGRpcmVjdG9yaWVzPTAsc3RhdHVzPTAsbWVudWJhcj0wLHNjcm9sbGJhcnM9MSxyZXNpemFibGU9MCx3aWR0aD01NDAsaGVpZ2h0PTQ1MCxsZWZ0PTEyMCx0b3A9NjAnKSJcPue0oOaPj+S4juWGmeeUn1w8L2FcPjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8UEI1MTAwMTU7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPFw8YSBocmVmPScjJyBvbmNsaWNrPSJ3aW5kb3cub3BlbignanN4eC5hc3B4P3hoPTE2MDExNzAxMzAmanN6Z2g9MDkwNiZ4a2toPSgyMDE3LTIwMTgtMiktUEI1MTAwMTUtMDkwNi0xJywnanN4eCcsJ3Rvb2xiYXI9MCxsb2NhdGlvbj0wLGRpcmVjdG9yaWVzPTAsc3RhdHVzPTAsbWVudWJhcj0wLHNjcm9sbGJhcnM9MSxyZXNpemFibGU9MCx3aWR0aD01NDAsaGVpZ2h0PTQ1MCxsZWZ0PTEyMCx0b3A9NjAnKSJcPuiBguamlVw8L2FcPjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85ZGo5LiA56ysOSwxMOiKgnvnrKwyLTE35ZGofTs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85a6e6K6tMy00MDg7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDIuMDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Mi4wLTAuMDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MzI7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDAyLTE3Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDw1MDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8NDk7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDE7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCgyMDE3LTIwMTgtMiktUEI1MTAwMTUtMDkwNi0xOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwmbmJzcFw7Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDxQQjUxMDAxNTs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MDkwNjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Qjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85YWs6YCJ6K++Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzmnKzpg6g7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCZuYnNwXDs7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOaVmeWKoeWkhDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Jm5ic3BcOzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Jm5ic3BcOzs+Pjs+Ozs+Oz4+O3Q8O2w8aTwyPjtpPDM+O2k8ND47aTw1PjtpPDY+O2k8Nz47aTw4PjtpPDk+O2k8MTA+O2k8MTE+O2k8MTI+O2k8MTM+O2k8MTQ+O2k8MTU+O2k8MTY+O2k8MTc+O2k8MTg+O2k8MTk+O2k8MjA+O2k8MjE+O2k8MjI+O2k8MjM+O2k8MjU+Oz47bDx0PHA8cDxsPFRleHQ7PjtsPFw8YSBocmVmPScjJyBvbmNsaWNrPSJ3aW5kb3cub3Blbigna2N4eC5hc3B4P3hoPTE2MDExNzAxMzAma2NkbT1QQjUxMDAyNCZ4a2toPSgyMDE3LTIwMTgtMiktUEI1MTAwMjQtMDM4My0xJywna2N4eCcsJ3Rvb2xiYXI9MCxsb2NhdGlvbj0wLGRpcmVjdG9yaWVzPTAsc3RhdHVzPTAsbWVudWJhcj0wLHNjcm9sbGJhcnM9MSxyZXNpemFibGU9MCx3aWR0aD01NDAsaGVpZ2h0PTQ1MCxsZWZ0PTEyMCx0b3A9NjAnKSJcPumZtueTt+e7mOeUu1w8L2FcPjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8UEI1MTAwMjQ7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPFw8YSBocmVmPScjJyBvbmNsaWNrPSJ3aW5kb3cub3BlbignanN4eC5hc3B4P3hoPTE2MDExNzAxMzAmanN6Z2g9MDM4MyZ4a2toPSgyMDE3LTIwMTgtMiktUEI1MTAwMjQtMDM4My0xJywnanN4eCcsJ3Rvb2xiYXI9MCxsb2NhdGlvbj0wLGRpcmVjdG9yaWVzPTAsc3RhdHVzPTAsbWVudWJhcj0wLHNjcm9sbGJhcnM9MSxyZXNpemFibGU9MCx3aWR0aD01NDAsaGVpZ2h0PTQ1MCxsZWZ0PTEyMCx0b3A9NjAnKSJcPuenpueOiVw8L2FcPjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85ZGo5Zub56ysOSwxMOiKgnvnrKwyLTE35ZGofTs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85a6e6K6tMy0yMDI7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDIuMDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Mi4wLTAuMDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MzI7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDAyLTE3Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwyNTs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MjM7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDI7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCgyMDE3LTIwMTgtMiktUEI1MTAwMjQtMDM4My0xOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwmbmJzcFw7Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDxQQjUxMDAyNDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MDM4Mzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Qjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85YWs6YCJ6K++Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzmnKzpg6g7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCZuYnNwXDs7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOaVmeWKoeWkhDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Jm5ic3BcOzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Jm5ic3BcOzs+Pjs+Ozs+Oz4+Oz4+Oz4+O3Q8QDA8cDxwPGw8UGFnZUNvdW50O18hSXRlbUNvdW50O18hRGF0YVNvdXJjZUl0ZW1Db3VudDtEYXRhS2V5czs+O2w8aTwxPjtpPDA+O2k8MD47bDw+Oz4+Oz47QDA8Ozs7Ozs7Ozs7Ozs7Ozs7O0AwPHA8bDxWaXNpYmxlOz47bDxvPGY+Oz4+Ozs7Oz47Pjs7Ozs7Ozs7Oz47Oz47dDxAMDw7Ozs7Ozs7Ozs7Pjs7Pjt0PHA8cDxsPFZpc2libGU7PjtsPG88Zj47Pj47Pjs7Pjs+Pjs+PjtsPGtjbWNHcmlkOl9jdGwyOnhrO2tjbWNHcmlkOl9jdGwyOmpjO2tjbWNHcmlkOl9jdGwzOnhrO2tjbWNHcmlkOl9jdGwzOmpjO2tjbWNHcmlkOl9jdGw0OnhrO2tjbWNHcmlkOl9jdGw0OmpjO2tjbWNHcmlkOl9jdGw1OnhrO2tjbWNHcmlkOl9jdGw1OmpjO2tjbWNHcmlkOl9jdGw2OnhrO2tjbWNHcmlkOl9jdGw2OmpjO2tjbWNHcmlkOl9jdGw3OnhrO2tjbWNHcmlkOl9jdGw3OmpjO2tjbWNHcmlkOl9jdGw4OnhrO2tjbWNHcmlkOl9jdGw4OmpjO2tjbWNHcmlkOl9jdGw5OnhrO2tjbWNHcmlkOl9jdGw5OmpjO2tjbWNHcmlkOl9jdGwxMDp4aztrY21jR3JpZDpfY3RsMTA6amM7a2NtY0dyaWQ6X2N0bDExOnhrO2tjbWNHcmlkOl9jdGwxMTpqYztrY21jR3JpZDpfY3RsMTI6eGs7a2NtY0dyaWQ6X2N0bDEyOmpjO2tjbWNHcmlkOl9jdGwxMzp4aztrY21jR3JpZDpfY3RsMTM6amM7a2NtY0dyaWQ6X2N0bDE0OnhrO2tjbWNHcmlkOl9jdGwxNDpqYztrY21jR3JpZDpfY3RsMTU6eGs7a2NtY0dyaWQ6X2N0bDE1OmpjO2tjbWNHcmlkOl9jdGwxNjp4aztrY21jR3JpZDpfY3RsMTY6amM7Pj6j+hWBqmvCJ996M5kqmjPXrg6B0Q=='
		}
	},

	cas: {
		url: 'https://cas.gdcxxy.net/'
	},

	card: {
		url: 'http://localhost:6000/'
	},

	library: {
		url: 'http://120.86.68.198:9090/',
		modeMap: {
			name: 'TITLE',
			author: 'AUTHOR',
			classNum: 'CLASSNO',
			label: 'SUBJECT',
			isbn: 'ISBN.010$a',
			issn: 'ISBN.011$a',
			official: 'UNIONNO'
		},
		filterMap: {
			name: '+select+BIBLIOS.BOOKRECNO%2CBIBLIOS.DCXML%2CBIBLIOS.MARCFORMAT%2CBIBLIOS.BOOKTYPE++FROM+BIBLIOS+where+++BIBLIOS.FINISH+%3E+-1++and++exists+%28+select+%27X%27++from+HOLDING++where++BOOKRECNO%3DBIBLIOS.BOOKRECNO%29++and++++exists+%28+select+%27X%27++from+BIB_TITIDX++where++%28upper%28TITLE%29++like+%27{{content}}%25%27%29+and+BOOKRECNO%3DBIBLIOS.BOOKRECNO+%29++',
			author: '+select+BIBLIOS.BOOKRECNO%2CBIBLIOS.DCXML%2CBIBLIOS.MARCFORMAT%2CBIBLIOS.BOOKTYPE++FROM+BIBLIOS+where+++BIBLIOS.FINISH+%3E+-1++and++exists+%28+select+%27X%27++from+HOLDING++where++BOOKRECNO%3DBIBLIOS.BOOKRECNO%29++and++++exists+%28+select+%27X%27++from+BIB_AUTIDX++where++%28upper%28AUTHOR%29++like+%27{{content}}%25%27%29+and+BOOKRECNO%3DBIBLIOS.BOOKRECNO+%29++',
			classNum: '+select+BIBLIOS.BOOKRECNO%2CBIBLIOS.DCXML%2CBIBLIOS.MARCFORMAT%2CBIBLIOS.BOOKTYPE++FROM+BIBLIOS+where+++BIBLIOS.FINISH+%3E+-1++and++exists+%28+select+%27X%27++from+HOLDING++where++BOOKRECNO%3DBIBLIOS.BOOKRECNO%29++and++++exists+%28+select+%27X%27++from+BIB_CLAIDX++where++%28upper%28CLASSNO%29++like+%27{{content}}%25%27%29+and+BOOKRECNO%3DBIBLIOS.BOOKRECNO+%29++',
			label: '+select+BIBLIOS.BOOKRECNO%2CBIBLIOS.DCXML%2CBIBLIOS.MARCFORMAT%2CBIBLIOS.BOOKTYPE++FROM+BIBLIOS+where+++BIBLIOS.FINISH+%3E+-1++and++exists+%28+select+%27X%27++from+HOLDING++where++BOOKRECNO%3DBIBLIOS.BOOKRECNO%29++and++++exists+%28+select+%27X%27++from+BIB_SUBIDX++where++%28upper%28SUBJECT%29++like+%27{{content}}%25%27%29+and+BOOKRECNO%3DBIBLIOS.BOOKRECNO+%29++',
			isbn: '+select+BIBLIOS.BOOKRECNO%2CBIBLIOS.DCXML%2CBIBLIOS.MARCFORMAT%2CBIBLIOS.BOOKTYPE++FROM+BIBLIOS+where+++BIBLIOS.FINISH+%3E+-1++and++exists+%28+select+%27X%27++from+HOLDING++where++BOOKRECNO%3DBIBLIOS.BOOKRECNO%29++and++++exists+%28+select+%27X%27++from+BIB_ISBNIDX++where++%28upper%28ISBN%29++like+%27{{content}}%25%27%29+and+BOOKRECNO%3DBIBLIOS.BOOKRECNO++and++FIELDTYPE+%3D+%27010%24a%27%29++',
			issn: '+select+BIBLIOS.BOOKRECNO%2CBIBLIOS.DCXML%2CBIBLIOS.MARCFORMAT%2CBIBLIOS.BOOKTYPE++FROM+BIBLIOS+where+++BIBLIOS.FINISH+%3E+-1++and++exists+%28+select+%27X%27++from+HOLDING++where++BOOKRECNO%3DBIBLIOS.BOOKRECNO%29++and++++exists+%28+select+%27X%27++from+BIB_ISBNIDX++where++%28upper%28ISBN%29++like+%27{{content}}%25%27%29+and+BOOKRECNO%3DBIBLIOS.BOOKRECNO++and++FIELDTYPE+%3D+%27011%24a%27%29++',
			official: '+select+BIBLIOS.BOOKRECNO%2CBIBLIOS.DCXML%2CBIBLIOS.MARCFORMAT%2CBIBLIOS.BOOKTYPE++FROM+BIBLIOS+where+++BIBLIOS.FINISH+%3E+-1++and++exists+%28+select+%27X%27++from+HOLDING++where++BOOKRECNO%3DBIBLIOS.BOOKRECNO%29++and++++exists+%28+select+%27X%27++from+BIB_UNIIDX++where++%28upper%28UNIONNO%29++like+%27{{content}}%25%27%29+and+BOOKRECNO%3DBIBLIOS.BOOKRECNO+%29++'
		}
	},

	studio: {
		url: 'https://java.cx-tidc.com/',
		oauth: {
			url: 'https://oauth.cx-tidc.com/',
			clientId: 'f5pzjhBDN7pFRFReCzpxpYlpNoPuikp2fWPJO+TQh3g0A1TftqDpjPLU0VwB3qmI2Xewf2wTiw5y5emo7NB0piiXesXoTxW2M9mlfBCcpxJxnvHeHu0fmX2VGl485YvWWSv6WXHdOlXHfRXZL28L6iRVVCBayZu0LO5pDM9FusM=',
			clientSecret: '0cgaq8gmkWgP9VIyNpRIiS1A09vT55iytqcPEBNuNPQNxJVyKHdL9L3u3Anrc00fjkw4+xzZyFic/20GkztN8ssx2jKKD6mlfWyqFRyQmVR4E1IrP2ls2CaLXOXzNHiaFj+rmUzTkSSHU6GI5Uer+0FadozGxZXpggtd7zNCdyY='
		},
		errList: {
			'1000': {
				code: '-3067',
				msg: 'studio user no access request interface'
			},
			'1005': {
				code: '-3066',
				msg: 'studio token auth failed'
			},
			'1006': {
				code: '-3061',
				msg: 'studio username or password error'
			}
		}
	},

	oss: {
		accessKeyId: 'LTAIO8GRYnlLVHkh',
		accessKeySecret: '2GWfWe21aH9vzg5brE411QWSmgfmJn'
	},

	/*
	
	 微信小程序配置
	 
	 [object] name 该小程序的配置
	 
	*/

	miniApp: {
		cxxy: {
			appId: 'wx7305eb5d5801811f',
			appSecret: '025639d73e1287915aeb1a9247b1f387',
			grantType: 'authorization_code',
			msgTemplate: {
				
			}
		}
	}

}
class h_sign {
	
	constructor() {
		
	}

	getDeviceByRoomId(roomId) {
		return await this.mysql.cxxy.select({
			table: [
				'sign_device s',
				'classroom c'
			],
			field: [
				'deviceId',
				'deviceDescription',
				'deviceBroken',
				'roomName'
			],
			where: 's.roomId=c.roomId'
		})
	}

}

module.exports = h_sign
class h_story {

    async getStoryListCache(start = 0, end = 30, sort = 'desc') {
        return await this.redis.system.get(`storyList@${start}-${end}@${sort}`, true)
    }

    async getStoryList(start = 0, end = 30, sort = 'desc') {
        const result = await this.mysql.cxxy.select({
            table: 'story',
            field: [
                'storyIndex',
                'storyTitle',
                'storyDescription',
                'storyCover',
                'storyAuthor',
                'storyDate'
            ],
            where: 'storyClosed=0 && storyPass=1',
            limit: {
                start,
                end
            },
            sort: {
                field: 'storyDate',
                order: sort == 'desc' ? 'desc' : 'asc'
            }
        })
        await this.redis.system.set(`storyList@${start}-${end}@${sort}`, result)
        return result
    }

    async getStoryCache(storyId, start = 0, end = 30) {
        return await this.redis.cache.get(`storyData@${storyId}@${start}-${end}`, true)
    }

    async getStory(storyId, start = 0, end = 30) {
        const storyResult = await this._checkStoryExists(storyId)
        if(!storyResult)
            return this.reject('-2111', 'story data not found')
        if(!storyResult.storyPass)
            return this.reject('-2114', 'story not pass')
        const followStoryResult = await this.mysql.cxxy.select({
            table: 'story_follow',
            field: [
                'storyId',
                'storyIndex',
                'storyAuthor',
                'storyContent',
                'storyClosed',
                'storyPass',
                'storyDate'
            ],
            where: 'storyFollowId=$storyId && storyPublish=1',
            data: {
                storyId
            },
            limit: {
                start,
                end
            }
        })
        let storyData = start == 0 ? storyResult : {storyId}
        storyData.storyList = followStoryResult
        await this.redis.cache.set(`storyData@${storyId}@${start}-${end}`, storyData)
        return storyData
    }

    async addStory(storyTitle, storyDescription, storyCover, storyAuthor, storyContent) {
        const storyId = await this.common.createRandomStr(32)
        await this.mysql.cxxy.insert({
            table: 'story',
            field: [
                'storyId',
                'storyTitle',
                'storyDescription',
                'storyCover',
                'storyAuthor',
                'storyContent',
                'storyDate'
            ],
            data: {
                storyId,
                storyTitle,
                storyDescription,
                storyCover,
                storyAuthor,
                storyContent,
                storyDate: await this.common.timestamp()
            }
        })
        for(let key of await this.redis.system.find('lostList@*'))
			await this.redis.system.delete(key)
        await this.redis.cache.delete(`storyList@0-30`, storyData)
        return 1
    }

    async followStory(storyFollowId, storyAuthor, storyContent) {
        const storyId = await this.common.createRandomStr(32)
        const storyResult = await this._checkStoryExists(storyFollowId)
        if(!storyResult)
            return this.reject('-2111', 'story data not found')
        const {storyPass, storyIndex} = storyResult
        if(!storyPass)
            return this.reject('-2114', 'story not pass')
        await this.mysql.cxxy.insert({
            table: 'story_follow',
            field: [
                'storyId',
                'storyFollowId',
                'storyAuthor',
                'storyContent',
                'storyDate'
            ],
            data: {
                storyId,
                storyFollowId,
                storyAuthor,
                storyContent,
                storyDate: await this.common.timestamp()
            }
        })
        return 1
    }
    
    async closeStory(storyId, storyAuthor) {
        const storyResult = await this._checkStoryExists(storyId)
        if(!storyResult)
            return this.reject('-2111', 'story data not found')
        if(storyResult.storyAuthor != storyAuthor)
            return this.reject('-2112', 'change story permission denied')
        const updateResult = await this.mysql.cxxy.update({
            table: 'story',
            field: [
                'storyClosed=1'
            ],
            where: 'storyId=$storyId && storyAuthor=$storyAuthor',
            data: {
                storyId,
                storyAuthor
            }
        })
        console.log('更改个数', updateResult)
        if(updateResult == 0)
            return this.reject('-2113', 'close story failed')
        return 1
    }

    async closeFollowStory(storyId, storyAuthor) {
        const followStoryResult = await this._checkFollowStoryExists(storyId)
        if(!followStoryResult)
            return this.reject('-2111', 'story data not found')
        if(followStoryResult.storyAuthor != storyAuthor)
            return this.reject('-2112', 'change story permission denied')
        const updateResult = await this.mysql.cxxy.update({
            table: 'story_follow',
            field: [
                'storyClosed=1'
            ],
            where: 'storyId=$storyId && storyAuthor=$storyAuthor',
            data: {
                storyId,
                storyAuthor
            }
        })
        if(updateResult == 0)
            return this.reject('-2113', 'close story failed')
        return 1
    }

    async _checkStoryExists(storyId) {
        let storyResult = await this.mysql.cxxy.select({
            table: 'story',
            field: [
                'storyIndex',
                'storyTitle',
                'storyDescription',
                'storyCover',
                'storyAuthor',
                'storyContent',
                'storyClosed',
                'storyDate'
            ],
            where: 'storyId=$storyId',
            data: {
                storyId
            }
        })
        storyResult[0].storyId = storyId
        return storyResult[0] || false
    }

    async _checkFollowStoryExists(storyId) {
        let storyResult = await this.mysql.cxxy.select({
            table: 'story_follow',
            field: [
                'storyIndex',
                'storyFollowId',
                'storyAuthor',
                'storyContent',
                'storyClosed',
                'storyDate'
            ],
            where: 'storyId=$storyId',
            data: {
                storyId
            }
        })
        storyResult[0].storyId = storyId
        return storyResult[0] || false
    }

}

module.exports = h_story
class h_active {

	constructor() {
		
	}

	async getActiveData(activeId) {
		activeId = this._convertActiveId(activeId)
		return await this._getActiveData(activeId)
	}

	async joinActive(userId, activeId, activeParams = {}) {
		activeId = this._convertActiveId(activeId)
		const activeData = await this._getActiveData(activeId)
		let result
		switch(activeData.activeType) {
			case 0:
				//提交表单报名
				result = await this._joinFormActive(userId, activeId, activeData, activeParams)
			break;
			case 1:
				//抽奖码返回
				result = await this._joinCodeActive(userId, activeId, activeData)
			break
		}
		return result
	}

	async getActivePrize(activeId) {
		const list = await this.redis.active.find(`prize@*`)
		for(let key of list) {
			const val = await this.redis.active.get(key)
			console.log(key + ' | ' +  val)
		}
		return await this.redis.active.get(`prize@${activeId}`)
	}

	async lotteryDraw(activeId, code) {
		await this.mysql.cxxy.trans()
		const mustCode = await this.redis.active.get(`mustCode@${activeId}`, true)
		console.log(mustCode)
		if(mustCode && mustCode[code]) {
			const prizeId = mustCode[code].code
			const prizeName = await this.redis.active.get(`prize@${activeId}@${prizeId}`)
			if(!prizeName)
				return this.reject('-2042', 'active prize is empty')
			await this.mysql.cxxy.insert({
				table: 'lottery',
				field: [
					'userId',
					'prizeName',
					'code',
					'codeDate',
					'date'
				],
				data: {
					userId: mustCode[code].user,
					prizeName,
					code,
					codeDate: 0,
					date: parseInt(Date.now() / 1000)
				}
			})
			await this.redis.active.delete(`prize@${activeId}@${prizeId}`)
			await this.redis.active.delete(`code@${activeId}@${ mustCode[code].user}`)
			return prizeName
		}
		if(parseInt(Date.now() / 1000) < 1524466800)
			return this.reject('-2038', 'active not start')
		if(parseInt(Date.now() / 1000) > 1524488400)
			return this.reject('-2039', 'active is expire')
		const keys = await this.redis.active.find(`code@${activeId}@*`)
		console.log(keys)
		for(let key of keys) {
			const temp = await this.redis.active.get(key, true)
			if(temp.code == code) {
				const prizes = await this.redis.active.find(`prize@${activeId}@*`)
				if(prizes.length == 0)
					return this.reject('-2042', 'active prize is empty')
				const k = prizes[parseInt(Math.random() * prizes.length)]
				const prizeName = await this.redis.active.get(k)
				if(!prizeName)
					return this.reject('-2042', 'active prize is empty')
				await this.mysql.cxxy.insert({
					table: 'lottery',
					field: [
						'userId',
						'prizeName',
						'code',
						'codeDate',
						'date'
					],
					data: {
						userId: temp.user,
						prizeName,
						code,
						codeDate: temp.time,
						date: parseInt(Date.now() / 1000)
					}
				})
				await this.redis.active.delete(k)
				await this.redis.active.delete(key)
				return prizeName
			}
		}
		await this.mysql.cxxy.cmt()
		return this.reject('-2041', 'active code not found')
	}

	async _joinFormActive(userId, activeId, activeData, activeParams) {
		return 1
	}

	async _joinCodeActive(userId, activeId, activeData) {
		const result = await this.redis.active.get(`code@${activeId}@${userId}`, true)
		if(result) {
			return result.code
		}
		const code = await this.common.createRandomStr(6)
		await this.redis.active.set(`code@${activeId}@${userId}`, {
			code,
			user: userId,
			time: parseInt(Date.now() / 1000)
		})
		return code
	}

	async _getActiveData(activeId) {
		let active = await this.redis.active.get(`active@${activeId}`,  true)
		if(active)
			return active
		//标记 记得做缓存
		const result = await this.mysql.cxxy.select({
			table: 'active',
			field: [
				'activeType',
				'activeName',
				'activeData',
				'activeStartDate',
				'activeEndDate',
				'activeDate'
			],
			where: 'activeId=$activeId',
			data: {
				activeId
			}
		})
		if(!result[0])
			return this.reject('-2037', 'active not found')
		const {activeType, activeName, activeData, activeStartDate, activeEndDate, activeDate} = result[0]
		const now = parseInt(Date.now() / 1000)
		if(now < activeStartDate)
			return this.reject('-2038', 'active not start')
		if(now >= activeEndDate)
			return this.reject('-2039', 'active is expire')
		active = {
			activeId,
			activeType,
			activeName,
			activeData,
			activeStartDate,
			activeEndDate,
			activeDate
		}
		await this.redis.active.set(`active@${activeId}`, active, activeEndDate - now)
		return active
	}

	_convertActiveId(activeId) {
		return this.crypto.md5(activeId)
	}

}

module.exports = h_active
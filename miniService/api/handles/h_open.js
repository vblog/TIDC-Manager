class h_open {

	async getDeviceById(deviceId) {
		await this._checkDeviceId(deviceId)
		const data = await this.mysql.cxxyOpen.select({
			table: 'device_status',
			field: [
				'deviceName',
				'deviceInterface',
				'deviceMAC',
				'deviceIPv4',
				'deviceIPv6',
				'deviceNetmask',
				'deviceSyncTime'
			],
			where: 'deviceId=$deviceId',
			data: {
				deviceId
			}
		})
		if(!data[0])
			return this.reject('-4001', 'device not found')
		return data[0]
	}

	async getDeviceIPv4(deviceId) {
		await this._checkDeviceId(deviceId)
		const data = await this.mysql.cxxyOpen.select({
			table: 'device_status',
			field: 'deviceIPv4',
			where: 'deviceId=$deviceId',
			data: {
				deviceId
			}
		})
		if(!data[0])
			return this.reject('-4001', 'device not found')
		return data[0].deviceIPv4
	}

	async createDevice(deviceName, deviceNetWorkInfo) {
		const networkData = await this._parseNetWorkInfo(deviceNetWorkInfo)
		const deviceId = await this.common.createRandomStr(32)
		for(let _interface in networkData) {
			const {mac, ipv4, ipv6, netmask} = networkData[_interface]
			await this.mysql.cxxyOpen.insert({
				table: 'device_status',
				field: [
					'deviceId',
					'deviceName',
					'deviceInterface',
					'deviceMAC',
					'deviceIPv4',
					'deviceIPv6',
					'deviceNetmask',
					'deviceSyncTime'
				],
				data: {
					deviceId,
					deviceName,
					deviceInterface: _interface,
					deviceMAC: mac,
					deviceIPv4: ipv4,
					deviceIPv6: ipv6,
					deviceNetmask: netmask,
					deviceSyncTime: parseInt(Date.now() / 1000)
				}
			})
		}
		return deviceId
	}

	async updateDevice(deviceId, deviceNetWorkInfo) {
		await this._checkDeviceId(deviceId)
		const networkData = await this._parseNetWorkInfo(deviceNetWorkInfo)
		for(let _interface in networkData) {
			const {mac, ipv4, ipv6, netmask} = networkData[_interface]
			const data = await this.mysql.cxxyOpen.select({
				table: 'device_status',
				field: 'deviceId',
				where: 'deviceInterface=$deviceInterface && deviceId=$deviceId',
				data: {
					deviceInterface: _interface,
					deviceId
				}
			})
			if(!data[0]) 
				await this.mysql.cxxyOpen.insert({
					table: 'device_status',
					field: [
						'deviceId',
						'deviceInterface',
						'deviceMAC',
						'deviceIPv4',
						'deviceIPv6',
						'deviceNetmask',
						'deviceSyncTime'
					],
					data: {
						deviceId,
						deviceInterface: _interface,
						deviceMAC: mac,
						deviceIPv4: ipv4,
						deviceIPv6: ipv6,
						deviceNetmask: netmask,
						deviceSyncTime: parseInt(Date.now() / 1000)
					}
				})
			else
				await this.mysql.cxxyOpen.update({
					table: 'device_status',
					field: [
						'deviceMAC=$deviceMAC',
						'deviceIPv4=$deviceIPv4',
						'deviceIPv6=$deviceIPv6',
						'deviceNetmask=$deviceNetmask'
					],
					where: 'deviceInterface=$deviceInterface and deviceId=$deviceId',
					data: {
						deviceMAC: mac,
						deviceIPv4: ipv4,
						deviceIPv6: ipv6,
						deviceNetmask: netmask,
						deviceInterface: _interface,
						deviceId
					}
				})
		}
		return 1
	}

	async updateDeviceName(deviceId, deviceName) {
		await this._checkDeviceId(deviceId)
		await this.mysql.cxxyOpen.update({
			table: 'device_status',
			field: 'deviceName=$deviceName',
			where: 'deviceId=$deviceId',
			data: {
				deviceName,
				deviceId
			}
		})
		return 1
	}

	async deleteDeviceById(deviceId) {
		await this._checkDeviceId(deviceId)
		await this.mysql.cxxyOpen.delete({
			table: 'device_status',
			where: 'deviceId=$deviceId',
			data: {
				deviceId
			}
		})
		return 1
	}

	async _checkDeviceId(deviceId) {
		if(!/\w{32}/.test(deviceId))
			return this.reject('-4000', 'device id invalid')
		const data = await this.mysql.cxxyOpen.select({
			table: 'device_status',
			field: 'deviceId',
			where: 'deviceId=$deviceId',
			data: {
				deviceId
			}
		})
		if(!data[0])
			return this.reject('-4001', 'device not found')
	}

	async _parseNetWorkInfo(info) {
		try {
			const temp = info.split('\n\n')
			console.log(temp)
			if(temp.length == 0)
				return this.reject('-4003', 'parse network failed')
			const interfaceRegExp = /([a-z0-9]+):\sf/i
			const macRegExp = /ether\s(([a-z0-9]{2}:){5}[a-z0-9]{2})/i
			const ipv4RegExp = /inet\s(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})/i
			const ipv6RegExp = /inet6\s([a-z0-9]{1,4}::([a-z0-9]{1,4}:){3}[a-z0-9]{1,4})/i
			const netmaskRegExp = /netmask\s(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})/i
			let networkData = {}
			for(let i = 0;i < temp.length;i++) {
				const other = temp[i]
				console.log(other)
				let otherTemp = other.match(interfaceRegExp)
				const _interface = otherTemp ? otherTemp[1] : ''
				otherTemp = other.match(macRegExp)
				const mac = otherTemp ? otherTemp[1] : ''
				otherTemp = other.match(ipv4RegExp)
				const ipv4 = otherTemp ? otherTemp[1] : ''
				otherTemp = other.match(ipv6RegExp)
				const ipv6 = otherTemp ? otherTemp[1] : ''
				otherTemp = other.match(netmaskRegExp)
				const netmask = otherTemp ? otherTemp[1] : ''
				console.log(mac, ipv4, ipv6, netmask)
				networkData[_interface] = {
					mac,
					ipv4,
					ipv6,
					netmask
				}
			}
			return networkData
		}
		catch(err) {
			console.error('parse network failed', err)
			return this.reject('-4003', 'parse network failed')
		}
	}

}

module.exports = h_open
class h_department {

	checkClassId(classId) {
		return this._checkClassId(classId)
	}

	//添加新教室
	async addClassroom(roomName) {
		return await this._addClassroom(roomName)
	}

	async addFaculty(facultyName) {
		if(facultyName == '-1')
			return '-1'
		if(facultyName.length < 3) 
			return this.reject('-1048', 'faculty data invalid')
		const facultyId = this.crypto.md5(this.common.trim(facultyName))
		const result = await this.mysql.cxxy.select({
			table: 'faculty',
			field: [
				'facultyId',
				'facultyName'
			],
			where: 'facultyId=$facultyId',
			data: {
				facultyId
			}
		})
		if(result.length == 0) {
			console.log(`add new faculty ${facultyId} ${facultyName}`)
			await this.mysql.cxxy.insert({
				table: 'faculty',
				field: [
					'facultyId',
					'facultyName',
					'facultyDate'
				],
				data: {
					facultyId,
					facultyName,
					facultyDate: this.common.timestamp()
				}
			})
			return {
				facultyId,
				facultyName
			}
		}
		else
			console.log(`faculty already exists ${facultyId} ${facultyName}`)
		return result[0]
	}

	async addMajor(majorName = '', facultyId = '') {
		if(majorName.length < 2 || facultyId.length != 32) 
			return this.reject('-2017', 'major data invalid')
		const majorId = this.crypto.md5(this.common.trim(majorName))
		const result = await this.mysql.cxxy.select({
			table: 'major',
			field: [
				'majorId',
				'majorName',
				'facultyId'
			],
			where: 'majorId=$majorId',
			data: {
				majorId
			}
		})
		if(result.length == 0) {
			console.log(`add new major ${majorId} ${majorName}`)
			await this.mysql.cxxy.insert({
				table: 'major',
				field: [
					'majorId',
					'majorName',
					'facultyId',
					'majorDate'
				],
				data: {
					majorId,
					majorName,
					facultyId,
					majorDate: this.common.timestamp()
				}
			})
			return {
				majorId,
				majorName
			}
		}
		else
			console.log(`major already exists ${majorId} ${majorName}`)
		return result[0]
	}

	async addClass(className, majorId) {
		return await this._addClass(className, majorId)
	}

	async addStaffRoom(roomName) {
		if(roomName == '-1')
			return {staffRoomId: '-1', staffRoomName: ''}
		const roomId = this.crypto.md5(roomName)
		const result = await this.mysql.cxxy.select({
			table: 'staff_room',
			field: 'roomId',
			where: 'roomId=$roomId',
			data: {
				roomId
			}
		})
		if(!result[0]) {
			console.log(`add new staff room ${roomName}`)
			await this.mysql.cxxy.insert({
				table: 'staff_room',
				field: [
					'roomId',
					'roomName'
				],
				data: {
					roomId,
					roomName
				}
			})
		}
		else
			console.log(`staff room ${roomName} already exists`)
		return {
			staffRoomId: roomId,
			staffRoomName: roomName
		}
	}

	async _addClass(className = '', majorId = '') {
		if(!/^\d{2}(.+){3,}$/.test(className))
			return this.reject('-2016', 'class data invalid')
		if(!/^[a-z0-9]{32}$/.test(majorId))
			return this.reject('-3026', 'major id invalid')
		const classId = this.crypto.md5(this.common.trim(className))
		const classGrade = className.substring(0, 2)
		className = className.substring(2)
		const result = await this.mysql.cxxy.select({
			table: 'class',
			field: [
				'classId',
				'classGrade',
				'className',
				'majorId'
			],
			where: 'classId=$classId',
			data: {
				classId
			}
		})
		if(result.length == 0) {
			console.log(`add new class ${classId} ${classGrade} ${className}`)
			await this.mysql.cxxy.insert({
				table: 'class',
				field: [
					'classId',
					'classGrade',
					'className',
					'majorId',
					'classDate'
				],
				data: {
					classId,
					classGrade,
					className,
					majorId,
					classDate: await this.common.timestamp()
				}
			})
			return {
				classId,
				classGrade,
				className
			}
		}
		else
			console.log(`class already exists ${classId} ${classGrade} ${className}`)
		return result[0]
	}

	async _addClassroom(roomName) {
		if(roomName instanceof Array) {
			const roomNames = roomName.join(',')
			
		}
		else {
			const roomId = this.crypto.md5(roomName)
			const result = await this.mysql.cxxy.select({
				table: 'classroom',
				field: 'id',
				where: 'roomId=$roomId',
				data: {
					roomId
				}
			})
			if(result.length == 0) {
				console.log(`add new classroom ${roomName}`)
				await this.mysql.cxxy.insert({
					table: 'classroom',
					field: [
						'roomId',
						'roomName'
					],
					data: {
						roomId,
						roomName
					}
				})
			}
			else
				console.log(`classroom ${roomName} already exists`)
			return roomId
		}
	}

	_checkClassId(classId) {
		return /^[a-z0-9]{32}$/.test(classId)
	}

}

module.exports = h_department
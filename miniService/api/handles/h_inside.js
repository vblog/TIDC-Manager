class h_inside {

	async insideLogin(insideName, insidePwd) {
		if(!insideName || !insidePwd || insidePwd.length < 6)
			return this.reject('-2024', 'inside user login failed')
		const encryptPwd = this._encryptInsidePwd(insideName, insidePwd)
		const result = await this.mysql.cxxy.select({
			table: [
				'inside i',
				'student s',
				'class c'
			],
			field: [
				'i.insideId',
				'i.insideUser',
				'i.insideNickName',
				's.stuId,s.stuName',
				'c.className',
				'c.classGrade'
			],
			where: 'i.insideUser=s.userId && s.stuClass=c.classId && i.insideName=$insideName && i.insidePwd=$insidePwd',
			data: {
				insideName,
				insidePwd: encryptPwd
			}
		})
		if(result.length == 0)
			return this.reject('-2024', 'inside user login failed')
		const {insideId, insideUser, insideNickName, stuId, stuName, className, classGrade} = result[0]
		const {session, token} = await this.hd.user.createSession('inside', insideId, {
			stuUserId: insideUser
		})
		return this.resolve({
			insideNickName,
			stuId,
			stuName,
			className,
			classGrade,
			session,
			token
		})
	}

	async getStuDataByInsideId(insideId, fields) {
		if(!insideId)
			return this.reject('-2026', 'inside id invalid')
		const stuData = await this._getStuDataByInsideId(insideId, fields)
		if(!stuData)
			return this.reject('-2025', 'inside student not found')
		return stuData
	}

	async getInsideStuDataByMajorId(majorId, fields) {
		const stuData = await this._getInsideStuDataByMajorId(majorId, fields)
		if(!stuData)
			return this.reject('-2025', 'inside student not found')
		return stuData
	}

	async _getInsideStuDataByMajorId(majorId, ...fields) {
		const stuData = await this.mysql.cxxy.select({
			table: [
				'inside i',
				'student s',
				'class c'
			],
			field: fields,
			where: 'i.insideUser=s.userId && s.stuClass=c.classId && c.majorId=$majorId',
			data: {
				majorId
			}
		})
		if(!stuData[0])
			return null
		return stuData[0]
	}

	async _getStuDataByInsideId(insideId, ...fields) {
		const stuData = await this.mysql.cxxy.select({
			table: [
				'inside i',
				'student s',
				'class c'
			],
			field: fields,
			where: 'i.insideUser=s.userId && s.stuClass=c.classId && insideId=$insideId',
			data: {
				insideId
			}
		})
		if(!stuData[0])
			return null
		return stuData[0]
	}

	_encryptInsidePwd(insideName, insidePwd) {
		return this.crypto.sha256(this.crypto.sha256(insideName) + this.config.frame.pwdSalt + this.crypto.sha256(insidePwd))
	} 

}

module.exports = h_inside
class h_repair {

	//获取维修类型列表
	async getRepairTypes() {
		const repairTypes = await this.hd.sys.getRepairTypes('repairTypes')
		if(!repairTypes)
			return this.reject('-2062', 'repair type parse error')
		return repairTypes
	}

	async createRepairOrder(repairUser, repairType, repairContent, uploadIdList, repairAddress, repairContact) {
		const repairId = await this.common.createRandomStr(32)
		await this._handleRepairPhotoIdList(repairId, uploadIdList)
		await this.mysql.cxxy.insert({
			table: 'repair',
			field: [
				'repairId',
				'repairUser',
				'repairType',
				'repairContent',
				'repairPhotoCount',
				'repairAddress',
				'repairContact',
				'repairEvaluateContent',
				'repairCreateDate'
			],
			data: {
				repairId,
				repairUser,
				repairType,
				repairContent,
				repairPhotoCount: uploadIdList.length,
				repairAddress,
				repairContact,
				repairEvaluateContent: '',
				repairCreateDate: this.common.timestamp()
			}
		})
		await this._clearWaitRepairOrderListByUserIdCache(repairUser)
		return repairId
	}

	async getWaitRepairOrderListByUserIdCache(userId) {
		return await this.redis.cache.get(`waitRepair@${userId}`, true)
	}

	async getAcceptRepairOrderListByUserIdCache(userId) {
		return await this.redis.cache.get(`acceptRepair@${userId}`, true)
	}

	async getFinishRepairOrderListByUserIdCache(userId) {
		return await this.redis.cache.get(`finishRepair@${userId}`, true)
	}

	async getWaitRepairOrderListByUserId(userId) {
		const waitList = await this.mysql.cxxy.select({
			table: 'repair',
			field: [
				'repairId',
				'repairType',
				'repairContent',
				'repairPhotoCount',
				'repairAddress',
				'repairContact',
				'repairCreateDate'
			],
			where: 'repairStatus=0 && repairUser=$repairUser',
			data: {
				repairUser: userId
			}
		})
		console.log(waitList)
		await this.redis.cache.set(`waitRepair@${userId}`, waitList, this.config.frame.sessionExpires.repair)
		return waitList
	}

	async getAcceptRepairOrderListByUserId(userId) {
		const acceptList = await this.mysql.cxxy.select({
			table: [
				'repair r',
				'repair_department d'
			],
			field: [
				'repairId',
				'repairType',
				'repairContent',
				'repairPhotoCount',
				'repairAddress',
				'repairContact',
				'repairCreateDate',
				'repairAcceptDate',
				'departmentName'
			],
			where: 'repairStatus=1 && repairUser=$repairUser && r.repairDepartment=d.departmentId',
			data: {
				repairUser: userId
			}
		})
		await this.redis.cache.set(`acceptRepair@${userId}`, acceptList, this.config.frame.sessionExpires.repair)
		return acceptList
	}

	async getFinishRepairOrderListByUserId(userId) {
		const finishList = await this.mysql.cxxy.select({
			table: [
				'repair r',
				'repair_department d'
			],
			field: [
				'repairId',
				'repairType',
				'repairContent',
				'repairPhotoCount',
				'repairAddress',
				'repairContact',
				'repairEvaluateStar',
				'repairEvaluateContent',
				'repairCreateDate',
				'repairAcceptDate',
				'repairFinishDate',
				'departmentName'
			],
			where: 'repairStatus=2 && repairUser=$repairUser && r.repairDepartment=d.departmentId',
			data: {
				repairUser: userId
			}
		})
		await this.redis.cache.set(`finishRepair@${userId}`, finishList, this.config.frame.sessionExpires.repair)
		return finishList
	}

	async addRepairEvaluate(repairId, repairEvaluateStar, repairEvaluateContent) {
		const result = await this.mysql.cxxy.update({
			table: 'repair',
			field: [
				'repairEvaluateStar=$repairEvaluateStar',
				'repairEvaluateContent=$repairEvaluateContent'
			],
			where: 'repairId=$repairId',
			data: {
				repairEvaluateStar: parseInt(repairEvaluateStar),
				repairEvaluateContent,
				repairId
			}
		})
		if(result == 0)
			return this.reject('-2083', 'repair order update evaluate data failed')
	}

	checkFields(field, value) {
		return {
			repairId: /^[a-zA-Z0-9]{32}$/,
			repairType: /^[a-zA-Z]+\-[0-9]{4}$/
		}[field].test(value)
	}

	async clearFinishRepairOrderListByUserIdCache(userId) {
		await this.redis.cache.delete(`finishRepair@${userId}`)
	}

	async _clearWaitRepairOrderListByUserIdCache(userId) {
		await this.redis.cache.delete(`waitRepair@${userId}`)
	}

	//处理报修图片上传
	async _handleRepairPhotoIdList(repairId, uploadIdList) {
		const repairPath = `${this.config.server.basePath}/${this.config.server.staticPath}/image/repair/${repairId}`
		await this.fs.emptyDir(repairPath)
		for(let index in uploadIdList) {
			const src = `${this.config.server.basePath}/${this.config.server.tmpPath}/${uploadIdList[index]}`
			const dst = `${repairPath}/${index}.jpg`
			await this.common.moveImage(src, dst)
			await this.common.compressJpeg(dst)
		}
	}

}

module.exports = h_repair
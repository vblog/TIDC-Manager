module.exports = {

	frame: {
		allowUserType: ['student', 'teacher'],
		logFile: `${process.cwd()}/logs/msys.log`,
		dataKey: 'Egnbb0B3vR69mHI7s5zxESner4AZBva3/OmcpQLP540aDVNMV2USm+2S71aq6Z8DX0Eo0rgHHMpDe042qylhk4zo66h/lq9BS2wUVlreNp2/gUp3xqV284gydzreGxSjuNErl0ZxlJUctyzNeDi+itnPwVJQ1L2q0TllkCm95nY=',
		dataIv: 'v0ySU4tm+gZRLLuVUAefTG3y4tkhJ66V+Hnzp3vcHmaVMkqP6fCapUmmdr5J1tnlXlz/On0XHcfEJob5WtY7KwfaDqgmr0mS02H4J1ImEgTPg+iKwAchho6Cr5J4TYxiME0d/mkQ4eDjPWu61KngrJ65cfV538pX7L2Hv+DsfNo='
	},

	server: {

		port: 2366

	},

	redis: {
		session: {
			host: '127.0.0.1',
			port: 7878,
			pwd: 'cxxyRedis456289788',
			db: 0
		},
		system: {
			host: '127.0.0.1',
			port: 7878,
			pwd: 'cxxyRedis456289788',
			db: 1
		},
		// grab: {
		// 	host: '127.0.0.1',
		// 	port: 7878,
		// 	pwd: 'cxxyRedis456289788',
		// 	db: 2
		// },
		user: {
			host: '127.0.0.1',
			port: 7878,
			pwd: 'cxxyRedis456289788',
			db: 3
		},
		// active: {
		// 	host: '127.0.0.1',
		// 	port: 7878,
		// 	pwd: 'cxxyRedis456289788',
		// 	db: 4
		// },
		// cache: {
		// 	host: '127.0.0.1',
		// 	port: 7878,
		// 	pwd: 'cxxyRedis456289788',
		// 	db: 5
		// }
	},

	amqp: {
		message: {
			host: 'localhost',
			port: 5672,
			vhost: 'cxxy',
			user: 'cxxy',
			pwd: 'cxxyVhost456289788'
		}
	}

}